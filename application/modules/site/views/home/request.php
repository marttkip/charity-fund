 <!--Parallax Section Start-->
<section class="cp-parallax-section">
   <div id="cp-map-home"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-md-offset-2">
                <!--Parallax Box Start-->
                <div class="cp-parallax-box">
                   <h2>Not sure where the best</h2>
                   <h3>Place to collect your <span>CAR RENTAL</span> is?</h3>
                   <div class="cp-location-box">
                       <h3>Try Our location finder</h3>
                       <ul class="cp-location-listed">
                           <li>Find rental loaction on map</li>
                           <li>Check ‘One way rental’ Fees</li>
                           <li>Plan a route <a href="#" class="cp-btn-style1">Try It Now</a></li>
                       </ul>
                   </div> 
                </div><!--Parallax Box End-->
            </div>
        </div>
    </div>
    <div class="animate-bus">
        <img src="images/animate-bus2.png" alt="">
    </div>
</section><!--Parallax Section End-->
