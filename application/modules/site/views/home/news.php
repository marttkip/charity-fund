<?php
$services_query = $this->site_model->get_active_items('About Us');
$services_items = '';
if($services_query->num_rows() > 0)
{
	foreach($services_query->result() as $row)
	{
		$brief_title = $row->post_title;
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image_service = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		$year = date('Y',strtotime($created));
		$created_on = date('jS M Y',strtotime($row->created));

		$image = '<img src="'.$image_service.'" class="img-circle" alt=""/>';
	}
}
?>

<div class="content-wrapper-page">
		<!-- Inner Wrapper -->
	<section class="inner-wrapper about" id="about-co">
	  <div class="container" id="blog-content-item">
	    <div class="row brown">
	      <div class="col-sm-12 col-md-6">
	        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
	        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	      </div>
	      <div class="col-sm-12 col-md-6">
	          <div class="item"><img src="<?php echo base_url()."assets/themes/sr-builders/";?>images/about-img1.jpg" alt="SR Builders - Constructions"> </div>
	          
	      </div>
	    </div>
	  
	   <div class="row brown">
	      <div class="col-sm-12 col-md-6">
	        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
	        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	      </div>
	      <div class="col-sm-12 col-md-6">
	          <div class="item"><img src="<?php echo base_url()."assets/themes/sr-builders/";?>images/about-img1.jpg" alt="SR Builders - Constructions"> </div>
	          
	      </div>
	    </div>
	  </div>
	</section>
	<!-- Footer Wrapper -->

</div>