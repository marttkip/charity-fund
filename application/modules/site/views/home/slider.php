		<div class="slider" style="background:url(<?php echo base_url();?>assets/images/slider-bg.jpg);">
        	
			<div id="owl-carousel" class="owl-carousel owl-theme">
                <?php
				if($slides->num_rows() > 0)
				{
					foreach($slides->result() as $slide)
					{
						$slide_name = $slide->slideshow_name;
						$description = $slide->slideshow_description;
						$slide_image = $slide->slideshow_image_name;
						$slideshow_link = $slide->slideshow_link;
						$slideshow_button_text = $slide->slideshow_button_text;
						$description = $this->site_model->limit_text($description, 8);
						$image = base_url().'assets/slideshow/'.$slide_image;
						
						?>
                        <div class="item">
                            <figure>
                                <figcaption>
                                    <span>Institute of Directors Kenya</span>
                                    <h3><?php echo $slide_name;?></h3>
                                    <p><?php echo $description;?></p>
                                    <a href="<?php echo $slideshow_link;?>" target="_blank" class="btn-1"><?php echo $slideshow_button_text;?></a>
                                </figcaption>
                            </figure>
                        </div>
					<?php
					}
				}
				?>
			</div>
		</div>

