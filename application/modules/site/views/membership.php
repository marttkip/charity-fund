
<?php

  $page_item = $this->site_model->decode_web_name('Membership');
  $query= $this->site_model->get_active_content_items($page_item);
   $result = '';
// var_dump($page_item); die();
  // var_dump($query->num_rows()); die();
   $image_header = 'http://placehold.it/1920x1280';
  if($query->num_rows() > 0)
  {

    foreach($query->result() as $row)
    {
      $brief_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_service = base_url().'assets/images/posts/'.$row->post_image;
      $image_header = base_url().'assets/images/posts/'.$row->post_header;
      $created_by = $row->created_by;
      $is_parent = $this->site_model->check_parent($blog_category_id);
      $modified_by = $row->modified_by;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $membership_description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $membership_description), 0, 20));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));


      $gallery = $this->site_model->get_post_gallery($post_id);
      $gallery_item = '';
      if($gallery->num_rows() > 0)
      {
        foreach ($gallery->result() as $key => $value) {
          # code...
          $post_gallery_image_name = $value->post_gallery_image_name;
          $gallery_item .= '<div class="item">
                              <div class="thumb">
                                <img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" alt="">
                              </div>
                            </div>';
        }
      }

    }
}


$about_query = $this->site_model->get_active_content_items('Membership Benefits');
$member_benefits_description = '';
$member_benefits_image = '';
$member_benefits_header = 'http://placehold.it/1920x1280';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $member_benefits_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($member_benefits_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $member_benefits_image = base_url().'assets/images/posts/'.$row->post_image;
    $member_benefits_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $member_benefits_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $member_benefits_description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));   
  }
}



$about_query = $this->site_model->get_active_content_items('Donor Bill Of Rights');
$donor_bills_description = '';
$donor_bills_image = '';
$donor_bills_header = 'http://placehold.it/1920x1280';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $donor_bills_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($donor_bills_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $donor_bills_image = base_url().'assets/images/posts/'.$row->post_image;
    $donor_bills_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $donor_bills_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $donor_bills_description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));   
  }
}


$about_query = $this->site_model->get_active_content_items('Become A Volunteer');
$volunteer_description = '';
$volunteer_image = '';
$item  = '';
$volunteer_header = 'http://placehold.it/1920x1280';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $volunteer_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($volunteer_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $post_video = $row->post_video;
    $volunteer_image = base_url().'assets/images/posts/'.$row->post_image;
    $volunteer_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $volunteer_description = $row->post_content;
    // $volunteer_description_mini = implode(' ', array_slice(explode(' ', $volunteer_description), 0, 100));

    $company_mini = strip_tags($volunteer_description);
    $volunteer_description_mini = $this->site_model->chop_string($company_mini,200);

    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));   

    // var_dump($post_video);die();
    if(!empty($post_video))
    {
      $item = '<iframe height="200" src="https://www.youtube.com/embed/'.$post_video.'" frameborder="0" allowfullscreen></iframe>';
    }
    else
    {
      $item  =  $volunteer_image;
    }
    
  }
}

$volunteer_query = $this->site_model->get_active_content_items('Volunteer Opportunities');
$linked_page = '';
$volunteer_count = 0;
if($volunteer_query->num_rows() > 0)
{
  foreach($volunteer_query->result() as $row)
  {
    $volunteer_title = $row->post_title;
    $volunteer_post_id = $row->post_id;
    $web_name = $this->site_model->create_web_name($volunteer_title);
    $linked_page = base_url().'opportunities/'.$web_name.'/'.$volunteer_post_id;

    $volunteer_count = $this->site_model->get_applicants($volunteer_post_id);
  }
}
?>
<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_header;?>">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36"><?php echo $brief_title?></h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li class="active">Membership</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: About -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              <?php echo $gallery_item?>
            </div>
          </div>
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0">Membership</h3>
            <p><?php echo $membership_description?></p>
          </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- divider: Emergency Services -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2"  data-bg-img="<?php echo $image_header?>">
      <div class="container">
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-12">
              <!-- <h2 class="mt-0">Join Us</h2> -->
              <a class="btn btn-dark btn-theme-colored btn-lg btn-flat text-center pl-30 pr-30" href="<?php echo site_url().'membership-registration'?>">JOIN US TODAY</a>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- divider: Emergency Services -->
   
    <!-- divider: Emergency Services -->
        <!-- divider: Emergency Services -->
       <!-- divider: Emergency Services -->
    <!-- divider: Emergency Services -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6">
              <h3> MEMBERSHIP BENEFITS </h3>
              <div class="row mt-30 mb-30 ml-20">
                <?php echo $member_benefits_description;?>
               </div>
            </div>
            <div class="col-md-6">
              <h3>DONOR BILL OF RIGHTS</h3>
              <div class="row mt-30 mb-30 ml-20">
                <?php echo $donor_bills_description;?>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </section>
    

    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.5" data-bg-img="<?php echo $image_header?>">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-7">
              <h3 class="line-bottom">Become a Volunteer</h3>
              <p class="mt-30 mb-30"><?php echo $volunteer_description?></p>
              <a class="btn btn-dark btn-theme-colored btn-lg btn-flat pull-left pl-30 pr-30" href="<?php echo $linked_page;?>">Join Us</a>
            </div>
            <div class="col-md-5">
              <div class="fluid-video-wrapper">
                <?php echo $item?>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </section>
    <?php
      $partners_query = $this->site_model->get_active_items('Company Partners');
$partners_item = '';
if($partners_query->num_rows() > 0)
{
  $x=0;
  foreach($partners_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_partners = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));




   
  $partners_item .='<div class="item"> <a href="#" width="155" height="106"><img src="'.$image_partners.'" alt=""></a> </div>';
  }
}
    ?>


    <!-- Divider: Partners & Donors -->
    <section class="">
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-12">
            <div class="clients-logo carousel">
              <?php echo $partners_item;?>
            </div>
          </div>
        </div>
      </div>
    </section>