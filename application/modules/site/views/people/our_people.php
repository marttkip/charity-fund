<?php

$result = '';
if($query->num_rows() > 0)
{
	foreach($query->result() as $res)
	{
		$blog_category_id = $res->blog_category_id;
		$blog_category_name = $res->blog_category_name;
		if($blog_category_name != 'Our People')
		{
			$result .= '<h2>'.$blog_category_name.'</h2>';
		}
		
		$posts = $this->site_model->get_active_items($blog_category_name);
		if($posts->num_rows() > 0)
		{
			foreach($posts->result() as $row)
			{
				$brief_title = $row->post_title;
				$post_id = $row->post_id;
				$post_title = $row->post_title;
				$web_name = $this->site_model->create_web_name($post_title);
				$post_status = $row->post_status;
				$post_views = $row->post_views;
				$image_service = base_url().'assets/images/posts/'.$row->post_image;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$attachment_name = $row->attachment_name;
				$no_tags = strip_tags($description);
				$all_words = explode(' ', $no_tags);
				$total_words = count($all_words);
				$first_fifty = '';
				$word_count = 180;
				$r = $good_words = 0;
				while($r < $total_words)
				{
					$current_word = $all_words[$r];
					if(!empty($current_word))
					{
						$first_fifty .= $current_word.' ';
						$good_words++;
						
						if($good_words == $word_count)
						{
							break;
						}
					}
					$r++;
				}
				$mini_desc = $first_fifty;//implode(' ', array_slice(explode(' ', $mini_desc), 0, 50));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M',strtotime($created));
				$year = date('Y',strtotime($created));
				$created_on = date('jS M Y',strtotime($row->created));
		
				$image = '<img src="'.$image_service.'"  alt=""/>';
				
				if(!empty($attachment_name))
				{
					$attachment_name = '<a href="'.base_url().'assets/images/posts/'.$attachment_name.'" target="_blank">View Resume</a>';
				}
				if($post_title != 'Our People')
				{
					$result .= ' 
					<div class="row ">
						<div class="col-sm-12 col-md-8">
							<div class="news-title"> <h3>'.$post_title.'</h3></div>
							
							<div class="comment more" id="read_more'.$post_id.'">
								'.$mini_desc.' <button onclick="read_more('.$post_id.')">Read More</button>
							</div>
							
							<div class="comment less display_none" id="read_less'.$post_id.'">
								'.$description.' 
								'.$attachment_name.' 
								<button href="#" onclick="read_less('.$post_id.')">Read Less</button>
							</div>
						</div>
						<div class="col-sm-12 col-md-4">
							<div class="item" id="image-news">
								'.$image.'
							</div>
						</div>
					</div>
					<div class="brown"> </div>
					';
				}
		
			}
		}

	}
}
?>

<div class="content-wrapper-page">
		<!-- Inner Wrapper -->
	<section class="inner-wrapper about" id="about-co">
	  <div class="container" id="blog-content-item">
	  		 <?php echo $result;?> 
	  </div>
	</section>
	<!-- Footer Wrapper -->

</div>

<script type="text/javascript">
	function read_more(post_id)
	{
		$('#read_more'+post_id).delay(1500).addClass("display_none");
		$('#read_less'+post_id).delay(1500).removeClass("display_none");
	}
	function read_less(post_id)
	{
		$('#read_less'+post_id).delay(1500).addClass("display_none");
		$('#read_more'+post_id).delay(1500).removeClass("display_none");
	}
	
</script>