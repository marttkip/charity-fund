<?php
$decoded_webname = $this->site_model->decode_web_name($page_item);
$about_query = $this->site_model->get_active_content_items($decoded_webname);
// var_dump($about_query); die();
$about_item = '';
$result = '';
if($about_query->num_rows() > 0)
{
    foreach($about_query->result() as $row)
    {
        $brief_title = $row->post_title;
        $post_id = $row->post_id;
        $blog_category_name = $row->blog_category_name;
        $blog_category_id = $row->blog_category_id;
        $post_title = $row->post_title;
        $web_name = $this->site_model->create_web_name($post_title);
        $post_status = $row->post_status;
        $post_views = $row->post_views;
        $image_service = base_url().'assets/images/posts/'.$row->post_image;
        $created_by = $row->created_by;
        $modified_by = $row->modified_by;
        $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
        $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
        $description = $row->post_content;
        $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
        $created = $row->created;
        $day = date('j',strtotime($created));
        $month = date('M',strtotime($created));
        $year = date('Y',strtotime($created));
        $created_on = date('jS M Y',strtotime($row->created));

        $image = '<img src="'.$image_service.'"  alt=""/>';



    }
}
?>

 <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
  <div class="container pt-0 pb-0">
    <!-- Section Content -->
    <div class="section-content text-center">
      <div class="row"> 
        <div class="col-md-6 col-md-offset-3 text-center">
          <h2 class="text-theme-colored font-36"><?php echo $decoded_webname;?></h2>
          <ol class="breadcrumb text-center mt-10 white">
            <li><a href="<?php echo site_url().'home'?>">Home</a></li>
            <li class="active"><?php echo $decoded_webname;?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>      
</section> 

<section>
  <div class="container">
    <div class="row">
       <div class="col-md-10 col-md-push-1">
        <div id="section-one" class="mb-50">
            <?php echo $description;?>
        </div>
         
      </div>
    </div>
  </div>
</section>