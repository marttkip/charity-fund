<?php

  // $page_item = $this->site_model->decode_web_name($post_id);
  $query= $this->site_model->get_active_post_content($post_id);
   $result = '';
// var_dump($page_item); die();
  // var_dump($query->num_rows()); die();
   $image_header = 'http://placehold.it/1920x1280';
  if($query->num_rows() > 0)
  {

    foreach($query->result() as $row)
    {
      $brief_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_service = base_url().'assets/images/posts/'.$row->post_image;
      $image_header = base_url().'assets/images/posts/'.$row->post_header;
      $created_by = $row->created_by;
      $is_parent = $this->site_model->check_parent($blog_category_id);
      $modified_by = $row->modified_by;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));


      $gallery = $this->site_model->get_post_gallery($post_id);
      $gallery_item = '';
      if($gallery->num_rows() > 0)
      {
        foreach ($gallery->result() as $key => $value) {
          # code...
          $post_gallery_image_name = $value->post_gallery_image_name;
          $gallery_item .= '<div class="item">
                              <div class="thumb">
                                <img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" alt="">
                              </div>
                            </div>';
        }
      }

    }
                    }
?>


<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_header?>">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36"><?php echo $post_title;?></h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home';?>">Home</a></li>
                <li class="active"><?php echo $post_title?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: Services -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              ]<?php echo $gallery_item?>
            </div>
          </div>
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0"><?php echo $post_title?></h3>
            <p><?php echo $description?></p>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: Call To Action -->
  
    <?php
    $where = 'applicant_category = 1';
    $table = 'applications';
    $volunteers = $this->site_model->get_total_numbers($table,$where);


    $where = 'applicant_category = 1 AND application_type = 1';
    $table = 'applications';
    $conservancies = $this->site_model->get_total_numbers($table,$where);


    $where = 'applicant_category = 1 AND application_type <> 1';
    $table = 'applications';
    $schools = $this->site_model->get_total_numbers($table,$where);

    $category_id = $this->site_model->get_category_id('Company Initiatives');
    $where = 'post.post_status = 1 AND blog_category.blog_category_id = post.blog_category_id AND blog_category.blog_category_parent = '.$category_id.' ';
    $table = 'post,blog_category';
    $projects = $this->site_model->get_total_numbers($table,$where);

    ?>

 

    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-light" data-stellar-background-ratio="0.5" data-bg-img="<?php echo $image_header?>">
      <div class="container pt-90 pb-90">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-smile text-black-light mt-5 font-48 pull-right" data-text-color="#ccc"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="<?php echo $volunteers?>" data-animation-duration="2000">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Volunteers</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-hammer text-black-light mt-5 font-48 pull-right" data-text-color="#ccc"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="<?php echo $schools?>" data-animation-duration="2500">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Schools</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-magic-wand text-black-light mt-5 font-48 pull-right"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="<?php echo $conservancies?>" data-animation-duration="3000">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Conservancies</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-portfolio text-black-light mt-5 font-48 pull-right" data-text-color="#ccc"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="<?php echo $projects?>" data-animation-duration="2500">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Projects</h4>
            </div>
          </div>
        </div>
      </div>
    </section>