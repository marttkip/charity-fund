<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $email2 = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $linkedin = $contacts['linkedin'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        
        if(!empty($facebook))
        {
            $facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
        }
        
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
    }
?>
   <div class="home-content">
        <div class="top-nav">
            <div class="container">
                <div class="pull-left">
                    <img src="<?php echo base_url()."assets/logo/".$logo;?>" alt="<?php echo $company_name;?>" class="logo">
                </div>
                
               <!-- <div class="pull-right">
                    <a href="#">
                        <i class="fa fa-user fa-2x"></i>
                    </a>
                </div>-->
                
                <div class="title">
                    <h3><?php echo $company_name;?></h3>
                    <!--<p>Terra Firma Testing</p>-->
                </div>
            </div>
        </div>
        	
        <div id="owl-carousel" class="owl-carousel owl-theme owl-carousel-head">
            <?php
            if($slides->num_rows() > 0)
            {
                foreach($slides->result() as $slide)
                {
                    $slide_name = $slide->slideshow_name;
                    $description = $slide->slideshow_description;
                    $slide_image = $slide->slideshow_image_name;
                    $slideshow_link = $slide->slideshow_link;
                    $slideshow_button_text = $slide->slideshow_button_text;
                    //$description = $this->site_model->limit_text($description, 8);
                    $image = base_url().'assets/slideshow/'.$slide_image;
                    
                    ?>
                    <div class="item">
                        <div class="slider" style="background:url(<?php echo base_url();?>assets/slideshow/<?php echo $slide_image;?>) no-repeat center center fixed;">
                            <figure >
                                <figcaption>
                                    <h3><?php echo $slide_name;?></h3>
                                    <p>
                                        <?php echo $description;?>
                                        <span style="width:100%; margin:0 auto; text-align:center;">
                                            <a href="<?php echo $slideshow_link;?>" style="font-weight:bold;">Read More</a>
                                        </span>
                                    </p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                <?php
                }
            }
            ?>
        </div>

       <!--  <div class="slider" style="background:url(<?php echo base_url();?>assets/slideshow/<?php echo $slide_image;?>) no-repeat center center fixed;  -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover;"> -->
            
            <!--<ul class="slide-images">
            <?php
				if($slides->num_rows() > 0)
				{
					foreach($slides->result() as $slide)
					{
						$slide_name = $slide->slideshow_name;
						$description = $slide->slideshow_description;
						$slide_image = $slide->slideshow_image_name;
						$slideshow_link = $slide->slideshow_link;
						$slideshow_button_text = $slide->slideshow_button_text;
						$description = $this->site_model->limit_text($description, 8);
						$image = base_url().'assets/slideshow/'.$slide_image;
						
						?>
                        <li>
                        	<div class="slide-image-thumb">
                        		<img src="<?php echo base_url();?>assets/slideshow/<?php echo $slide_image;?>" alt=""/>
                            </div>
                            <p><a href="<?php echo site_url().$slideshow_link;?>"><?php echo $slide_name;?></a></p>
                        </li>
					<?php
					}
				}
			?>
            </ul>-->
	</div>
<div class="clearfix"></div>
<div class="row site-instructions">
	<div class="col-md-12">
    	<div class="container">
        	<a href="<?php echo site_url().'home';?>">PLEASE CLICK HERE FOR RECENT NEWS & UPDATES OR MENUS FOR SITE NAVIGATION</a>
        </div>
    </div>
</div>