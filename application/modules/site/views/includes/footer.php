<?php
$contacts = $this->site_model->get_contacts();
if(count($contacts) > 0)
{
	$email = $contacts['email'];
	$facebook = $contacts['facebook'];
	$linkedin = $contacts['linkedin'];
	$twitter = $contacts['twitter'];
	$logo = $contacts['logo'];
	$company_name = $contacts['company_name'];
	$phone = $contacts['phone'];
	$address = $contacts['address'];
	$post_code = $contacts['post_code'];
	$city = $contacts['city'];
	$building = $contacts['building'];
	$floor = $contacts['floor'];
	$location = $contacts['location'];

	$working_weekday = $contacts['working_weekday'];
	$working_weekend = $contacts['working_weekend'];
   $about = $contacts['about'];

   $mini_desc = implode(' ', array_slice(explode(' ', $about), 0, 40));



}


$projects_query = $this->site_model->get_active_items('Quick Links');
$projects_sub_menu_projects = '';
if($projects_query->num_rows() > 0)
{
  foreach($projects_query->result() as $res)
  {
    $post_title = $res->post_title;
    $post_id = $res->post_id;
    $web_name = $this->site_model->create_web_name($post_title);

    $projects_sub_menu_projects .= '<li><a href="'.site_url().'view-info/'.$web_name.'/'.$post_id.'">'.$post_title.'</a></li>';


  }
}


?>
<footer id="footer" class="footer pb-0" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pb-20">
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="<?php echo site_url().'assets/logo/'.$logo.''?>">
            <p class="font-12 mt-20 mb-10"><?php echo $mini_desc?></p>
            <a class="text-gray font-12" href="<?php echo site_url().'about-us';?>"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="styled-icons icon-dark mt-20">
              <li class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".1s" data-wow-offset="10"><a href="<?php echo $facebook;?>" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".2s" data-wow-offset="10"><a href="<?php echo $twitter;?>" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".4s" data-wow-offset="10"><a href="<?php echo $linkedin;?>" data-bg-color="#A11312"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Pages</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="<?php echo site_url().'home';?>">Home</a></li>
              <li><a href="<?php echo site_url().'about-us';?>">About Us</a></li>
              <li><a href="<?php echo site_url().'membership';?>">Membership</a></li>
              <li><a href="<?php echo site_url().'initiatives';?>">Initiatives</a></li>
              <li><a href="<?php echo site_url().'events';?>">Events</a></li>
              <li><a href="<?php echo site_url().'blog';?>">Blog</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <?php echo $projects_sub_menu_projects;?>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list-border">
              <li><a href="#"><?php echo $phone?></a></li>
              <li><a href="#"><?php echo $email?></a></li>
              <li><a href="#" class="lineheight-20"><?php echo $address;?> - <?php echo $post_code;?> <?php echo $location;?> <?php echo $building;?> <?php echo $floor;?></a></li>
            </ul>
            <p class="text-white mb-5 mt-15">Subscribe to our newsletter</p>
            <?php echo form_open("site/newsletter_subscription", array("class" => "newsletter-form mt-10"));?>
              <label class="display-block" for="mce-EMAIL"></label>
              <div class="input-group">
                <input type="email" name="email" value="" name="EMAIL" placeholder="Your Email"  class="form-control" data-height="37px" id="mce-EMAIL">
                <input name="uri_string" type="hidden" value="<?php echo $this->uri->uri_string()?>">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-colored btn-theme-colored m-0"><i class="fa fa-paper-plane-o text-white"></i></button>
                </span>
              </div>
            <?php echo form_close();?>
            <!-- Mailchimp Subscription Form Validation-->
            <script type="text/javascript">
              $('#footer-mailchimp-subscription-form').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#footer-mailchimp-subscription-form'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid bg-theme-colored p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="text-white font-11 m-0">Copyright &copy;<?php echo date('Y')?> <?php echo $company_name;?>. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>