<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $email2 = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $linkedin = $contacts['linkedin'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        $working_weekday = $contacts['working_weekday'];
        
       
        
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $wooking_weekday = '';
    }
?>

</header>

  <header class="header">
    <div class="header-top bg-deep sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <div class="widget no-border m-0">
              <ul class="styled-icons icon-sm sm-text-center">
                <li><a href="<?php echo $facebook?>"><i class="fa fa-facebook"></i></a></li>
                <li><a href="<?php echo $twitter;?>"><i class="fa fa-twitter"></i></a></li>
                <li><a href="<?php echo $linkedin?>"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="widget no-border m-0">
              <ul class="list-inline pull-right sm-pull-none sm-text-center mt-5">
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored"></i> <a class="text-gray" href="#"><?php echo $phone;?></a> </li>
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-clock-o text-theme-colored"></i> Mon-Fri <?php echo $working_weekday?> </li>
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored"></i> <a class="text-gray" href="#"><?php echo $email;?></a> </li>
                <li class="sm-display-block mt-sm-10 mb-sm-10">
                <?php 
                  $customer_login_status = $this->session->userdata('customer_login_status');
                    $applicant_id = $this->session->userdata('applicant_id');
                    $applicant_type = $this->session->userdata('applicant_type');
                    $applicant_login_status = $this->session->userdata('applicant_login_status');
                    if($applicant_login_status == TRUE AND $customer_login_status == FALSE)
                    {

                      if($applicant_type == 1)
                      {
                        ?>
                        <a class="btn btn-colored btn-flat btn-xs btn-theme-colored" href="<?php echo site_url().'conservancy/profile'?>" > <?php echo $this->session->userdata('applicant_name')?>'s Profile</a>
                        <?php

                      }
                      else
                      {
                        ?>
                        <a class="btn btn-colored btn-flat btn-xs btn-theme-colored" href="<?php echo site_url().'member/profile'?>" > <?php echo $this->session->userdata('applicant_name')?>'s Profile</a>
                        <?php
                      }
                        
                    }
                    else
                    {
                      if($customer_login_status == FALSE)
                      {
                        ?>
                        <a class="btn btn-primary btn-flat btn-xs " href="<?php echo site_url().'membership-registration'?>" > Institution Login</a>

                        <a class="btn btn-colored btn-flat btn-xs btn-theme-colored" href="<?php echo site_url().'conservation-registration'?>" > Conservancy Login</a>
                        <?php

                      }
                        

                    }
                    $customer_login_status = $this->session->userdata('customer_login_status');
                    $applicant_login_status = $this->session->userdata('applicant_login_status');
                                    
                    if($customer_login_status == TRUE AND $applicant_login_status == FALSE)
                    {
                      $first_name = $this->session->userdata('first_name');
                        echo '<a href="'.site_url().'customer/profile" class="btn btn-colored btn-flat btn-xs btn-theme-colored"> '.$first_name.'\'s Profile</a>';
                    }
                    else
                    {
                      if($applicant_login_status == FALSE)
                      {
                        echo ' <a class="btn btn-success btn-xs btn-flat" href="'.site_url().'login" >Member Login</a>';
                      }
                      else
                      {
                        $login = '';
                      }
                    }
                ?>
                  

                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
        <div class="container">
          <nav id="menuzord-right" class="menuzord orange bg-lightest">
            <a class="menuzord-brand" href="<?php echo site_url().'home'?>">
              <img src="<?php echo base_url()."assets/logo/logo-wide.png";?>" alt="">
            </a>
            <div id="side-panel-trigger" class="side-panel-trigger"><a href="#"><i class="fa fa-bars font-24 text-gray"></i></a></div>
            <ul class="menuzord-menu">
                <?php echo $this->site_model->get_navigation();;?>
            </ul>
          </nav>
        </div>
      </div>
    </div>
</header>
