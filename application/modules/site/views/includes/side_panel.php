<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $email2 = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $linkedin = $contacts['linkedin'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        
       
        
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
    }
?>
<div class="body-overlay"></div>
<div id="side-panel" class="dark" data-bg-img="http://placehold.it/1920x1280">
  <div class="side-panel-wrap">
    <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon_close font-30"></i></a></div>
    <a href="javascript:void(0)"><img alt="logo" src="<?php echo base_url()."assets/logo/logo-wide.png";?>"></a>
    <div class="side-panel-nav mt-30">
      <div class="widget no-border">
        <nav>
          <ul class="nav nav-list">
            <?php echo $this->site_model->get_side_bar()?>
          </ul>
        </nav>        
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="side-panel-widget mt-30">
      <div class="widget no-border">
        <ul>
          <li class="font-14 mb-5"> <i class="fa fa-phone text-theme-colored"></i> <a href="#" class="text-gray"> <?php echo $phone;?></a> </li>
          <li class="font-14 mb-5"> <i class="fa fa-clock-o text-theme-colored"></i> Mon-Fri 8:00 to 2:00 </li>
          <li class="font-14 mb-5"> <i class="fa fa-envelope-o text-theme-colored"></i> <a href="#" class="text-gray"><?php echo $email;?></a> </li>
        </ul>      
      </div>
      <div class="widget">
        <ul class="styled-icons icon-dark icon-theme-colored icon-sm">
          <li><a href="<?php echo $linkedin;?>"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="<?php echo $facebook;?>"><i class="fa fa-facebook"></i></a></li>
          <li><a href="<?php echo $twitter;?>"><i class="fa fa-twitter"></i></a></li>
        </ul>
      </div>
      <p>Copyright &copy;<?php echo date('Y')?></p>
    </div>
  </div>
</div>