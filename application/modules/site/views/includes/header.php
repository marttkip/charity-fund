<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="CharityFund - Charity & Crowdfunding HTML Template" />
    <meta name="keywords" content="building,business,construction,cleaning,transport,workshop" />
    <meta name="author" content="Martin" />

    <!-- Page Title -->
    <title><?php echo $title;?></title>

    <!-- Favicon and Touch Icons -->
    <link href="<?php echo base_url()."assets/";?>images/favicon.png" rel="shortcut icon" type="image/png">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>images/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

    <!-- Stylesheet -->
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/css-plugin-collections.css" rel="stylesheet"/>
    <!-- CSS | menuzord megamenu skins -->
    <link id="menuzord-menu-skins" href="<?php echo base_url()."assets/themes/charity-fund/";?>css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
    <!-- CSS | Main style file -->
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/style-main.css" rel="stylesheet" type="text/css">
    <!-- CSS | Preloader Styles -->
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/preloader.css" rel="stylesheet" type="text/css">
    <!-- CSS | Custom Margin Padding Collection -->
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
    <!-- CSS | Responsive media queries -->
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/responsive.css" rel="stylesheet" type="text/css">
    <!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
    <!-- <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/style.css" rel="stylesheet" type="text/css"> -->

    <!-- Revolution Slider 5.x CSS settings -->
    <link  href="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
    <link  href="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
    <link  href="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

    <!-- CSS | Theme Color -->
    <link href="<?php echo base_url()."assets/themes/charity-fund/";?>css/colors/theme-skin-yellow.css" rel="stylesheet" type="text/css">

    <!-- external javascripts -->
    <script src="<?php echo base_url()."assets/themes/";?>js/jquery-2.2.0.min.js"></script>
    <script src="<?php echo base_url()."assets/themes/";?>js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url()."assets/themes/";?>js/bootstrap.min.js"></script> 
    <!-- JS | jquery plugin collection for this theme -->
    <script src="<?php echo base_url()."assets/themes/";?>js/jquery-plugin-collection.js"></script>

    
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/summernote/dist/summernote.css" />
<link rel="stylesheet" href="<?php echo base_url()."assets/themes/ubiker/";?>vendor/summernote/dist/summernote-bs3.css" />

    <!-- Revolution Slider 5.x SCRIPTS -->
    <script src="<?php echo base_url()."assets/themes/";?>js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url()."assets/themes/";?>js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>