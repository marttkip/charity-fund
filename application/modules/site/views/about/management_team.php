<?php
$services_query = $this->site_model->get_active_items('Management Team');
$services_items = '';
if($services_query->num_rows() > 0)
{
	foreach($services_query->result() as $row)
	{
		$post_title = $row->post_title;
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		$year = date('Y',strtotime($created));
		$created_on = date('jS M Y',strtotime($row->created));
		// $post_video = $row->post_video;
		
		// if(empty($post_video))
		// {
		// 	$image = '<img src="'.$image.'" class="img-responsive " alt=""/>';
		// }
		
		// else
		// {
		// 	$image = '<div class="youtube" id="'.$post_video.'" class="img-responsive "></div>';
		// }

		$image = '<img src="'.$image.'" class="img-responsive " alt=""/>';
		



		$services_items .= '<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="team-member">
									<div class="member-thumb">
										'.$image.'
									</div>
									<div class="member-info">
										<h3>'.$post_title.'</h3>
										<span>'.$description.'</span>
									</div>
								</div>
							</div>';
	}
}


?>

<!-- team-wrap-->
	<section class="team-wrap section-padding">
		<div class="container">
			<div class="section-heading">
				<h2 class="section-title">Our crew</h2>
			</div>
			

			<div class="row">
				<?php echo $services_items;?>
			</div><!-- /.row -->

		</div><!-- /.container -->
	</section>
	<!-- /.team-wrap -->