<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $email2 = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $linkedin = $contacts['linkedin'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        $about = $contacts['about'];
         $mission = $contacts['mission'];
        // $about = $contacts['about'];
        
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $about = '';
        $mission = '';
    }

$partners_query = $this->site_model->get_active_items('Company Partners');
$partners_item = '';
if($partners_query->num_rows() > 0)
{
  $x=0;
  foreach($partners_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_partners = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));




   
  $partners_item .='<div class="item"> <a href="#"><img src="'.$image_partners.'" alt=""></a> </div>';
  }
}



$team_query = $this->site_model->get_active_items('Company Team');
$team_item = '';
if($team_query->num_rows() > 0)
{
  $x=0;
  foreach($team_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_team_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($company_team_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $company_team_image = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_team_description = $row->post_content;
    $company_team_mini_description = implode(' ', array_slice(explode(' ', $company_team_description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));




   
  $team_item .=' <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
                  <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                    <div class="thumb"><img alt="" src="'.$company_team_image.'" class="img-fullwidth"></div>
                    <div class="info">
                      <h4 class="name"><a href="#">'.$company_team_title.'</a></h4>
                      <p>'.$company_team_description.'</p>
                      <hr>
                      <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>';
  }
}

$about_query = $this->site_model->get_active_items('About Us');
$about_item = '';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $image_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $about_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $about_description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));


    $gallery = $this->site_model->get_post_gallery($post_id);
    $gallery_item = '';
    if($gallery->num_rows() > 0)
    {
      foreach ($gallery->result() as $key => $value) {
        # code...
        $post_gallery_image_name = $value->post_gallery_image_name;
        $gallery_item .= '<div class="item">
                            <div class="thumb">
                              <img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" alt="">
                            </div>
                          </div>';
      }
    }

   
  }
   $about_item .=$about_description;
}



$about_query = $this->site_model->get_active_items('Word From The CEO');
$ceo_word_description = '';
$ceo_word_image = '';
$ceo_word_header = 'http://placehold.it/1920x1280';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $ceo_word_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($ceo_word_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $ceo_word_image = base_url().'assets/images/posts/'.$row->post_image;
    $ceo_word_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $ceo_word_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $ceo_word_description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));   
  }
}


?>
 <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_header?>">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">ABOUT US</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li class="active">About</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: About -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0">About Us</h3>
            <p><?php echo $about_item;?> </p>  
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              <?php echo $gallery_item;?>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- divider: Emergency Services -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2"  data-bg-img="<?php echo $image_header?>">
      <div class="container">
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-12">
              <h2 class="mt-0">Need any help </h2>
              <h3>Write to us at  <span class="text-theme-colored"><?php echo $email?></span></h3>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- divider: Emergency Services -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-7">
              <p></p>
            </div>
            <div class="col-md-5">
              <p></p>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- divider: Became a Volunteers -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.5" data-bg-img="<?php echo $ceo_word_header;?>">
      <div class="container">
        <div class="section-content">
          <div class="row">
             <div class="col-md-5">
              <div class="fluid-parallax">
                <img src="<?php echo $ceo_word_image;?>" alt="">
              </div>
            </div>
            <div class="col-md-7">
              <h3 class="line-bottom">Word from the CEO</h3>
              <p class="mt-30 mb-30"><?php echo $ceo_word_description;?></p>
             
            </div>
           
          </div>
        </div>
      </div>      
    </section>


     <!-- Section: Volunteer -->

    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2" data-bg-img="<?php echo $image_header?>">
      <div class="container pb-30">
        <div class="section-content">
           <div class="col-md-12">
              <h3 class="text-theme-colored text-uppercase mt-0">Our Team</h3>
           </div>
          <div class="row multi-row-clearfix">
           <?php echo $team_item;?>
            
          </div>
        </div>
      </div>
    </section>



      <section class="">
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-12">

              <h3 class="text-theme-colored text-uppercase mt-0">Our Partners</h3>
          
            <div class="clients-logo carousel">
              <?php echo $partners_item;?>
            </div>
          </div>
        </div>
      </div>
    </section>