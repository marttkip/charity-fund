<?php

$result = '';
//echo $query->num_rows(); die();
if($query->num_rows() > 0)
{
	foreach($query->result() as $row)
	{
		$brief_title = $row->post_title;
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image_service = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		$year = date('Y',strtotime($created));
		$image = '<img src="'.$image_service.'"  alt=""/>';

		if($page_item == "Professional-Affiliations")
		{
			$created_on = "";
			$result .= ' <div class="row professional-affiliations">
				      <div class="col-sm-12 col-md-8">
				      	<div class="news-title"> <h3>'.$post_title.'</h3></div>
				        <p>'.$description.'</p>
				      </div>
				      <div class="col-sm-12 col-md-4">
				          <div class="item" id="image-news">
				          	'.$image.'
				          </div>
				          
				      </div>
				    </div>
				    <div class="brown"> </div>
				    ';
		}
		else
		{
			$created_on = date('F d, Y',strtotime($row->post_date));
			$result .= ' <div class="row ">
				      <div class="col-sm-12 col-md-8">
				      	<div class="news-title"> <h3>'.$post_title.'</h3></div>
				        <p>'.$created_on.'</p>
				        <p>'.$description.'</p>
				      </div>
				      <div class="col-sm-12 col-md-4">
				          <div class="item" id="image-news">
				          	'.$image.'
				          </div>
				          
				      </div>
				    </div>
				    <div class="brown"> </div>
				    ';
		}
	}
}
?>

<div class="content-wrapper-page">
		<!-- Inner Wrapper -->


		
	<section class="inner-wrapper about" id="about-co">
	  <div class="container" id="blog-content-item">
	  	<?php
		if($page_item == "Client-Reviews" )
		{
			$review_details = $this->site_model->get_customer_review_header(272);
			if($review_details->num_rows() > 0)
			{
				$review = $review_details->row();
				$review_title = $review->post_title;
				$review_description = $review->post_content;
			}
			?>

				<div class="row ">
			      <div class="col-sm-12">
			        <p><?php echo $review_description;?></p>
			          
			      </div>
			    </div>
			<?php
		}
		?>
	    <?php echo $result;?>
	  </div>
	</section>
	<!-- Footer Wrapper -->

</div>