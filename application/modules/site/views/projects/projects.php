<?php

  $page_item = $this->site_model->decode_web_name($page_item);
  $query= $this->site_model->get_active_content_items($page_item);
   $result = '';
   $image_header = '';
  if($query->num_rows() > 0)
  {

    foreach($query->result() as $row)
    {
      $brief_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_service = base_url().'assets/images/posts/'.$row->post_image;
      $image_header = base_url().'assets/images/posts/'.$row->post_header;
      $created_by = $row->created_by;
      $is_parent = $this->site_model->check_parent($blog_category_id);
      $modified_by = $row->modified_by;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));


      $gallery = $this->site_model->get_post_gallery($post_id);
      $gallery_item = '';
      if($gallery->num_rows() > 0)
      {
        foreach ($gallery->result() as $key => $value) {
          # code...
          $post_gallery_image_name = $value->post_gallery_image_name;
          $gallery_item .= '<div class="item">
                              <div class="thumb">
                                <img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" alt="">
                              </div>
                            </div>';
        }
      }

    }
                    }
?>


<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36"><?php echo $post_title;?></h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home';?>">Home</a></li>
                <li class="active"><?php echo $post_title?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: Services -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              ]<?php echo $gallery_item?>
            </div>
          </div>
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0"><?php echo $post_title?></h3>
            <p><?php echo $description?></p>
          </div>
        </div>
      </div>
    </section>
    
  