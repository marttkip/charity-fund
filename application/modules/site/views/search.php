<?php

$total_posts = count($all_posts);
$result = '<h2>'.$search.' Results</h2>';
//echo $total_posts; die();
if($total_posts > 0)
{
	for($r = 0; $r < $total_posts; $r++)
	{
		$hit_ratio = $all_posts[$r]['hit_ratio'];
		$post_id = $all_posts[$r]['post_id'];
		$post_title = $all_posts[$r]['post_title'];
		$post_content = $all_posts[$r]['post_content'];
		$blog_category_name = $all_posts[$r]['blog_category_name'];
		$blog_category_id = $all_posts[$r]['blog_category_id'];
		$post_image = $all_posts[$r]['post_image'];
		$web_name = $this->site_model->create_web_name($post_title);
		$blog_category_name_old = $this->site_model->create_web_name($blog_category_name);
		$image_service = base_url().'assets/images/posts/'.$post_image;
		$no_tags = strip_tags($post_content);
		$all_words = explode(' ', $no_tags);
		$total_words = count($all_words);
		$first_fifty = '';
		$word_count = 50;
		$s = $good_words = 0;
		while($s < $total_words)
		{
			$current_word = $all_words[$s];
			if(!empty($current_word))
			{
				$first_fifty .= $current_word.' ';
				$good_words++;
				
				if($good_words == $word_count)
				{
					break;
				}
			}
			$s++;
		}
		$mini_desc = $first_fifty;//implode(' ', array_slice(explode(' ', $mini_desc), 0, 50));
		$image = '<img src="'.$image_service.'"  alt=""/>';
		
		//highlight text
		$post_title = $this->site_model->highlight_text($post_title, $search);
		$mini_desc = $this->site_model->highlight_text($mini_desc, $search);
		$blog_category_name = $this->site_model->highlight_text($blog_category_name, $search);


		// get the url for the ite,

		// check if this category has a parent
		// $post_checked1 ='';
		// $post_checked2 ='';
		// $post_checked3 ='';
		// $post_checked4 ='';
		// // var_dump($blog_category_id); die();
		// $blog_parent = $this->site_model->get_blog_category_parents($blog_category_id);
		// if($blog_parent->num_rows() > 0)
		// {
		// 	foreach ($blog_parent->result() as $key) {
		// 		# code...
		// 		$blog_category_parent_id0 = $key->blog_category_parent;
		// 		$blog_category_name0 = $key->blog_category_name;
		// 		$web_name0 = $this->site_model->create_web_name($blog_category_name0);
				

		// 		if($web_name0 == "Services-List")
		// 		{
		// 			$post_checked4 = 'our-services';
		// 		}
		// 		else if($web_name0 == "About-Us")
		// 		{

		// 			$post_checked4 = 'about-us';
		// 		}
		// 		else
		// 		{
		// 			$post_checked1 = $web_name0;
		// 		}

		// 		$blog_parent1 = $this->site_model->get_blog_category_parents($blog_category_parent_id0);
		// 		if($blog_parent1->num_rows() > 0)
		// 		{
		// 			foreach ($blog_parent1->result() as $key1) {
		// 				# code...
		// 				$blog_category_parent_id1 = $key1->blog_category_parent;
		// 				$blog_category_name1 = $key1->blog_category_name;
		// 				$web_name1 = $this->site_model->create_web_name($blog_category_name1);
					

		// 				if($web_name1 == "Services-List")
		// 				{
		// 					$post_checked4 = 'our-services';
		// 				}
		// 				else if($web_name1 == "About-Us")
		// 				{

		// 					$post_checked4 = 'about-us';
		// 				}
		// 				else if($web_name1 == "Our-Project")
		// 				{

		// 					$post_checked4 = 'our-projects';
		// 				}
		// 				else
		// 				{
		// 					$post_checked1 = $web_name1;
		// 				}

		// 				$blog_parent2 = $this->site_model->get_blog_category_parents($blog_category_parent_id1);
		// 				if($blog_parent2->num_rows() > 0)
		// 				{
		// 					foreach ($blog_parent1->result() as $key2) {
		// 						# code...
		// 						$blog_category_parent_id2 = $key2->blog_category_parent;
		// 						$blog_category_name2 = $key2->blog_category_name;

		// 						$web_name2 = $this->site_model->create_web_name($blog_category_name2);
		// 						$post_checked3 .= '/'.$web_name2;
		// 						$blog_parent3 = $this->site_model->get_blog_category_parents($blog_category_parent_id2);
		// 						if($blog_parent3->num_rows() > 0)
		// 						{
		// 							foreach ($blog_parent3->result() as $key3) {
		// 								# code...
		// 								$blog_category_parent_id3 = $key3->blog_category_parent;
		// 								$blog_category_name3 = $key3->blog_category_name;

										
		// 							}
		// 							$web_name = $this->site_model->create_web_name($blog_category_name3);
		// 							if($web_name == "Services-List")
		// 							{
		// 								$post_checked4 = 'our-services';
		// 							}
		// 							else
		// 							{
		// 								$post_checked4 = $web_name;
		// 							}

		// 						}
		// 					}

		// 				}
		// 			}
		// 		}
		// 	}
		// }
		// $post_checked = $post_checked4.$post_checked3.$post_checked2.$post_checked1;

		// var_dump($post_checked); die();
		
		$result .= ' 
		<div class="row ">
			<div class="col-sm-12 col-md-8">
				<div class="news-title"> <h3><a href="'.site_url().'search-result/'.$web_name.'" >'.$post_title.' </a></h3></div>
				
				<div class="comment more" id="read_more'.$post_id.'">
					<h4>'.$blog_category_name.'</h4>
					'.$mini_desc.' <a href="'.site_url().'search-result/'.$web_name.'">Read More</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="item" id="image-news">
					'.$image.'
				</div>
			</div>
		</div>
		<div class="brown"> </div>
		';
	}
}

else
{
	$result .= '
		<div class="row ">
			<div class="col-sm-12 col-md-8">
				<div class="news-title"> <h3>You have not entered a search query</h3></div>
			</div>
		</div>';
}
?>
<div class="content-wrapper-page">
		<!-- Inner Wrapper -->
	<section class="inner-wrapper about" id="about-co">
	  <div class="container" id="blog-content-item">
	  		 <?php echo $result;?> 
	  </div>
	</section>
	<!-- Footer Wrapper -->

</div>