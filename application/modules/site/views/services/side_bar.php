<?php
$services_query = $this->site_model->get_active_items('Services List');
$services_items = '';
if($services_query->num_rows() > 0)
{
	foreach($services_query->result() as $row)
	{
		$post_title = $row->post_title;

		$web_name = $this->site_model->create_web_name($post_title);
		$services_items .='<li><a href="'.site_url().'services/'.$web_name.'">'.$post_title.'</a></li>';
	}
}

?>
<div class="sidebar-wrapper">

    <div class="widget">
        <h2 class="widget-title">Services</h2>
        
        <ul class="service-list widget-arrow-list">                             
           <?php echo $services_items;?>                          


        </ul>
        
    </div><!-- /.widget -->


    <div class="widget">
        <div class="download-wrap company">
            <a href="#">Company profile <span>Download</span><i class="fa fa-download"></i></a>
        </div> <!-- /.download-wrap -->
        <div class="download-wrap annual">
            <a href="#">Annual Report <span>Download</span><i class="fa fa-download"></i></a>
        </div> <!-- /.download-wrap -->
       
    </div><!-- /.widget -->


                      
</div>