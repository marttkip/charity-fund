<?php
$services_query = $this->site_model->get_active_items('Services Brief');
$services_items = '';
if($services_query->num_rows() > 0)
{
	foreach($services_query->result() as $row)
	{
		$brief_title = $row->post_title;
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image_service = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		$year = date('Y',strtotime($created));
		$created_on = date('jS M Y',strtotime($row->created));
	}
}
?>
<!--Service-intro-start -->
	<section class="service-intro-wrap section-padding">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="service-intro-thumb text-center">
						<img src="<?php echo $image_service;?>" alt="service">

					</div>
				</div><!--/.col-->
				<div class="col-sm-6">

					<div class="service-intro-content">
						<div class="section-heading">
							<h2 class="section-title"><?php echo $brief_title;?></h2>
						</div>
						<p><?php echo $description;?></p>
					</div><!--/.service-intro-content-->

				</div><!--/.col-->
			</div><!--/.row -->
		</div><!--/.container-->

	</section>
	<!--/Service-intro-end -->


<?php
$services_query = $this->site_model->get_active_items('Services List');
$services_items = '';
if($services_query->num_rows() > 0)
{
	foreach($services_query->result() as $row)
	{
		$post_title = $row->post_title;
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		$year = date('Y',strtotime($created));
		$created_on = date('jS M Y',strtotime($row->created));
		// $post_video = $row->post_video;
		
		// if(empty($post_video))
		// {
		// 	$image = '<img src="'.$image.'" class="img-responsive " alt=""/>';
		// }
		
		// else
		// {
		// 	$image = '<div class="youtube" id="'.$post_video.'" class="img-responsive "></div>';
		// }

		$image = '<img src="'.$image.'" class="img-responsive " alt=""/>';
		



		$services_items .= '<div class="col-sm-4">
								<div class="featured-service">
									<div class="featured-service-thumb">
										
										'.$image.'
									</div>
									<div class="featured-service-content">
										<h3>'.$post_title.'</h3>
										<p>'.$mini_desc.'</p>
										<a class="btn btn-primary readmore" href="service-single.html">Continue reading<i class="fa fa-long-arrow-right"></i></a>
									</div>
								</div>
						
							</div>';
	}
}


?>


	<!-- Featured-service-start -->
	<section class="featured-service-wrap section-padding">
		<div class="container">
			<div class="section-heading">
				<h2 class="section-title">Featured services</h2>
			</div>
			<div class="row">
				<?php echo $services_items;?>
			</div><!-- /.row -->
		</div><!-- /.container -->

	</section>
	<!-- Featured-service-end -->