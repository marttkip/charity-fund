<?php

$result = '';
if($query->num_rows() > 0)
{
	foreach($query->result() as $row)
	{
		$brief_title = $row->post_title;
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$post_status = $row->post_status;
		$post_views = $row->post_views;
		$image_service = base_url().'assets/images/posts/'.$row->post_image;
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		
		if($page_item == 'our-services')
		{
			$parent_search_title = 'Services List';
		}
		else
		{
			$parent_search_title = $post_title;
		}
		$is_parent = $this->site_model->check_parent($parent_search_title);
		//var_dump($parent_search_title); die();
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M',strtotime($created));
		$year = date('Y',strtotime($created));
		$created_on = date('jS M Y',strtotime($row->created));
		$post_gallery = $this->site_model->get_post_gallery($post_id);
		$description = str_replace("<p> </p>", "", $description);
		$gallery_images = '';
		
		if($post_gallery->num_rows() > 0)
		{
			$gallery_images .= '<div id="owl-carousel-gallery" class="owl-carousel owl-theme">';
			foreach($post_gallery->result() as $res)
			{
				$post_gallery_image_name = $res->post_gallery_image_name;
				$gallery_images .= '
					<div class="item">
						<img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" />
					</div>
				';
			}
			$gallery_images .= '</div>';
		}
		
		else
		{
			$gallery_images = '<img src="'.$image_service.'"  alt=""/>';
		}
		
		
		if($is_parent)
		{
			$page = explode("/",uri_string());
			$total = count($page);
			
			$name = strtolower($page[0]);
			$name2 = $name3 = '';
			$title = $name;
			if(isset($page[1]))
			{
				$name2 = $page[1];
				$title = $name;
			}
			if(isset($page[2]))
			{
				$name3 = $page[2];
				$title = $name2;
			}
			if(isset($page[3]))
			{
				$name4 = $page[3];
				$title = $name3;
			}
			
			if($page_item == 'our-services')
			{
				$category_search = 'Services List';
				$name = 'our-services';
			}
			else
			{
				$category_search = $post_title;
			}
			$services_query = $this->site_model->get_active_items($category_search);
			$services_sub_menu_services = '';
			if($services_query->num_rows() > 0)
			{
				foreach($services_query->result() as $res)
				{
					$post_title2 = $res->post_title;
					$web_name = $this->site_model->create_web_name($post_title2);
					if(!empty($name4))
					{
						$services_sub_menu_services .= '<li><a href="'.site_url().$name.'/'.$name2.'/'.$name3.'/'.$name4.'/'.$web_name.'">'.$post_title2.'</a></li>';
					}
					else if(!empty($name3))
					{
						$services_sub_menu_services .= '<li><a href="'.site_url().$name.'/'.$name2.'/'.$name3.'/'.$web_name.'">'.$post_title2.'</a></li>';
					}
					else
					{
						$services_sub_menu_services .= '<li><a href="'.site_url().$name.'/'.$name2.'/'.$web_name.'">'.$post_title2.'</a></li>';
					}
					//$services_sub_menu_services .= '<li><a href="'.site_url().'our-services/Geo-Technical-Engineering/'.$web_name.'">'.$post_title.'</a></li>';
				}
			}
			$result .= ' 
				<div class="row ">
					<div class="col-sm-12 col-md-6">
						<!--<div class="news-title"> <h3>'.$post_title.'</h3></div>-->
						'.$description.'
					</div>';
					
			
			if($post_title == 'Construction Management')
			{
				$post_title = 'Other Geotechnical Services';
			}
			$result .= ' 
					<div class="col-sm-12 col-md-6">
						<div class="item">
							<p>Click on the categories below for more in-depth knowledge about our '.$post_title.'</p>
							<ul>
							'.$services_sub_menu_services.'
							</ul>
						</div>
					</div>
				</div>
			';
		}
		
		else
		{
			$result .= ' 
				<div class="row ">
					<div class="col-sm-12 col-md-6">
						<!--<div class="news-title"> <h3>'.$post_title.'</h3></div>-->
						'.$description.'
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="item" id="image-news">
							'.$gallery_images.'
						</div>
					</div>
				</div>
			';
		}
	
	}
}
?>



<div class="content-wrapper-page">
    <!-- Inner Wrapper -->
    <section class="inner-wrapper about" id="about-co">
        <div class="container" id="blog-content-item">
        	<div class="inner-breadcrumb" style="margin-top:0;">
                <div class="container">
                    <ul>
                        <?php echo $this->site_model->get_breadcrumbs();?>
                    </ul>
               	</div>
            </div>
            <?php echo $result;?>
        </div>
    </section>
</div>