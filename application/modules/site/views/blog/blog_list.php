 <!-- Section: inner-header -->
  <?php

$company_events_query = $this->site_model->get_active_items('Company Blog');
$company_events_item = '';
$initiative_header  ='http://placehold.it/1920x1280';
if($company_events_query->num_rows() > 0)
{
  $x=0;
  foreach($company_events_query->result() as $row)
  {
    $company_events_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_events_name = $row->post_title;
    $web_name = $this->site_model->create_web_name($company_events_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $initiative_image = base_url().'assets/images/posts/'.$row->post_image;
    $initiative_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_events_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $company_events_description), 0, 40));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));


   $company_events_item .= '<div class="col-sm-12 col-md-12 blog-pull-right">
                                  <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="schedule-box maxwidth500 bg-light mb-30">
                                      <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="'.$initiative_image.'">
                                        <div class="overlay">
                                          <a href="#"><i class="fa fa-calendar mr-5"></i></a>
                                        </div>
                                      </div>
                                      <div class="schedule-details clearfix p-15 pt-10">
                                        <h5 class="font-16 title"><a href="'.site_url().'view-post/'.$web_name.'/'.$post_id.'">'.$company_events_name.'</a></h5>
                                        <ul class="list-inline font-11 mb-20">
                                          <li><i class="fa fa-calendar mr-5"></i> '.$created_on.'</li>
                                        </ul>
                                        <p>'.$mini_desc.'</p>
                                        <div class="mt-10">
                                         <a href="'.site_url().'view-post/'.$web_name.'/'.$post_id.'" class="btn btn-dark btn-sm mt-10">View Blog</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>         
                                  
                                </div>';


   
  }
}
?>
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h3 class="text-theme-colored font-36"><?php echo $title?></h3>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li class="active"><?php echo $title?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: event calendar -->
    <section>
      <div class="container">
        <div class="row">
          
          <?php echo $company_events_item?>
        </div>
      </div>
    </section>