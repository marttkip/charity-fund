<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        $address = $contacts['address'];
        $post_code = $contacts['post_code'];
        $city = $contacts['city'];
        $building = $contacts['building'];
        $floor = $contacts['floor'];
        $location = $contacts['location'];

        $working_weekday = $contacts['working_weekday'];
        $working_weekend = $contacts['working_weekend'];

        $mission = $contacts['mission'];
        $vision = $contacts['vision'];
        $about = $contacts['about'];
        $core_values = $contacts['core_values'];
    }
?>

  <?php
        //if users exist display them
        $item_data = "";
        if ($items->num_rows() > 0)
        {   
           $counter = 0;
           
            foreach ($items->result() as $row)
            {
                $counter++;
                $post_id = $row->post_id;
                $blog_category_name = $row->blog_category_name;
                $blog_category_id = $row->blog_category_id;
                $post_title = $row->post_title;
                $web_name = $this->site_model->create_web_name($post_title);
                $post_status = $row->post_status;
                $post_views = $row->post_views;
                $image = base_url().'assets/images/posts/'.$row->post_image;
                if($row->post_image == "" || $row->post_image == NULL)
                {
                    // $image ="http://placehold.it/450x250?text=Comparison+graph";
                    $image = base_url().'assets/themes/metal/img/sections/bg/intro.jpg';
                }
                $created_by = $row->created_by;
                $modified_by = $row->modified_by;
                $description = $row->post_content;
                $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                $created = $row->created;
                $day = date('j',strtotime($created));
                $month = date('M Y',strtotime($created));
                $created_on = date('jS M Y',strtotime($row->created));
                
                $categories = '';
                $item_data ='<p class="description upper">'.$description.'</p>';
            }
        }
        ?>


<!--Why Choose Section Start-->
<section class="cp-why-choose-section2 pd-tb80">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <figure class="cp-thumb choose-thumb">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/about-img1.png" alt="" class="img-circle">
                </figure>
            </div>
            <div class="col-md-5 col-sm-12">
                <!--Why Choose Text-->
                <div class="cp-why-choose-text">
                    <h3>Why Choose</h3>
                    <h2> <span>tAXIGO</span> for taxi hire </h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly or randomised words which don't  believable. </p>
                    <ul class="cp-choose-list">
                        <li>10 Years on the Global Market</li>
                        <li>12 Years on the Global Market</li>
                        <li>14 Years on the Global Market</li>
                        <li>16 Years on the Global Market</li>
                        <li>18 Years on the Global Market</li>
                        <li>20 Years on the Global Market</li>
                    </ul>
                </div><!--Why Choose Text-->
            </div>
        </div>
    </div>
</section><!--Why Choose Section End-->

<!--Services Section Start-->
<section class="cp-services-section pd-tb80">
    <div class="container">
         <div class="cp-heading-style1">
            <h2>Our Best <span>Services</span></h2>
        </div>
        <!--Why Choose Listed-->
        <ul class="cp-why-choose-listed">
            <li>
                <div class="cp-box">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/why-choose-img-01.png" alt="">
                    <h3>Inclusive Rates</h3>
                    <span class="icon-cash19 icomoon"></span>
                    <p> We offers Fully Inclusive car hire rates.There are no additional insurances that you need to purchase locally.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
            </li>
             <li>
                <div class="cp-box cp-box2">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/why-choose-img-03.png" alt="">
                    <h3>Easy Searching</h3>
                    <span class="icon-transport1105 icomoon"></span>
                    <p> We offers Fully Inclusive car hire rates.There are no additional insurances that you need to purchase locally.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
            </li>
            <li>
                <div class="cp-box">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/why-choose-img-02.png" alt="">
                    <h3>Home Pickup</h3>
                    <span class="icon-house158 icomoon"></span>
                    <p> We offers Fully Inclusive car hire rates.There are no additional insurances that you need to purchase locally.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
            </li>
        </ul><!--Why Choose Listed End-->
    </div>
</section><!--Services Section End-->

<!--Our Awwards Section Start-->
<section class="cp-awwards-section pd-tb80">
    <div class="container">
         <div class="cp-heading-style2">
            <h2>Our <span>Awwards</span></h2>
        </div>
        <!--Awwards innner Outer Start-->
        <div class="cp-inner-awwards-holder">
            <ul>
                <li><span>BritieyTravel Awards</span> - Car Hire Company of the year 2012 & 2013</li>
                <li><span>Travel Awards night</span> - Best Travel and Tourism iPhone/Smart Phone App 2015</li>
                <li><span>Travolution Awards </span> - Best Car Hire Company 2007, 2009, 2010 & 2015</li>
                <li><span>Travolution Awards</span> - Best Mobile Applcation 2015</li>
                <li><span>Travolution Awards </span> - Best Car Hire Website 2007 & 2009</li>
            </ul>  
        </div><!--Awwards innner Outer End-->
    </div>
</section><!--Our Awwards Section End-->

<!--Driver Section Start-->
<section class="cp-driver-section pd-tb80">
    <div class="container">
        <div class="cp-heading-style1">
            <h2>Drivers <span>For Hire</span></h2>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!--Driver Box Start-->
                <figure class="cp-driver-box">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/driver-img-01.jpg" alt="">
                    <figcaption class="cp-caption">
                        <h4><a href="booking.html">Anita Doe</a></h4>
                        <ul class="cp-meta-listed">
                            <li>Cab: <span>Driver</span></li>
                            <li>Age: <span>27</span></li>
                        </ul>
                    </figcaption>
                </figure>
                <!--Driver Box End-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!--Driver Box Start-->
                <figure class="cp-driver-box">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/driver-img-02.jpg" alt="">
                    <figcaption class="cp-caption">
                        <h4><a href="booking.html">Adam Eli</a></h4>
                        <ul class="cp-meta-listed">
                            <li>Part time: <span>Driver</span></li>
                            <li>Age: <span>25</span></li>
                        </ul>
                    </figcaption>
                </figure>
                <!--Driver Box End-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!--Driver Box Start-->
                <figure class="cp-driver-box">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/driver-img-03.jpg" alt="">
                    <figcaption class="cp-caption">
                        <h4><a href="booking.html">Johni Yoe</a></h4>
                        <ul class="cp-meta-listed">
                            <li>Full time: <span>Driver</span></li>
                            <li>Age: <span>29</span></li>
                        </ul>
                    </figcaption>
                </figure>
                <!--Driver Box End-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!--Driver Box Start-->
                <figure class="cp-driver-box">
                    <img src="<?php echo base_url()."assets/themes/taxigo/";?>images/driver-img-04.jpg" alt="">
                    <figcaption class="cp-caption">
                        <h4><a href="booking.html">Anita Doe</a></h4>
                        <ul class="cp-meta-listed">
                            <li>Cab: <span>Driver</span></li>
                            <li>Age: <span>21</span></li>
                        </ul>
                    </figcaption>
                </figure>
                <!--Driver Box End-->
            </div>
        </div>
    </div>
</section><!--Driver Section End-->

 <!--Testimonial Section Start-->
 <section class="cp-testimonial-section pd-tb80">
     <div class="container">
        <div class="cp-heading-style2">
            <h2>Latest <span>Reviews</span></h2>
        </div>
        <div class="owl-carousel" id="cp-testimonial-slider">
            <div class="item">
                <div class="cp-testimonial-inner">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="cp-text">
                                <strong>Highly recommended by our customers</strong>
                                <blockquote class="cp-blockquote">
                                    Sabemos que el diferencial está en los detalles, y es por ello que nuestros se
                                    vicios en alquiler de vehículos, tanto en el sector turístico como en el empr
                                    sarial, se destacan por su calidad y buen gusto para brindarte una experien
                                    cia única, segura y agradable.
                                </blockquote>
                                <span>ALBERT EISHAL, ART DIRECTOR</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span class="cp-icon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                </div> 
            </div>
             <div class="item">
                <div class="cp-testimonial-inner">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="cp-text">
                                <strong>Highly recommended by our clients</strong>
                                <blockquote class="cp-blockquote">
                                    Tanto en el sector turístico como en el empr
                                    sarial, se destacan por su calidad y buen gusto para brindarte una experien
                                    cia única, segura y agradable Sabemos que el diferencial está en los detalles, y es por ello que nuestros se
                                    vicios en alquiler de vehículos, .
                                </blockquote>
                                <span>Niky John, ART DIRECTOR</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span class="cp-icon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                </div> 
            </div>
             <div class="item">
                <div class="cp-testimonial-inner">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="cp-text">
                                <strong>Highly recommended by our customers</strong>
                                <blockquote class="cp-blockquote">
                                    Sabemos que el diferencial está en los detalles, y es por ello que nuestros se
                                    vicios en alquiler de vehículos, tanto en el sector turístico como en el empr
                                    sarial, se destacan por su calidad y buen gusto para brindarte una experien
                                    cia única, segura y agradable.
                                </blockquote>
                                <span>William Bard, ART DIRECTOR</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span class="cp-icon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
     </div>
 </section><!--Testimonial Section End-->