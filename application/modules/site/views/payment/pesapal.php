<?php
$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}
?>
<!-- Section: inner-header -->
<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_about?>">
  <div class="container pt-0 pb-0">
    <!-- Section Content -->
    <div class="section-content text-center">
      <div class="row"> 
        <div class="col-md-6 col-md-offset-3 text-center">
          <h2 class="text-theme-colored font-36">Donation Portal</h2>
          <ol class="breadcrumb text-center mt-10 white">
           
          </ol>
        </div>
      </div>
    </div>
  </div>      
</section>

<section>
  	<div class="container">
	    <div class="section-content">
	      	<div class="row">
		      	<div class="col-md-12">
					<?php
					 if(!empty($iframe))
					{
					    ?>
					    <iframe src="<?php echo $iframe;?>" width="100%" height="700px"  scrolling="no" frameBorder="0">
					        <p>Browser unable to load iFrame</p>
					    </iframe>
					    <?php
					}

					?>
				</div>
			</div>
		</div>
	</div>
</section>
