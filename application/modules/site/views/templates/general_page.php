<!doctype html>
<html>
       <!-- head -->
    <?php echo $this->load->view('site/includes/header', '', TRUE); ?>
    <!-- end of head -->
	<!-- <script src="<?php echo base_url()."assets/themes/sr-builders/";?>assets/jquery/jquery-3.1.1.min.js"></script> -->
    
    <body class="has-side-panel side-panel-left fullwidth-page side-push-panel">
    	
	<?php echo $this->load->view('site/includes/side_panel', '', TRUE); ?>
	<div id="wrapper" class="clearfix">
		  <!-- preloader -->
		  <!-- <div id="preloader">
		    <div id="spinner">
		      <div class="preloader-dot-loading">
		        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
		      </div>
		    </div>
		    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		  </div> -->
		<!-- Header -->

	 
		<?php echo $this->load->view('site/includes/navigation', '', TRUE); ?>
		<div class="main-content">
	    	<?php echo $content;?>
	    </div>
	</div>

     <?php echo $this->load->view('site/includes/footer', '', TRUE); ?>
  
		<!-- Footer Scripts -->
		<!-- JS | Custom script for all pages -->
		<script src="<?php echo base_url()."assets/themes/charity-fund/";?>js/custom.js"></script>

		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
		      (Load Extensions only on Local File Systems ! 
		       The following part can be removed on Server for On Demand Loading) -->
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/charity-fund/";?>js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>

		<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/summernote/dist/summernote.min.js"></script>
   		<script type="text/javascript">
   			$(function () {
				// $('#datapicker2').datepicker({format: 'yyyy-mm-dd'});

		        // Initialize summernote plugin
		        $('.summernote').summernote();

		        var sHTML = $('.summernote').code();
		      
		        console.log(sHTML);

		        $('.summernote1').summernote({
		            toolbar: [
		                ['headline', ['style']],
		                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
		                ['textsize', ['fontsize']],
		                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
		            ]
		        });

		        $('.summernote2').summernote({
		            airMode: true,
		        });

		    });
   		</script>
    </body>
</html>