<?php
$initiative_header = 'http://placehold.it/1920x1280';
if($query->num_rows()>0)
{
	foreach($query->result() as $row)
	{
		$post_id = $row->post_id;
		$blog_category_name = $row->blog_category_name;
		$blog_category_id = $row->blog_category_id;
		$post_image = $row->post_image;
		$post_content = $row->post_content;
		$post_title = $row->post_title;
		$post_comments = $row->post_comments;
		$post_status = $row->post_status;
		$post_views = $row->post_views;
    $post_video = $row->post_video;
		$image = base_url().'assets/images/posts/'.$row->post_image;
    $initiative_header = base_url().'assets/images/posts/'.$row->post_header;
		$web_name = $this->site_model->create_web_name($post_title);
		$created_by = $row->created_by;
		$modified_by = $row->modified_by;
		$total_comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
		$created = $row->created;
		$day = date('j',strtotime($created));
		$month = date('M Y',strtotime($created));
		$created_on = date('jS M Y H:i a',strtotime($row->created));
		
		$categories = '';
		$count = 0;
    $post_target = $row->post_target;

    $raised = $this->site_model->get_project_contributions($post_id);
    $contributors = $this->site_model->get_project_contributors($post_id);
    if($post_target == 0)
    {
      $post_target = 1;
      $value_post = 0;
    }
    else
    {
      $value_post = $post_target;
    }

    if($raised == 0 )
    {
      $percent = 0;
    }
    else
    {
      
      $percent = ($raised / $post_target) * 100; 
    }
		
	}
}
//get all administrators
	

	foreach($categories_query->result() as $res)
	{
		$count++;
		$category_name = $res->blog_category_name;
		$category_id = $res->blog_category_id;
		
		if($count == $categories_query->num_rows())
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
		}
		
		else
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
		}
	}
	$comments_query = $this->blog_model->get_post_comments($post_id);
	//comments
	$comments = 'No Comments';
	$total_comments = $comments_query->num_rows();
	if($total_comments == 1)
	{
		$title = 'comment';
	}
	else
	{
		$title = 'comments';
	}
	// var_dump($post_id); die();
	if($comments_query->num_rows() > 0)
	{
		$comments = '';
		foreach ($comments_query->result() as $row)
		{
			$post_comment_user = $row->post_comment_user;
			$post_comment_description = $row->post_comment_description;
			$date = date('jS M Y H:i a',strtotime($row->comment_created));
			
			$comments .= 
			'
       <li>
       <div class="author-details media-post">
          <div class="post-right">
            <h5 class="post-title mt-0 mb-0"><a href="#" class="font-18">'.$post_comment_user.'</a></h5>
            <p>'.$post_comment_description.'</p>
           
          </div>
          <div class="clearfix"></div>
        </div>
		    </li>
				
			';
		}
	}

// var_dump($image); die();
?>


 <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $initiative_header;?>">
      <div class="container pt-0 pb-10">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-10 col-md-offset-1 text-center">
              <h2 class="text-theme-colored font-36"><?php echo $post_title;?></h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li><a href="<?php echo site_url().'initiatives'?>">Causes</a></li>
                <li class="active"><?php echo $post_title;?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

     <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-9">
            <div class="upcoming-events media bg-light p-15 pb-60 mb-50 mb-sm-30">
              <div class="thumb">
                <img class="img-fullwidth" src="<?php echo $image?>" alt="..." width="730" height="400" >
              </div>
              <div class="row">
                <div class="col-sm-8">
                  <div class="mt-30">
                    <h4 class="media-heading text-uppercase font-weight-500"><?php echo $post_title?></h4>
                    <p class="mb-20"> 
                     

                      <?php echo $description?>
                        <?php

                          if(!empty($post_video))
                          {
                            $item = '<iframe height="200" src="https://www.youtube.com/embed/'.$post_video.'" frameborder="0" allowfullscreen></iframe>';
                          }
                          else
                          {
                            $item = '';
                          }
                          echo $item;
                        ?>

                      </p>
                  </div>
                </div>
                <div class="col-sm-4">
                 
                  <div class="event-count causes clearfix p-15 mt-15 border-left">
                    <div class="progress-item mt-20 mb-30">
                      <div class="progress mb-30">
                        <div class="progress-bar" data-percent="<?php echo $percent;?>"></div>
                      </div>
                    </div>
                    <ul class="list-inline clearfix">
                      <li class="pull-left pr-0">Raised: <?php echo $raised;?></li>
                      <li class="pull-right pr-0">Goal: <?php echo $value_post;?></li>
                    </ul>
                    <div class="mt-10">
                      
                     <a href="#donations" class="btn btn-success btn-flat btn-large mt-15" data-loading-text="Please wait...">DONATE NOW</a>
                      
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
            <div id="donations">
                <?php
                  $customer_id = $this->session->userdata('customer_id');
                  $customer_login_status = $this->session->userdata('customer_login_status');
                  $applicant_login_status = $this->session->userdata('applicant_login_status');
                      
                  if($customer_login_status == TRUE OR $applicant_login_status == TRUE)
                  {

                		echo form_open(site_url().'procced-to-payment/'.$post_id, array('class' => 'form-horizontal', 'role' => 'form'));
                  
        					echo form_hidden('type', 'MERCHANT');
        					echo form_hidden('credit_type_id', 2);
        					echo form_hidden('project_id', $post_id);
        					echo form_hidden('description', $post_title);
        					
        				?>
                    <div class="col-md-12">
                    	
                        <h3>Payment Information: </h3>
                    	<div class="col-sm-12">
        			      <div class="form-group">
        			        <label for="form_email">Amount <small>*</small></label>
        			        <input id="form_email" name="amount" class="form-control required " type="number" placeholder="Enter Amount">
        			      </div>
        			    </div>
        			    <div class="col-sm-12">
        	                <div class="payment-method">
        	                  <div class="radio">
        	                    <label>
        	                      <input type="radio" name="credit_type_id" value="2" checked>
        	                      Pesapal Payment </label>
        	                    <p>Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
        	                  </div>
        	                </div>
        	            </div>
                    </div>
          			<div class="col-md-12">
          				<button type="submit" class="btn btn-dark btn-flat btn-sm pull-right mt-15" data-loading-text="Please wait...">Proceed to Donate</button>
          			</div>
        			 <?php
        					echo form_close();
                }
                else
                {
                  ?>
                  <div class="col-md-12">
                  <a href="<?php echo site_url().'login'?>" class="btn btn-dark btn-flat btn-lg pull-right mt-15" data-loading-text="Please wait...">Login to Donate</a>
                </div>
                  <?php
                }
                ?>
              </div>

          </div>
         
          <div class="col-sm-12 col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">
              
                <?php

                $company_news_query = $this->site_model->get_active_items('Company News',10);
                $company_news_item = '';
                if($company_news_query->num_rows() > 0)
                {
                  $x=0;
                  foreach($company_news_query->result() as $row)
                  {
                    $company_news_title = $row->post_title;
                    $post_id = $company_news_id = $row->post_id;
                    $blog_category_name = $row->blog_category_name;
                    $blog_category_id = $row->blog_category_id;
                    $company_news_name = $row->post_title;
                    $company_news_web_name = $this->site_model->create_web_name($company_news_name);
                    $post_status = $row->post_status;
                    $post_views = $row->post_views;
                    $company_news_image = base_url().'assets/images/posts/'.$row->post_image;
                    $created_by = $row->created_by;
                    $modified_by = $row->modified_by;
                    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                    $company_news_description = $row->post_content;
                    $company_news_description_mini_desc = implode(' ', array_slice(explode(' ', $company_news_description), 0, 50));
                    $created = $row->created;
                    $day = date('j',strtotime($created));
                    $month = date('M',strtotime($created));
                    $year = date('Y',strtotime($created));
                    $created_on = date('jS M Y',strtotime($row->created));


                   $company_news_item .= ' <article class="post media-post clearfix pb-0 mb-10">
                                            <a class="post-thumb" href="#"><img src="'.$company_news_image.'" width="75"  height="75" alt=""></a></a>
                                            <div class="post-right">
                                              <h5 class="post-title mt-0"><a href="'.site_url().'view-post/'.$company_news_web_name.'/'.$company_news_id.'">'.$company_news_name.'</a></h5>
                                              <p>'.$company_news_description_mini_desc.'</p>
                                            </div>
                                          </article>';


                   
                  }
                }

                  ?>
              <div class="widget">
                <h5 class="widget-title line-bottom">Latest News</h5>
                <div class="latest-posts">
                  <?php echo $company_news_item;?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    