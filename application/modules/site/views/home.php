<?php


    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $email2 = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $linkedin = $contacts['linkedin'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        $about = $contacts['about'];
         $mission = $contacts['mission'];
        // $about = $contacts['about'];
        
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $about = '';
        $mission = '';
    }
?>





 <!-- Section: home -->
  <section id="home" class="divider">
    <div class="container-fluid p-0">
      
      <!-- Slider Revolution Start -->
      <div class="rev_slider_wrapper">
        <div class="rev_slider" data-version="5.0">
          <ul>
            <!-- SLIDE 1 -->
           
            <?php 

              $slide_show ='';
              if($slides->num_rows() > 0)
              {
                $counter = 0;
                foreach($slides->result() as $res)
                {
                  $slideshow_name = $res->slideshow_name;
                  $slideshow_image_name = $res->slideshow_image_name;
                  $slideshow_description = $res->slideshow_description;
                  $slideshow_button_text = $res->slideshow_button_text;
                  $slideshow_link = $res->slideshow_link;

                  if(!empty($slideshow_link))
                  {
                    $link = " <button class='btn btn-hero btn-lg' href='".$slideshow_link."'> ".$slideshow_button_text." </button>";
                  }
                  else
                  {
                    $link ='';
                  }
                  if($counter == 0)
                  {
                    $active = 'active';
                  }
                  else
                  {
                    $active = '';
                  }

                  $name_explode = explode(" ", $slideshow_name);

                  $name1 = $name_explode[0];
                  if(isset($name_explode[1]))
                  {

                  $name2 = $name_explode[1];

                  }
                  else
                  {
                    $name2 = '';
                  }

                  $title_item = '<span class="text-theme-colored">'.$name1.'</span> '.$name2;
                  $counter++;
                  
                 ?>

                 <li data-index="rs-<?php echo $counter?>" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="<?php echo site_url()."assets/slideshow/".$slideshow_image_name?>"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo site_url()."assets/slideshow/".$slideshow_image_name?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption BigBold-Title tp-resizeme rs-parallaxlevel-0 text-uppercase"
                      id="rs-1-layer-1"

                      data-x="['left','left','left','left']" 
                      data-hoffset="['50','50','30','17']" 
                      data-y="['bottom','bottom','bottom','bottom']" 
                      data-voffset="['110','110','180','160']" 
                      data-fontsize="['105','100','70','60']"
                      data-lineheight="['100','90','60','60']"
                      data-width="['none','none','none','400']"
                      data-height="none"
                      data-whitespace="['nowrap','nowrap','nowrap','normal']"
                      data-transform_idle="o:1;"
                      data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                      data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                      data-mask_in="x:0px;y:[100%];" 
                      data-mask_out="x:inherit;y:inherit;" 
                      data-start="500" 
                      data-splitin="none" 
                      data-splitout="none" 
                      data-basealign="slide" 
                      data-responsive_offset="on"
                      style="z-index: 6; white-space: nowrap;"><?php echo $title_item;?>
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption BigBold-SubTitle tp-resizeme rs-parallaxlevel-0"
                      id="rs-1-layer-2"

                      data-x="['left','left','left','left']" 
                      data-hoffset="['55','55','33','20']" 
                      data-y="['bottom','bottom','bottom','bottom']" 
                      data-voffset="['40','1','74','58']" 
                      data-fontsize="['15','15','15','13']"
                      data-lineheight="['24','24','24','20']"
                      data-width="['410','410','410','280']"
                      data-height="['60','100','100','100']"
                      data-whitespace="normal"
                      data-transform_idle="o:1;"
                      data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                      data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                      data-start="650" 
                      data-splitin="none" 
                      data-splitout="none" 
                      data-basealign="slide" 
                      data-responsive_offset="on"
                      style="z-index: 7; min-width: 410px; max-width: 410px; max-width: 60px; max-width: 60px; white-space: normal;"><?php echo $slideshow_description;?>
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption btn btn-default btn-transparent btn-flat btn-lg pl-40 pr-40 rs-parallaxlevel-0"

                      id="rs-1-layer-3"
                      data-x="['left','left','left','left']" 
                      data-hoffset="['470','480','30','20']" 
                      data-y="['bottom','bottom','bottom','bottom']" 
                      data-voffset="['50','50','30','20']" 
                      data-width="none"
                      data-height="none"
                      data-whitespace="nowrap"
                      data-transform_idle="o:1;"
                      data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                      data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                      data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
                      data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                      data-start="650" 
                      data-splitin="none" 
                      data-splitout="none" 
                      data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
                      data-basealign="slide" 
                      data-responsive_offset="on"
                      style="z-index: 8; white-space: nowrap;border-color:rgba(255, 255, 255, 0.25);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;" onclick="redirect_now()">DONATE NOW 
                    </div>
                  <?php
                }
              }
              ?>
            
          </ul>
        </div><!-- end .rev_slider -->
      </div>
      <!-- end .rev_slider_wrapper -->
      <script>
        function redirect_now()
        {
          // window.location.href = <?php echo base_url()?>+"initiatives";
        }
        $(document).ready(function(e) {
          $(".rev_slider").revolution({
            sliderType:"standard",
            sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 5000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                },
                arrows: {
                    style: "gyges",
                    enable: true,
                    hide_onmobile: false,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 0,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 0,
                        v_offset: 0
                    }
                },
                bullets: {
                  enable:true,
                  hide_onmobile:true,
                  hide_under:960,
                  style:"zeus",
                  hide_onleave:false,
                  direction:"horizontal",
                  h_align:"right",
                  v_align:"bottom",
                  h_offset:80,
                  v_offset:50,
                  space:5,
                  tmp:'<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title">{{title}}</span>'
              }
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [1170, 1024, 778, 480],
            gridheight: [550, 768, 960, 720],
            lazyType: "none",
            parallax: {
                origo: "slidercenter",
                speed: 1000,
                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                type: "scroll"
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "on",
            stopAfterLoops: 0,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            fullScreenAutoWidth: "off",
            fullScreenAlignForce: "off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "0",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
          });
        });
      </script>
      <!-- Slider Revolution Ends -->

    </div>
  </section>


<?php
$about_query = $this->site_model->get_active_items('About Us');
$about_item = '';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title_item = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title_item);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));   
  }
   $about_item .=$description;
}

$mission_query = $this->site_model->get_active_items('Mission');
$mission_item = '';
if($mission_query->num_rows() > 0)
{
  $x=0;
  foreach($mission_query->result() as $row)
  {
    $mission_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $mission_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($mission_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $mission_image = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $mission_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));




   
  }
   $mission_item .=$mission_description;
}

$category_id = $this->site_model->get_category_id('Company Initiatives');

$company_initiatives_query = $this->site_model->get_active_post_content_by_category($category_id,4);
$company_initiatives_item = '';
if($company_initiatives_query->num_rows() > 0)
{
  $x=0;
  foreach($company_initiatives_query->result() as $row)
  {
    $company_initiatives_title = $row->post_title;
    $company_initiatives_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_initiatives_name = $row->post_title;
    $company_initiatives_web_name = $this->site_model->create_web_name($company_initiatives_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $initiative_image = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_initiatives_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $company_initiatives_description), 0, 40));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $raised = $this->site_model->get_project_contributions($post_id);
    $contributors = $this->site_model->get_project_contributors($post_id);

    $company_mini = strip_tags($company_initiatives_description);
    $mini_desc = $this->site_model->chop_string($company_mini,200);

    if($post_target == 0)
    {
      $post_target = 1;
      $value_post = 0;
    }
    else
    {
      $value_post = $post_target;
    }
    if($raised == 0 )
    {
      $percent = 0;
    }
    else
    {
      
      $percent = ($raised / $post_target) * 100; 
      if($percent > 100)
       {
        $percent = 100;
       }
    }

   $company_initiatives_item .= '<div class="col-sm-6 col-md-3 col-lg-3">
                                  <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-sm-30">
                                    <div class="thumb">
                                      <img class="img-fullwidth" alt="" src="'.$initiative_image.'" width="265" height="195">
                                    </div>
                                    <div class="progress-item mt-0">
                                      <div class="progress mb-0">
                                        <div class="progress-bar" data-percent="'.$percent.'"></div>
                                      </div>
                                    </div>
                                    <div class="causes-details clearfix border-bottom p-15 pt-10">
                                      <h5><a href="#">'.$company_initiatives_name.'</a></h5>
                                      <div  style="height:130px;">'.$mini_desc.'.</div>
                                      <ul class="list-inline clearfix mt-20">
                                        <li class="pull-left pr-0">Raised:Ksh. '.$raised.'</li>
                                        <li class="text-theme-colored pull-right pr-0">Goal:Ksh '.$post_target.'</li>
                                      </ul>
                                      <div class="mt-10">
                                       <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="'.site_url().'view-project/'.$company_initiatives_web_name.'/'.$company_initiatives_id.'">Donate</a>
                                       <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> '.$contributors.' Donors</div>
                                      </div>
                                    </div>
                                  </div>
                                </div>';


   
  }
}


$company_project_query = $this->site_model->get_active_items('Company Projects',5);
$company_project_item = '';
if($company_project_query->num_rows() > 0)
{
  $x=0;
  foreach($company_project_query->result() as $row)
  {
    $company_project_title = $row->post_title;
    $post_id = $company_project_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_project_name = $row->post_title;
    $company_project_web_name = $this->site_model->create_web_name($company_project_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $company_project_image = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_project_description = $row->post_content;
    // $company_project_description_mini = implode(' ', array_slice(explode(' ', $company_project_description), 0, 100));


    $company_mini = strip_tags($company_project_description);
    $company_project_description_mini = $this->site_model->chop_string($company_mini,200);


    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $raised = $this->site_model->get_project_contributions($post_id);
    $contributors = $this->site_model->get_project_contributors($post_id);

    if($post_target == 0)
    {
      $post_target = 1;
      $value_post = 0;
    }
    else
    {
      $value_post = $post_target;
    }
    if($raised == 0)
    {
      $percent = 0;
    }
    else
    {

    $percent = ($raised / $post_target) * 100; 
    if($percent > 100)
       {
        $percent = 100;
       }
    }

   $company_project_item .= ' <div class="item">
                                  <div class="causes bg-lighter box-hover-effect effect1 sm-maxwidth500 mb-sm-30">
                                    <div class="thumb">
                                      <img class="img-fullwidth" alt="" src="'.$company_project_image.'" width="320" height="195">
                                    </div>
                                    <div class="progress-item mt-0">
                                      <div class="progress mb-0">
                                        <div class="progress-bar" data-percent="'.$percent.'"></div>
                                      </div>
                                    </div>
                                    <div class="causes-details clearfix border-bottom p-15 pt-10">
                                    <p class="mb-10 mt-5"><span class="text-uppercase text-theme-colored"><strong>'.$company_project_name.' :</strong></span></p> 
                                      <div style="height:130px">'.$company_project_description_mini.'</div>
                                       <div class="donate-details">
                                         <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="'.site_url().'view-project/'.$company_project_web_name.'/'.$company_project_id.'">Donate</a>
                                         <ul class="pull-right list-inline mt-15">
                                           <li>Raised: Ksh '.$raised.'</li>
                                           <li>Goal: Ksh '.$post_target.'</li>
                                         </ul>
                                       </div>
                                    </div>
                                  </div>
                                </div>';


   
  }
}


$client_review_query = $this->site_model->get_active_items('Client Reviews',5);
$client_review_item = '';
if($client_review_query->num_rows() > 0)
{
  $x=0;
  foreach($client_review_query->result() as $row)
  {
    $client_review_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $client_review_name = $row->post_title;
    $web_name = $this->site_model->create_web_name($client_review_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $client_review_image = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $client_review_description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));


   $client_review_item .= ' <div class="item">
                                <div class="testimonial-wrapper text-center">
                                  <div class="content pt-10">
                                    <p>'.$client_review_description.'</p>
                                    <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                                    <h5 class="author text-theme-colored mb-0">'.$client_review_name.'</h5>
                                  </div>
                                </div>
                            </div>';


   
  }
}
?>
  
  <!-- Section: about -->
  <section id="about" class="bg-lighter">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <h4 class="m-0">Welcome to</h4>
          <h2 class="title text-theme-colored mt-0"><?php echo $post_title_item;?></h2>
          <p class="mb-10"> <?php echo $about?></p>
          

          <h4 class="mt-20 mb-5">Our Initiatives</h4>
          <ul class="list theme-colored angle-double-right mb-sm-30">
            <li><a href="#">Environmental Initiatives</a></li>
            <li><a href="#">Wildlife Initiatives</a></li>
            <li><a href="#">Cultural Initiatives</a></li>
            <li><a href="#">Water preservation and protection initiatives</a></li>
          </ul>
        </div>
        <div class="col-md-5">
          <div class="thumb mt-5">
            <img alt="" src="<?php echo $image_about;?>" class="img-fullwidth">
          </div>
          <h4 class="mt-20 mb-5">Our Mission</h4>
          <p><?php echo $mission;?></p>
        </div>
      </div>
    </div>
  </section>
  
  <!-- Section: Causes -->
  <section>
    <div class="container pb-80">
      <div class="section-title text-center">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h3 class="text-uppercase mt-0">Our Causes</h3>
            <div class="title-icon">
              <i class="flaticon-charity-hand-holding-a-heart"></i>
            </div>
            <p>Your kind donation will go a long way in ensuring some beautiful things (our rivers, forests, wildlife, fish stocks etc.) last forever.<br> Be a changemaker!</p>
          </div>
        </div>
      </div>
      <div class="row mtli-row-clearfix">
        <?php echo $company_initiatives_item;?>
       
      </div>
    </div>
  </section>
  
  <!-- divider: Emergency Services -->
  <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2"  data-bg-img="<?php echo $image_about;?>">
    <div class="container">
      <div class="section-content text-center">
        <div class="row">
          <div class="col-md-12">
            <h3 class="mt-0">Need any Help ?</h3>
            <h2>Write to us at <span class="text-theme-colored"><?php echo $email;?></span></h2>
          </div>
        </div>
      </div>
    </div>      
  </section>

<?php
$about_query = $this->site_model->get_active_content_items('Become A Volunteer');
$volunteer_description = '';
$volunteer_image = '';
$item  = '';
$volunteer_header = 'http://placehold.it/1920x1280';
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $volunteer_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($volunteer_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $post_video = $row->post_video;
    $volunteer_image = base_url().'assets/images/posts/'.$row->post_image;
    $volunteer_header = base_url().'assets/images/posts/'.$row->post_header;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $volunteer_description = $row->post_content;
    // $volunteer_description_mini = implode(' ', array_slice(explode(' ', $volunteer_description), 0, 100));

    $company_mini = strip_tags($volunteer_description);
    $volunteer_description_mini = $this->site_model->chop_string($company_mini,200);

    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));   

    // var_dump($post_video);die();
    if(!empty($post_video))
    {
      $item = '<iframe height="200" src="https://www.youtube.com/embed/'.$post_video.'" frameborder="0" allowfullscreen></iframe>';
    }
    else
    {
      $item  =  $volunteer_image;
    }
    
  }
}

$volunteer_query = $this->site_model->get_active_content_items('Volunteer Opportunities');
$linked_page = '';
$volunteer_count = 0;
if($volunteer_query->num_rows() > 0)
{
  foreach($volunteer_query->result() as $row)
  {
    $volunteer_title = $row->post_title;
    $volunteer_post_id = $row->post_id;
    $web_name = $this->site_model->create_web_name($volunteer_title);
    $linked_page = base_url().'opportunities/'.$web_name.'/'.$volunteer_post_id;

    $volunteer_count = $this->site_model->get_applicants($volunteer_post_id);
  }
}





?>
  <!-- Section: featured project -->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <h4 class="text-uppercase line-bottom mt-0">Featured Project</h4>
          <div class="featured-project-carousel owl-nav-top">
           <?php echo $company_project_item;?>
            
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <h4 class="text-uppercase line-bottom mt-0">Become a Volunteer</h4>
          <div class="volunteer-wrapper border-bottom sm-maxwidth500 clearfix pb-15 mb-sm-30">
            <div class="fluid-video-wrapper">
              <?php echo $item;?>
            </div>
            <div class="mb-10 mt-15" style="height:160px;"> <?php echo $volunteer_description;?></div>
            <p>
              <a href="<?php echo $linked_page;?>" class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10">Join Us</a>
              <span class="pull-right ml-10 mt-12 font-14">volunteers</span>
              <span class="animate-number pull-right font-20 text-theme-colored lineheight-20 mt-5" data-value="<?php echo $volunteer_count;?>" data-animation-duration="2500"><?php echo $volunteer_count?></span>
            </p>
          </div>
        </div>

          <?php

          $company_events_query = $this->site_model->get_active_items('Company Events',10);
          $company_events_item = '';
          $initiative_header  ='http://placehold.it/1920x1280';
          if($company_events_query->num_rows() > 0)
          {
            $x=0;
            foreach($company_events_query->result() as $row)
            {
              $company_events_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $company_events_name = $row->post_title;
              $events_web_name = $this->site_model->create_web_name($company_events_name);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $company_events_image = base_url().'assets/images/posts/'.$row->post_image;
              $initiative_header = base_url().'assets/images/posts/'.$row->post_header;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $company_events_description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $company_events_description), 0, 40));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));


             $company_events_item .= '<div class="event media sm-maxwidth400 p-15 mt-0 mb-15">
                                        <div class="row">
                                          <div class="col-xs-3 p-0">
                                            <div class="thumb pl-15">
                                              <img alt="..." src="'.$company_events_image.'" class="media-object" width="75" height="75">
                                            </div>
                                          </div>
                                          <div class="col-xs-6 p-0 pl-15">
                                            <div class="event-content">
                                              <h5 class="media-heading text-uppercase"><a href="'.site_url().'view-post/'.$events_web_name.'/'.$post_id.'">'.$company_events_name.'</a></h5>
                                              <ul>
                                                <li><i class="fa fa-clock-o"></i> on '.$created_on.'</li>
                                              </ul>                    
                                            </div>                
                                          </div>
                                          <div class="col-xs-3 pr-0">
                                            <div class="event-date text-center">
                                              <ul>
                                                <li class="font-36 text-theme-colored font-weight-700">'.$day.'</li>
                                                <li class="font-20 text-center text-uppercase">'.$month.'</li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>';


             
            }
          }
          ?>
        <div class="col-sm-12 col-md-4">
          <h4 class="text-uppercase line-bottom mt-0">Events</h4>
          <div class="bxslider bx-nav-top">
              <?php echo $company_events_item?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php
$category_id = $this->site_model->get_category_id('Company Initiatives');
// service number two
$about_query = $this->site_model->get_active_child_items($category_id);
$about_sub_menu_services = '';
if($about_query->num_rows() > 0)
{
  foreach($about_query->result() as $res)
  {
    $blog_category_name = $res->blog_category_name;
    $blog_category_id = $res->blog_category_id;
    $about_sub_menu_services .= '<option value="'.$blog_category_id.'">'.$blog_category_name.'</option>';
  }
}

?>

  
  <!-- divider: Donate Now -->
  <section class="divider parallax" data-stellar-background-ratio="0.4"  data-bg-img="<?php echo $image_about;?>">
    <div class="container pt-0 pb-0">
      <div class="row">
        <div class="col-md-7">
          <div class="bg-theme-colored-transparent-deep p-40">
            <h4 class="text-uppercase line-bottom">Make a Donation Now!</h4>
            
            <!-- Paypal Both Onetime/Recurring Form Starts -->
            <!-- <form id="paypal_donate_form_onetime_recurring" class="form-text-white"> -->
            <?php
              $customer_id = $this->session->userdata('customer_id');
              $customer_login_status = $this->session->userdata('customer_login_status');
              $applicant_login_status = $this->session->userdata('applicant_login_status');
                  
              if($customer_login_status == TRUE OR $applicant_login_status == TRUE)
              {

                echo form_open(site_url().'donate-now', array('class' => 'form-horizontal', 'role' => 'form'));
              
                  echo form_hidden('type', 'MERCHANT');
                  echo form_hidden('credit_type_id', 2);
              }
              else
              {
                ?>
                <form  class="form-text-white">
                <?php
              }
              ?>
              <div class="row">
                <!-- <div class="col-md-12">
                  <div class="form-group mb-20">
                    <label><strong>Payment Type</strong></label> <br>
                    <label class="radio-inline">
                      <input type="radio" checked="" value="one_time" name="payment_type"> 
                      One Time
                    </label>
                    <label class="radio-inline">
                      <input type="radio" value="recurring" name="payment_type"> 
                      Recurring
                    </label>
                  </div>
                </div> -->

                <!-- <div class="col-sm-12" id="donation_type_choice">
                  <div class="form-group mb-20">
                    <label><strong>Donation Type</strong></label>
                    <div class="radio mt-5">
                      <label class="radio-inline">
                        <input type="radio" value="D" name="t3" checked="">
                        Daily</label>
                      <label class="radio-inline">
                        <input type="radio" value="W" name="t3">
                        Weekly</label>
                      <label class="radio-inline">
                        <input type="radio" value="M" name="t3">
                        Monthly</label>
                      <label class="radio-inline">
                        <input type="radio" value="Y" name="t3">
                        Yearly</label>
                    </div>
                  </div>
                </div> -->

                <div class="col-sm-12">
                  <div class="form-group mb-20">
                    <label><strong>I Want to Donate for</strong></label>
                    <select name="item_name" id="item_id" class="form-control" >
                      <option value="0">SELECT AN INITIATIVE</option>
                      <?php echo $about_sub_menu_services;?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group mb-20">
                    <label><strong>Conservancy </strong></label>
                    <select name="conservancy_id" id="services_charges" class="form-control" >
                   
                    </select>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group mb-20">
                    <label><strong>Currency</strong></label>
                    <select name="currency_code" class="form-control">
                      <option value="">Select Currency</option>
                      <option value="USD" >USD - U.S. Dollars</option>
                       <option value="KES" selected="selected">KES - Kenya Shillings</option>
                      <option value="AUD">AUD - Australian Dollars</option>
                      <option value="BRL">BRL - Brazilian Reais</option>
                      <option value="GBP">GBP - British Pounds</option>
                      <option value="HKD">HKD - Hong Kong Dollars</option>
                      <option value="HUF">HUF - Hungarian Forints</option>
                      <option value="INR">INR - Indian Rupee</option>
                      <option value="ILS">ILS - Israeli New Shekels</option>
                      <option value="JPY">JPY - Japanese Yen</option>
                      <option value="MYR">MYR - Malaysian Ringgit</option>
                      <option value="MXN">MXN - Mexican Pesos</option>
                      <option value="TWD">TWD - New Taiwan Dollars</option>
                      <option value="NZD">NZD - New Zealand Dollars</option>
                      <option value="NOK">NOK - Norwegian Kroner</option>
                      <option value="PHP">PHP - Philippine Pesos</option>
                      <option value="PLN">PLN - Polish Zlotys</option>
                      <option value="RUB">RUB - Russian Rubles</option>
                      <option value="SGD">SGD - Singapore Dollars</option>
                      <option value="SEK">SEK - Swedish Kronor</option>
                      <option value="CHF">CHF - Swiss Francs</option>
                      <option value="THB">THB - Thai Baht</option>
                      <option value="TRY">TRY - Turkish Liras</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group mb-20">
                    <label><strong>How much do you want to donate?</strong></label>
                    <select name="amount" class="form-control">
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                        <option value="other">Other Amount</option>
                    </select>
                    <div id="custom_other_amount">
                      <label><strong>Custom Amount:</strong></label>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group mb-20">
                    <?php
                    $customer_id = $this->session->userdata('customer_id');
                    $customer_login_status = $this->session->userdata('customer_login_status');
                    $applicant_login_status = $this->session->userdata('applicant_login_status');
                        
                    if($customer_login_status == TRUE OR $applicant_login_status == TRUE)
                    {
                    ?>
                      <button type="submit" class="btn btn-flat btn-dark mt-10 pl-30 pr-30" data-loading-text="Please wait...">Donate Now</button>
                    <?php
                    }
                    else
                    {
                       ?>
                        <div class="col-md-12">
                        <a href="<?php echo site_url().'login'?>" class="btn btn-dark btn-flat btn-sm pull-right mt-15" data-loading-text="Please wait...">Login to Donate</a>
                      </div>
                        <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </form>
            
            <!-- Script for Donation Form Custom Amount -->
            <script type="text/javascript">
              $(document).ready(function(e) {
                var $donation_form = $("#paypal_donate_form_onetime_recurring");
                //toggle custom amount
                var $custom_other_amount = $donation_form.find("#custom_other_amount");
                $custom_other_amount.hide();
                $donation_form.find("select[name='amount']").change(function() {
                    var $this = $(this);
                    if ($this.val() == 'other') {
                      $custom_other_amount.show().append('<div class="input-group"><span class="input-group-addon">$</span> <input id="input_other_amount" type="text" name="amount" class="form-control" value="100"/></div>');
                    }
                    else{
                      $custom_other_amount.children( ".input-group" ).remove();
                      $custom_other_amount.hide();
                    }
                });

                //toggle donation_type_choice
                var $donation_type_choice = $donation_form.find("#donation_type_choice");
                $donation_type_choice.hide();
                $("input[name='payment_type']").change(function() {
                    if (this.value == 'recurring') {
                        $donation_type_choice.show();
                    }
                    else {
                        $donation_type_choice.hide();
                    }
                });


                // submit form on click
                $donation_form.on('submit', function(e){
                        $( "#paypal_donate_form-onetime" ).submit();
                    var item_name = $donation_form.find("select[name='item_name'] option:selected").val();
                    var currency_code = $donation_form.find("select[name='currency_code'] option:selected").val();
                    var amount = $donation_form.find("select[name='amount'] option:selected").val();
                    var t3 = $donation_form.find("input[name='t3']:checked").val();

                    if ( amount == 'other') {
                      amount = $donation_form.find("#input_other_amount").val();
                    }

                    // submit proper form now
                    if ( $("input[name='payment_type']:checked", $donation_form).val() == 'recurring' ) {
                        var recurring_form = $('#paypal_donate_form-recurring');

                        recurring_form.find("input[name='item_name']").val(item_name);
                        recurring_form.find("input[name='currency_code']").val(currency_code);
                        recurring_form.find("input[name='a3']").val(amount);
                        recurring_form.find("input[name='t3']").val(t3);

                        recurring_form.find("input[type='submit']").trigger('click');

                    } else if ( $("input[name='payment_type']:checked", $donation_form).val() == 'one_time' ) {
                        var onetime_form = $('#paypal_donate_form-onetime');

                        onetime_form.find("input[name='item_name']").val(item_name);
                        onetime_form.find("input[name='currency_code']").val(currency_code);
                        onetime_form.find("input[name='amount']").val(amount);

                        onetime_form.find("input[type='submit']").trigger('click');
                    }
                    return false;
                });

              });
            </script>



            <!-- Paypal Onetime Form -->
            <form id="paypal_donate_form-onetime" class="hidden" action="https://www.paypal.com/cgi-bin/webscr" method="post">
              <input type="hidden" name="cmd" value="_donations">
              <input type="hidden" name="business" value="accounts@thememascot.com">

              <input type="hidden" name="item_name" value="Educate Children"> <!-- updated dynamically -->
              <input type="hidden" name="currency_code" value="USD"> <!-- updated dynamically -->
              <input type="hidden" name="amount" value="20"> <!-- updated dynamically -->

              <input type="hidden" name="no_shipping" value="1">
              <input type="hidden" name="cn" value="Comments...">
              <input type="hidden" name="tax" value="0">
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="bn" value="PP-DonationsBF">
              <input type="hidden" name="return" value="http://www.yoursite.com/thankyou.html">
              <input type="hidden" name="cancel_return" value="http://www.yoursite.com/paymentcancel.html">
              <input type="hidden" name="notify_url" value="http://www.yoursite.com/notifypayment.php">
              <input type="submit" name="submit">
            </form>
            
            <!-- Paypal Recurring Form -->
            <form id="paypal_donate_form-recurring" class="hidden" action="https://www.paypal.com/cgi-bin/webscr" method="post">
              <input type="hidden" name="cmd" value="_xclick-subscriptions">
              <input type="hidden" name="business" value="accounts@thememascot.com">

              <input type="hidden" name="item_name" value="Educate Children"> <!-- updated dynamically -->
              <input type="hidden" name="currency_code" value="USD"> <!-- updated dynamically -->
              <input type="hidden" name="a3" value="20"> <!-- updated dynamically -->
              <input type="hidden" name="t3" value="D"> <!-- updated dynamically -->


              <input type="hidden" name="p3" value="1">
              <input type="hidden" name="rm" value="2">
              <input type="hidden" name="src" value="1">
              <input type="hidden" name="sra" value="1">
              <input type="hidden" name="no_shipping" value="0">
              <input type="hidden" name="no_note" value="1">                     
              <input type="hidden" name="lc" value="US">
              <input type="hidden" name="bn" value="PP-DonationsBF">
              <input type="hidden" name="return" value="http://www.yoursite.com/thankyou.html">
              <input type="hidden" name="cancel_return" value="http://www.yoursite.com/paymentcancel.html">
              <input type="hidden" name="notify_url" value="http://www.yoursite.com/notifypayment.php">
              <input type="submit" name="submit">
            </form>
            <!-- Paypal Both Onetime/Recurring Form Ends -->

          </div>
        </div>
        <div class="col-md-5">
        </div>
      </div>
    </div>
  </section>
  <?php
   $gallery_div = $this->site_model->get_active_gallery();
    $gallery_items = '';
    if($gallery_div->num_rows() > 0)
    {   
        foreach($gallery_div->result() as $res_gallery_div)
        {
            $gallery_name = $res_gallery_div->gallery_name;
            $gallery_web_name = $this->site_model->create_web_name($gallery_name);
            $gallery_image_name = $res_gallery_div->gallery_image_name;
            $gallery_image_thumb = $res_gallery_div->gallery_image_thumb;

             $gallery_items .=
                                    '
                                    <div class="gallery-item">
                                        <div class="thumb">
                                          <img alt="project" src="'.$gallery_location.''.$gallery_image_name.'" class="img-fullwidth" width="155" height="106">
                                          <div class="overlay-shade"></div>
                                          <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                              <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                <a href="'.$gallery_location.''.$gallery_image_name.'"  data-lightbox-gallery="gallery"><i class="fa fa-picture-o"></i></a>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    ';      
        }
    }

  ?>
  <!-- divider: Gallery -->
  <section class="divider bg-lighter">
    <div class="container">
      <div class="section-title text-center">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h3 class="text-uppercase mt-0">Gallery</h3>
            <div class="title-icon">
              <i class="flaticon-charity-hand-holding-a-heart"></i>
            </div>
            <p>Here are some pictures of people creating an impact<br> Become a changemaker!</p>
          </div>
        </div>
      </div>
      <div class="section-content">
        <div class="row">
          <div class="col-md-12">
            <div class="gallery-isotope grid-7 gutter-small clearfix" data-lightbox="gallery">
                <?php echo $gallery_items?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- Section: Testimonials and Logo -->
  <section class="divider parallax layer-overlay overlay-light" data-stellar-background-ratio="0.2" data-bg-img="<?php echo $image_about;?>">
    <div class="container pt-0 pb-0">
      <div class="row equal-height">
        <div class="col-md-7">
          <div class="display-table-parent pr-90 pl-90">
            <div class="display-table">
              <div class="display-table-cell">
                
                <div class="mt-30">
                  <h4 class="text-uppercase mb-5">Subscribe to our newsletter</h4>
                  <!-- Mailchimp Subscription Form-->
                  <?php echo form_open("site/newsletter_subscription", array("class" => "newsletter-form mt-10"));?>
                    <label class="display-block" for="mce-EMAIL"></label>
                    <div class="input-group">
                      <input type="email" name="email" id="mce-EMAIL" data-height="43px" class="form-control input-lg" placeholder="Your Email"  value="">
                       <input name="uri_string" type="hidden" value="<?php echo $this->uri->uri_string()?>">
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-flat btn-lg btn-colored btn-theme-colored m-0" data-height="43px">Subscribe</button>
                      </span>
                    </div>
                  <?php echo form_close();?>

                  <!-- Mailchimp Subscription Form Validation-->
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5 bg-light-transparent">
          <div class="pt-50 pb-50 pl-20 pr-20">
            <h4 class="line-bottom text-uppercase mt-0">Our Donors Say</h4>
            <div class="testimonial-carousel owl-nav-top">
                <?php echo $client_review_item;?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <?php

$company_news_query = $this->site_model->get_active_items('Company News',3);
$company_news_item = '';
if($company_news_query->num_rows() > 0)
{
  $x=0;
  foreach($company_news_query->result() as $row)
  {
    $company_news_title = $row->post_title;
    $post_id = $company_news_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_news_name = $row->post_title;
    $company_news_web_name = $this->site_model->create_web_name($company_news_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $company_news_image = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_news_description = $row->post_content;
    // $company_news_description_mini_desc = implode(' ', array_slice(explode(' ', $company_news_description), 0, 40));

    $company_mini = strip_tags($company_news_description);
    $company_news_description_mini_desc = $this->site_model->chop_string($company_mini,200);


    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));


   $company_news_item .= ' <div class="col-md-4">
                              <article class="post clearfix maxwidth600 mb-sm-30">
                                <div class="entry-header">
                                  <div class="post-thumb thumb"> <img src="'.$company_news_image.'" alt="" class="img-responsive img-fullwidth"> </div>
                                  <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                                  <div class="display-table">
                                    <div class="display-table-cell">
                                      <ul>
                                        <li><a class="text-white" href="#"><i class="fa fa-thumbs-o-up"></i> '.$comments.' <br> Likes</a></li>
                                        <li class="mt-20"><a class="text-white" href="#"><i class="fa fa-comments-o"></i> '.$comments.' <br> comments</a></li>
                                      </ul>
                                    </div>
                                  </div>
                                  </div>
                                </div>
                                <div class="entry-content border-1px p-20">
                                  <h5 class="entry-title mt-0 pt-0"><a href="#">'.$company_news_name.'</a></h5>
                                  <div class="text-left mb-20 mt-15 font-13" style="height:120px;">'.$company_news_description_mini_desc.'</div>
                                  <a class="btn btn-flat btn-colored btn-theme-colored btn-sm pull-left" href="'.site_url().'view-post/'.$company_news_web_name.'/'.$company_news_id.'">Read more</a>
                                  <ul class="list-inline entry-date pull-right font-12 mt-5">
                                    <li><a class="text-theme-colored" href="#">Admin |</a></li>
                                    <li><span class="text-theme-colored">'.$created_on.'</span></li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                              </article>
                            </div>';


   
  }
}

  ?>
  
  <!-- Section: News -->
  <section> 
    <div class="container pb-80">
      <div class="section-title text-center">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h3 class="text-uppercase mt-0">Recent News</h3>
            <div class="title-icon">
              <i class="flaticon-charity-hand-holding-a-heart"></i>
            </div>
            <p>Get the latest news on initiatives making a change <br> Be a change maker!</p>
          </div>
        </div>
      </div>
      <div class="row">
            <?php echo $company_news_item;?>
      </div>
    </div>
  </section>

  <script type="text/javascript">
   

    $(document).on("change","select#item_id",function(e)
    {
      var visit_type_id = $("select#item_id").val();
      var url = "<?php echo site_url();?>site/get_related_posts/"+visit_type_id;
      // alert(url);
      $.get( url, function( data ) 
      {
        $( "#services_charges" ).html( data );
      });
    });
  </script>