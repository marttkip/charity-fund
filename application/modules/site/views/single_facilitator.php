<?php 

if($facilitator->num_rows() > 0)
{
	$facilitators = $facilitator->row();
	$facilitators_name = $facilitators->facilitators_name;
	$description = $facilitators->facilitators_description;
	$facilitators_image = $facilitators->facilitators_image_name;
	$facilitators_link = $facilitators->facilitators_link;
	$facilitators_button_text = $facilitators->facilitators_button_text;
}

else
{
	$facilitators = '';
}
?>
<div class="kf_inr_banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<!--KF INR BANNER DES Wrap Start-->
                <div class="kf_inr_ban_des">
                	<div class="inr_banner_heading">
						<h3><?php echo $title;?></h3>
                	</div>
                   
                    <div class="kf_inr_breadcrumb">
						<ul>
							<?php echo $this->site_model->get_breadcrumbs();?>
						</ul>
					</div>
                </div>
                <!--KF INR BANNER DES Wrap End-->
            </div>
        </div>
    </div>
</div>

<!--Content Wrap Start-->
<div class="kf_content_wrap">
    <!--ABOUT UNIVERSITY START-->
    <section>
        <div class="container">
            <div class="row">
				
                <?php if(!empty($facilitators)){?>
                <div class=" col-lg-8 col-md-8">
                    <!--TEACHER BIO WRAP START-->
                    <div class="teacher_bio_wrap">
                        <!--TEACHER BIO LOGO START-->
                        <div class="teacher_bio_logo">
                            <span><i class="fa fa-user"></i></span>
                            <h3>Facilitator</h3>
                        </div>
                        <!--TEACHER BIO LOGO END-->
                        <!--TEACHER BIO des START-->
                        <div class="teacher_bio_des">
                            <h4><?php echo $facilitators_name;?></h4>
                            <!--<ul>
                                <li><i class="fa fa-envelope-o"></i><a href="#">Info@info.com</a></li>
                                <li><i class="fa fa-calendar"></i>01 Jan, 2016</li>
                                <li><i class="fa fa-phone"></i>+ (02) 132 456 7989</li>
                            </ul>-->
                            <p><?php echo $description;?></p>
                        </div>
                        <!--TEACHER BIO DES END-->
                    </div>
                    <!--TEACHER BIO WRAP END-->
                </div>

                <div class="col-lg-4 col-md-4">
                    <!--TEACHER THUMB START-->
                    <div class="teacher_thumb">
                        <figure>
                            <img src="<?php echo $facilitators_location.''.$facilitators_image;?>" alt=""/>
                        </figure>
                    </div>
                    <!--TEACHER THUMB END-->
                </div>
				<?php } else{?>
                <div class="row">
                	<div class="col-md-12">
                    	<p>Facilitator not found</p>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </section>
    <!--ABOUT UNIVERSITY END-->
	
</div>
<!--Content Wrap End-->
     