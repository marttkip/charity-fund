<?php

  $page_item = $this->site_model->decode_web_name($page_item);
  $query= $this->site_model->get_active_content_items($page_item);
   $result = '';
// var_dump($page_item); die();
  // var_dump($query->num_rows()); die();
   $image_header = 'http://placehold.it/1920x1280';
  if($query->num_rows() > 0)
  {

    foreach($query->result() as $row)
    {
      $brief_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_service = base_url().'assets/images/posts/'.$row->post_image;
      $image_header = base_url().'assets/images/posts/'.$row->post_header;
      $created_by = $row->created_by;
      $is_parent = $this->site_model->check_parent($blog_category_id);
      $modified_by = $row->modified_by;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));


      $gallery = $this->site_model->get_post_gallery($post_id);
      $gallery_item = '';
      if($gallery->num_rows() > 0)
      {
        foreach ($gallery->result() as $key => $value) {
          # code...
          $post_gallery_image_name = $value->post_gallery_image_name;
          $gallery_item .= '<div class="item">
                              <div class="thumb">
                                <img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" alt="">
                              </div>
                            </div>';
        }
      }

    }
                    }
?>


<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_header?>">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36"><?php echo $post_title;?></h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home';?>">Home</a></li>
                <li class="active"><?php echo $post_title?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>
    <!-- Section: About -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="image-carousel">
            	<?php echo $gallery_item;?>
            </div>
            
            <h3 class="text-theme-colored text-uppercase mt-0"><?php echo $post_title?></h3>
            <p><?php echo $description;?></p>
          </div>
           <div class="col-sm-12 col-md-6"> 
        	 <?php 
            echo form_open_multipart($this->uri->uri_string(),array("class" => "form", "role" => "form","id"=>"application_form"));
            
            $success_message = $this->session->userdata('success_message');
            
            if(!empty($success_message))
            {
                echo '<div class="alert alert-success"><p>'.$success_message.'</p></div>';
                $this->session->unset_userdata('success_message');
            }

            $error_message = $this->session->userdata('error_message');
            
            if(!empty($error_message))
            {
                echo '<div class="alert alert-danger"><p>'.$error_message.'</p></div>';
                $this->session->unset_userdata('error_message');
                
             }
            ?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_name">Name <small>*</small></label>
                    <input id="form_name" name="applicant_name" type="text" placeholder="Enter Name" required="" class="form-control">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_email">Email <small>*</small></label>
                    <input id="form_email" name="applicant_email" class="form-control required email" type="email" placeholder="Enter Email">
                  </div>
                </div>
              </div>
              <div class="row">               
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_sex">Sex <small>*</small></label>
                    <select id="form_sex" name="applicant_gender" class="form-control required">
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_branch">Choose Branch <small>*</small></label>
                    <select id="form_branch" name="applicant_branch" class="form-control required">
                      <option value="UK">UK</option>
                      <option value="USA">USA</option>
                      <option value="Australia">Australia</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="form_message">Message <small>*</small></label>
                <textarea id="form_message" name="applicant_message" class="form-control required" rows="5" placeholder="Your cover letter/message sent to the employer"></textarea>
              </div>
              <div class="form-group">
                <label for="form_attachment">C/V Upload</label>
                <input id="form_attachment" name="attachment" class="file" type="file" multiple data-show-upload="false" data-show-caption="true">
                <small>Maximum upload file size: 12 MB</small>
              </div>
              <div class="form-group">
                <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Apply Now</button>
              </div>
            <?php echo form_close();?>
       </div>  
         
        </div>
      </div>
    </section>