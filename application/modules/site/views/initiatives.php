 <?php

$category_id = $this->site_model->get_category_id('Company Initiatives');

$company_initiatives_query = $this->site_model->get_active_post_content_by_category($category_id);
$company_initiatives_item = '';
$header  ='http://placehold.it/1920x1280';
if($company_initiatives_query->num_rows() > 0)
{
  $x=0;
  foreach($company_initiatives_query->result() as $row)
  {
    $company_initiatives_title = $row->post_title;
    $company_initiatives_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_initiatives_name = $row->post_title;
    $company_initiatives_web_name = $this->site_model->create_web_name($company_initiatives_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $initiative_image = base_url().'assets/images/posts/'.$row->post_image;
    $initiative_header = base_url().'assets/images/posts/'.$row->post_header;

    if(empty($initiative_header))
    {
    }
    else
    {
      $header =  $initiative_header;
    }
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$company_initiatives_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_initiatives_description = $row->post_content;
    $company_mini = strip_tags($company_initiatives_description);
    // $mini_desc = implode(' ', array_slice(explode(' ', $company_mini), 0, 30));
    $mini_desc = $this->site_model->chop_string($company_mini,200);

    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $raised = $this->site_model->get_project_contributions($company_initiatives_id);
    $contributors = $this->site_model->get_project_contributors($company_initiatives_id);
    if($post_target == 0)
    {
      $post_target = 1;
      $value_post = 0;
    }
    else
    {
      $value_post = $post_target;
    }
    if($raised == 0 )
    {
      $percent = 0;
    }
    else
    {
      
      $percent = ($raised / $post_target) * 100; 


       if($percent > 100)
       {
        $percent = 100;
       }
    }

   $company_initiatives_item .= '<div class="col-sm-6 col-md-4 col-lg-4 mb-10">
                                  <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-sm-30">
                                    <div class="thumb">
                                      <img class="img-fullwidth" alt="" src="'.$initiative_image.'" width="265" height="195">
                                    </div>
                                    <div class="progress-item mt-0">
                                      <div class="progress mb-0">
                                        <div class="progress-bar" data-percent="'.$percent.'"></div>
                                      </div>
                                    </div>
                                    <div class="causes-details clearfix border-bottom p-15 pt-10">
                                      <h5><a href="#">'.$company_initiatives_name.'</a></h5>
                                      <div style="height:130px;">'.$mini_desc.'.</div>
                                      <ul class="list-inline clearfix mt-20">
                                        <li class="pull-left pr-0">Raised:Ksh. '.$raised.'</li>
                                        <li class="text-theme-colored pull-right pr-0">Goal:Ksh '.$value_post.'</li>
                                      </ul>
                                      <div class="mt-10">
                                       <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="'.site_url().'view-project/'.$company_initiatives_web_name.'/'.$company_initiatives_id.'">Donate</a>
                                       <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> '.$contributors.' Donors</div>
                                      </div>
                                    </div>
                                  </div>
                                </div>';


   
  }
}
 ?>

 <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $header;?>">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h3 class="text-theme-colored font-36">Causes</h3>
              <ol class="breadcrumb text-center mt-5 white">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li class="active">Causes</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Upcoming Event -->

     <?php

$company_news_query = $this->site_model->get_active_items('Company News',3);
$company_news_item = '';
if($company_news_query->num_rows() > 0)
{
  $x=0;
  foreach($company_news_query->result() as $row)
  {
    $company_news_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $company_news_name = $row->post_title;
    $web_name = $this->site_model->create_web_name($company_news_name);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $company_news_image = base_url().'assets/images/posts/'.$row->post_image;

    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $company_news_description = $row->post_content;
    $company_news_description_mini_desc = implode(' ', array_slice(explode(' ', $company_news_description), 0, 20));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));


   $company_news_item .= ' <article class="post media-post clearfix pb-0 mb-10">
                            <a class="post-thumb" href="#"><img src="'.$company_news_image.'" width="75" height="75" alt=""></a>
                            <div class="post-right">
                              <h5 class="post-title mt-0"><a href="#">'.$company_news_name.'</a></h5>
                              <p >'.$company_news_description_mini_desc.'</p>
                            </div>
                          </article>';


   
  }
}

  ?>
   <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-9">
            <div class="row">
              <?php echo $company_initiatives_item;?>
            </div>
          </div>
          <div class="col-sm-12 col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">             
             
              <div class="widget">
                <h5 class="widget-title line-bottom">Latest News</h5>
                <div class="latest-posts">
                   <?php echo $company_news_item;?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>