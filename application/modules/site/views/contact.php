 <?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		$address = $contacts['address'];
		$post_code = $contacts['post_code'];
		$city = $contacts['city'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		
		if(!empty($facebook))
		{
			//$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
		$address = '';
		$post_code = '';
		$city = '';
		$building = '';
		$floor = '';
		$location = '';
	}
?>

<?php 

$branches = $this->site_model->get_all_branches();

$branch_item = '';
if($branches->num_rows() > 0)
{
    foreach ($branches->result() as $key) {
        # code...
        $branch_name = $key->branch_name;
        $branch_building = $key->branch_building;
        $branch_post_code = $key->branch_post_code;
        $branch_address = $key->branch_address;
        $branch_phone = $key->branch_phone;
        $branch_email = $key->branch_email;
        $branch_location = $key->branch_location;
        $branch_floor = $key->branch_floor;
        $branch_city = $key->branch_city;


        $branch_item .= '
                        <div class="col-sm-3 col-xs-6">
                            <div class="office-address">
                                <h3>'.$branch_name.'</h3>
                                <address>
                                    <span>'.$branch_post_code.' - '.$branch_address.'</span>
                                    <span>'.$branch_city.'</span>
                                    <span>'.$branch_floor.' Floor, '.$branch_building.'</span>
                                    <span>'.$branch_location.'</span>
                                   
                                    <span>T. '.$branch_phone.'</span>
                                    <a >'.$branch_email.'</a>

                                </address>
                            </div>
                        </div>

                        ';
    }
}


?>


 <?php
   $query= $this->site_model->get_active_items('Main Contact');
  

   $result_main = '';
  if($query->num_rows() > 0)
  {
    foreach($query->result() as $row)
    {
      $brief_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_service = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 20));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));

      $image = '<img src="'.$image_service.'" width="80%"  alt=""/>';

      $result_main .= ' 
                  <div class="row about">
                      <div class="col-sm-12 col-md-6 contact-items">
                        <div class="news-title"> <h3>'.$brief_title.'</h3></div>
                        '.$description.'
                      </div>
                      <div class="col-sm-12 col-md-6">
                           <div class="item">'.$image.'</div>

							             <div id="contact-map"></div>
                      </div>
                    </div>
              ';


    }
  }

$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}

?>

<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_about?>">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-white font-36">Contact</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li class="active">Contact</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Divider: Contact -->
    <section class="divider layer-overlay overlay-white-9" data-bg-img="<?php echo $image_about?>">
      <div class="container">
        <div class="row pt-30">
          <div class="col-md-7">
            <h3 class="line-bottom mt-0 mb-30">Interested in discussing?</h3>
            <!-- Contact Form -->
            <?php 
            echo form_open($this->uri->uri_string(),array("class" => "form-transparent", "role" => "form","id"=>"contact_form"));
            
            $success_message = $this->session->userdata('success_message');
            
            if(!empty($success_message))
            {
                echo '<div class="alert alert-success"><p>'.$success_message.'</p></div>';
                $this->session->unset_userdata('success_message');
            }
            ?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_name">Name <small>*</small></label>
                    <input id="form_name" name="sender_name" class="form-control" type="text" placeholder="Enter Name" required="">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_email">Email <small>*</small></label>
                    <input id="form_email" name="sender_email" class="form-control required email" type="email" placeholder="Enter Email">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="form_name">Subject <small>*</small></label>
                    <input id="form_subject" name="subject" class="form-control required" type="text" placeholder="Enter Subject">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="form_name">Message</label>
                <textarea id="form_message" name="message" class="form-control required" rows="5" placeholder="Enter Message"></textarea>
              </div>
              <div class="form-group">
                <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Please wait...">Send your message</button>
              </div>
             <?php echo form_close();?>
            <!-- Contact Form Validation-->
           
          </div>
          <div class="col-md-5">
            <h3 class="line-bottom mt-0">Get in touch with us</h3>
          
            <ul class="styled-icons icon-dark icon-sm icon-circled mb-20">
              <li><a href="<?php echo $facebook;?>" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="<?php echo $twitter;?>" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="<?php echo $linkedin;?>" data-bg-color="#D71619"><i class="fa fa-linkedin"></i></a></li>
            </ul>

            <div class="icon-box media mb-15"> <a class="media-left pull-left flip mr-20" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
              <div class="media-body">
                <h5 class="mt-0">Our Office Location</h5>
                <p><?php echo $address;?> - <?php echo $post_code;?> <?php echo $location;?> <?php echo $building;?> <?php echo $floor;?></p>
              </div>
            </div>
            <div class="icon-box media mb-15"> <a class="media-left pull-left flip mr-15" href="#"> <i class="pe-7s-call text-theme-colored"></i></a>
              <div class="media-body">
                <h5 class="mt-0">Contact Number</h5>
                <p><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></p>
              </div>
            </div>
            <div class="icon-box media mb-15"> <a class="media-left pull-left flip mr-15" href="#"> <i class="pe-7s-mail text-theme-colored"></i></a>
              <div class="media-body">
                <h5 class="mt-0">Email Address</h5>
                <p><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a></p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
    
    <!-- Divider: Google Map -->
    <section>
      <div class="container-fluid p-0">
        <div class="row">

          <!-- Google Map HTML Codes -->
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d249.29474689343533!2d36.7612284!3d-1.3468982!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4973e27d12a0b706!2sGametrackers+K+ltd!5e0!3m2!1sen!2ske!4v1510737346233" width="100%" height="475" frameborder="0" style="border:0" allowfullscreen></iframe>
          <div class="map-popupstring hidden" id="popupstring1">
            <div class="text-center">
              <h3><?php echo $company_name;?></h3>
              <p><?php echo $address;?> - <?php echo $post_code;?> <?php echo $location;?> <?php echo $building;?> <?php echo $floor;?></p>
            </div>
          </div>
            <!-- Google Map Javascript Codes -->
            <script src="http://maps.google.com/maps/api/js?key=AIzaSyAYWE4mHmR9GyPsHSOVZrSCOOljk8DU9B4"></script>
            <!-- <script src="js/google-map-init.js"></script> -->

        </div>
      </div>
    </section>
    <script type="text/javascript">
        
        $(document).ready(function() 
		{
			generate_map();
        }); 
	</script>