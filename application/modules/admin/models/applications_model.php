<?php

class Applications_model extends CI_Model 
{	

	/*
	*	Retrieve all categories
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_applicants($table, $where, $per_page, $page, $order = 'applicant_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('post.post_title, applications.*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_conservancies($table, $where, $per_page, $page, $order = 'applicant_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('applications.*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	/*
	*	Activate a deactivated applicant
	*	@param int $applicant_id
	*
	*/
	public function activate_applicant($applicant_id)
	{
		$data = array(
				'applicant_status' => 1
			);
		$this->db->where('applicant_id', $applicant_id);
		
		if($this->db->update('applications', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated applicant
	*	@param int $applicant_id
	*
	*/
	public function deactivate_applicant($applicant_id)
	{
		$data = array(
				'applicant_status' => 0
			);
		$this->db->where('applicant_id', $applicant_id);
		
		if($this->db->update('applications', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_conservation_personnel($conservation_id)
	{
		$this->db->where('registration_id',$conservation_id);
		$query =  $this->db->get('personnel');

		return $query;

	}
}
?>