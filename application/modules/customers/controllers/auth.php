<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MX_Controller 
{
	var $csv_path;
	var $customers_path;
	var $customer_document_location;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/file_model');
		$this->load->model('customers_model');
		$this->load->model('site/email_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->customers_path = realpath(APPPATH . '../assets/customers');
		$this->customer_document_location = base_url().'assets/customers/';
	}
	
	public function login_customer() 
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$v_data['customer_password_error'] = '';
		$v_data['customer_email_error'] = '';
		
		//form validation rules
		$this->form_validation->set_rules('customer_password', 'Password', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean|exists[customer.customer_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			// $this->session->sess_destroy();
			//check if user has valid login credentials
			if($this->customers_model->validate_customer())
			{
				$customer_id = $this->session->userdata('customer_id');
				$this->session->set_userdata('success_message', 'Welcome back');
				$customer_status = $this->session->userdata('customer_status');

				$this->session->unset_userdata('applicant_login_status');
				$this->session->unset_userdata('applicant_id');

				redirect('customer/profile');
			}
			
			else
			{
				$this->session->set_userdata('login_error', 'The email or password provided is incorrect. Please try again');
				$v_data['customer_email'] = set_value('customer_email');
				$v_data['customer_password'] = set_value('customer_password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['customer_password_error'] = form_error('customer_password');
				$v_data['customer_email_error'] = form_error('customer_email');
				
				//repopulate fields
				$v_data['customer_password'] = set_value('customer_password');
				$v_data['customer_email'] = set_value('customer_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['customer_password'] = "";
				$v_data['customer_email'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('customer_signin', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function login_conservancy()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$v_data['customer_password_error'] = '';
		$v_data['customer_email_error'] = '';
		
		//form validation rules
		$this->form_validation->set_rules('customer_password', 'Password', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean|exists[applications.username]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		//if form has been submitted


		if ($this->form_validation->run())
		{
			// $this->session->sess_destroy();
			//check if user has valid login credentials
			if($this->customers_model->validate_conservancy_user())
			{
				$applicant_id = $this->session->userdata('applicant_id');
				
				$this->session->unset_userdata('customer_login_status');
				$this->session->unset_userdata('customer_id');


				$this->session->set_userdata('success_message', 'Welcome back');
				$applicant_status = $this->session->userdata('applicant_status');
				
				
				$this->session->set_userdata('success_message', 'Welcome Back');

				$applicant_type =  $this->session->userdata('applicant_type');
				// var_dump($applicant_type); die();
				if($applicant_type == 1)
				{
					redirect('conservancy/profile');
				}
				else
				{
					redirect('member/profile');
				}
				
			}
			
			else
			{
				$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
				
				redirect('conservation-registration');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$this->session->set_userdata('login_error', 'Ensure you have entered a username and password');
				
				redirect('conservation-registration');
			}
			
			//populate form data on initial load of page
			else
			{	
				$this->session->set_userdata('login_error', 'Ensure you have entered a username and password');
				
				redirect('conservation-registration');
			}
		}
	}

	public function register_customer()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('customer_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'New Password', 'required|xss_clean');
		// var_dump($_POST); die();
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('password') == $this->input->post('confirm_password'))
			{
				if($this->customers_model->register_customer())
				{
					$this->session->set_userdata('success_message', 'You have successfully registred. Please login to see your account');					
				}
				else
				{
					$this->session->set_userdata('success_message', 'Sorry Please try again');	
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
		}

		redirect('login');
	}


	public function register_member()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('conservancy_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_email', 'Email', 'required|is_unique[applications.applicant_email]|xss_clean');
		$this->form_validation->set_rules('conservancy_phone', 'Phone', 'required|xss_clean');
		$this->form_validation->set_rules('account_type', 'Account Type', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_contact_person', 'Contact Person', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_descrption', 'Location', 'required|xss_clean');

		$this->form_validation->set_rules('contact_person_email', 'Email', 'required|is_unique[applications.contact_person_email]|xss_clean');


		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->customers_model->register_membership_person())
			{
				$this->session->set_userdata('success_message', 'You have successfully registred. Please wait for our administrator to activate your account');					
			}
			else
			{
				$this->session->set_userdata('success_message', 'Sorry Please try again');	
			}
			
		}
		
		else
		{
			$validation_errors = validation_errors();
		}

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('member_registration', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function register_conservation()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('conservancy_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_email', 'Email', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_phone', 'Phone', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_contact_person', 'Contact Person', 'required|xss_clean');
		$this->form_validation->set_rules('conservancy_descrption', 'Location', 'required|xss_clean');


		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->customers_model->register_conservancy())
			{
				$this->session->set_userdata('success_message', 'You have successfully registred. Please wait for our administrator to activate your account');					
			}
			else
			{
				$this->session->set_userdata('success_message', 'Sorry Please try again');	
			}
			
		}
		
		else
		{
			$validation_errors = validation_errors();
		}

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('customer_registration', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	
	public function change_password()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_new_password'))
			{
				if($this->customers_model->change_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
					redirect('customer/profile');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
		}
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('reset_password1', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	public function change_institution_password()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Confirm New Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_password'))
			{
				if($this->customers_model->change_institution_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
					
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata('error_message', 'Ensure that you have filled in all the fields');
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}


	public function change_member_password()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Confirm New Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_password'))
			{
				if($this->customers_model->change_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
					
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata('error_message', 'Ensure that you have filled in all the fields');
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}


	public function update_member_profile($customer_id)
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		

		
		$this->form_validation->set_rules('customer_phone', 'Phone', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean');
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->customers_model->change_member_profile($customer_id))
			{
				$this->session->set_userdata('success_message', 'Your password has been changed successfully');
				
			}
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata('error_message', 'Ensure that you have filled in all the fields');
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	public function reset_password6()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_new_password'))
			{
				if($this->customers_model->change_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
					redirect('customer/profile');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
		}
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('change_password', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	
	public function logout_customer()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	public function logout_conservancy()
	{
		$this->session->sess_destroy();
		redirect('conservation-registration');
	}
	public function logout_member()
	{
		$this->session->sess_destroy();
		redirect('membership-registration');
	}
	public function reset_password() 
	{
		$v_data['member_email_error'] = $v_data['member_email'] = '';
		
		//form validation rules

		$this->form_validation->set_rules('applicant_email', 'Email', 'required|xss_clean|exists[applications.applicant_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			$return = $this->customers_model->reset_password2();
			if($return['status'] != FALSE)
			{
				$this->session->set_userdata('success_message', 'Your password has been reset successfully. Please check your email for instructions.');
				redirect('membership-registration');
			}
			
			else
			{
				$this->session->set_userdata('login_error', $return['message']);
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['customer_email_error'] = form_error('customer_email');
				
				//repopulate fields
				$v_data['applicant_email'] = set_value('applicant_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['applicant_email'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $data['contacts'] = $contacts;
		
		$data['content'] = $this->load->view('institution_reset', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	public function reset_member_password()
	{
		$v_data['member_email_error'] = $v_data['member_email'] = '';
		
		//form validation rules

		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean|exists[customer.customer_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			$return = $this->customers_model->reset_member_password();
			if($return['status'] != FALSE)
			{
				$this->session->set_userdata('success_message', 'Your password has been reset successfully. Please check your email for instructions.');
				redirect('login');
			}
			
			else
			{
				$this->session->set_userdata('login_error', $return['message']);
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['customer_email_error'] = form_error('customer_email');
				
				//repopulate fields
				$v_data['customer_email'] = set_value('customer_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['customer_email'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $data['contacts'] = $contacts;
		
		$data['content'] = $this->load->view('reset_password', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	public function reset_password3() 
	{
		$v_data['applicant_email_error'] = $v_data['applicant_email'] = '';
		
		//form validation rules

		$this->form_validation->set_rules('applicant_email', 'Email', 'required|xss_clean|exists[applications.applicant_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			$return = $this->customers_model->reset_password3();
			if($return['status'] != FALSE)
			{
				$this->session->set_userdata('success_message', 'Your password has been reset successfully. Please check your email for instructions.');
				redirect('conservation-registration');
			}
			
			else
			{
				$this->session->set_userdata('login_error', $return['message']);
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['applicant_email_error'] = form_error('applicant_email');
				
				//repopulate fields
				$v_data['applicant_email'] = set_value('applicant_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['applicant_email'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $data['contacts'] = $contacts;
		
		$data['content'] = $this->load->view('reset_password1', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function add_conservancy_user($conservancy_id) 
	{
		
		$this->form_validation->set_rules('username'.$conservancy_id, 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('name'.$conservancy_id, 'Email', 'required|xss_clean');
		// var_dump($_POST); die();

		$redirect_url = $this->input->post('redirect_url');
		// var_dump($redirect_url); die();
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			$return = $this->customers_model->add_conservancy_user($conservancy_id);
			if($return['status'] != FALSE)
			{
				$this->session->set_userdata('success_message', 'Your password has been reset successfully. Please check your email for instructions.');
				
			}
			
			else
			{
				$this->session->set_userdata('login_error', $return['message']);
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			
		}

		redirect($redirect_url);
	}
}