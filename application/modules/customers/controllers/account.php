<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/site/controllers/site.php";
class Account extends site 
{
	var $csv_path;
	var $customers_path;
	var $customer_document_location;
	var $branches_path;
	var $branches_location;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/file_model');
		$this->load->model('customers_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->customers_path = realpath(APPPATH . '../assets/customers');
		$this->customer_document_location = base_url().'assets/customers/';


		//path to image directory
		$this->branches_path = realpath(APPPATH . '../assets/customers');
		$this->branches_location = base_url().'assets/customers/';
		
		// $customer_login_status = $this->session->userdata('customer_login_status');
		// $applicant_login_status = $this->session->userdata('applicant_login_status');
		// if(!$customer_login_status || $applicant_login_status)
		// {
		// 	$this->session->set_userdata('error_message', 'Please log in to continue');
		// 	redirect('login');
		// }
	}
	
	public function profile() 
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$v_data['customer_password_error'] = '';
		$v_data['customer_email_error'] = '';
		$customer_login_status = $this->session->userdata('customer_login_status');
		if(!$customer_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('conservation-registration');
		}
		
		//form validation rules
		$this->form_validation->set_rules('customer_password', 'Password', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean|exists[customer.customer_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			if($this->customers_model->validate_customer())
			{
				$customer_id = $this->session->userdata('customer_id');
				$customer_password_readable = $this->session->userdata('customer_password_readable');
				
				if(empty($customer_password_readable))
				{
					$this->session->set_userdata('success_message', 'Welcome back');
					$customer_status = $this->session->userdata('customer_status');
					redirect('customer/profile');
				}
				
				else
				{
					$this->session->set_userdata('success_message', 'Welcome back. Your password was reset. Kindly update it to continue');
					redirect('customer/change-password');
				}
			}
			
			else
			{
				$this->session->set_userdata('login_error', 'The email or password provided is incorrect. Please try again');
				$v_data['customer_email'] = set_value('customer_email');
				$v_data['customer_password'] = set_value('customer_password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['customer_password_error'] = form_error('customer_password');
				$v_data['customer_email_error'] = form_error('customer_email');
				
				//repopulate fields
				$v_data['customer_password'] = set_value('customer_password');
				$v_data['customer_email'] = set_value('customer_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['customer_password'] = "";
				$v_data['customer_email'] = "";
			}
		}
		$customer_id = $this->session->userdata('customer_id');
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		// $query2 = $this->customers_model->get_customer_documents($customer_id);
		// $v_data['document_types'] = $this->personnel_model->all_document_types();
		$v_data['customer_document_location'] = $this->customer_document_location;
		// $v_data['documents'] = $query2;
		
		$data['content'] = $this->load->view('profile', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function my_donations()
	{

		$applicant_login_status = $this->session->userdata('applicant_login_status');
		$customer_login_status = $this->session->userdata('customer_login_status');

		// var_dump($applicant_login_status); die();
		if(!$customer_login_status AND !$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('login');
		}


		$customer_id = $this->session->userdata('customer_id');
		$applicant_id = $this->session->userdata('applicant_id');
		if(!empty($customer_id))
		{
			$add = ' AND customer_donations.customer_id = '.$applicant_id;
			$location = 'my-donations';
		}
		else if(!empty($applicant_id))
		{
		    $add = ' AND customer_donations.member_id = '.$applicant_id;
		    $location = 'member-contributions';
		}
		$where = 'post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 '.$add;
		$table = 'post, customer_donations';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().$location;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_contributions($table, $where, $config["per_page"], $page, $order='customer_donations.customer_donation_id', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Donations';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('donations', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function my_transactions()
	{

		$applicant_login_status = $this->session->userdata('applicant_login_status');
		$customer_login_status = $this->session->userdata('customer_login_status');
		if(!$customer_login_status AND !$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('login');
		}


		$customer_id = $this->session->userdata('customer_id');
		$applicant_id = $this->session->userdata('applicant_id');
		if(!empty($customer_id))
		{
			$add = ' AND customer_donations.customer_id = '.$applicant_id;
			$location = 'my-transactions';
		}
		else if(!empty($applicant_id))
		{
		    $add = ' AND customer_donations.member_id = '.$applicant_id;
		    $location = 'member-transactions';
		}
		$where = 'post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 '.$add;
		$table = 'post, customer_donations';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().$location;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_transactions($table, $where, $config["per_page"], $page, $order='customer_donations.customer_donation_id', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Donations';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('transactions', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function causes()
	{
		
		$where = 'post.blog_category_id = blog_category.blog_category_id AND post.post_status = 1 AND (blog_category.blog_category_name = "Company Programmes" OR blog_category.blog_category_name = "Company Initiatives")';
		$table = 'post,blog_category';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'causes';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_causes($table, $where, $config["per_page"], $page, $order='post.created', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Donations';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('causes', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}


	public function conservancy_profile() 
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$v_data['customer_password_error'] = '';
		$v_data['customer_email_error'] = '';

		$applicant_login_status = $this->session->userdata('applicant_login_status');
		if(!$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('conservation-registration');
		}
		
		//form validation rules
		$this->form_validation->set_rules('customer_password', 'Password', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean|exists[customer.customer_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			if($this->customers_model->validate_customer())
			{
				$customer_id = $this->session->userdata('customer_id');
				$customer_password_readable = $this->session->userdata('customer_password_readable');
				
				if(empty($customer_password_readable))
				{
					$this->session->set_userdata('success_message', 'Welcome back');
					$customer_status = $this->session->userdata('customer_status');
					redirect('customer/profile');
				}
				
				else
				{
					$this->session->set_userdata('success_message', 'Welcome back. Your password was reset. Kindly update it to continue');
					redirect('customer/change-password');
				}
			}
			
			else
			{
				$this->session->set_userdata('login_error', 'The email or password provided is incorrect. Please try again');
				$v_data['customer_email'] = set_value('customer_email');
				$v_data['customer_password'] = set_value('customer_password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['customer_password_error'] = form_error('customer_password');
				$v_data['customer_email_error'] = form_error('customer_email');
				
				//repopulate fields
				$v_data['customer_password'] = set_value('customer_password');
				$v_data['customer_email'] = set_value('customer_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['customer_password'] = "";
				$v_data['customer_email'] = "";
			}
		}
		$customer_id = $this->session->userdata('customer_id');
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		// $query2 = $this->customers_model->get_customer_documents($customer_id);
		// $v_data['document_types'] = $this->personnel_model->all_document_types();
		$v_data['customer_document_location'] = $this->customer_document_location;
		// $v_data['documents'] = $query2;
		
		$data['content'] = $this->load->view('conservancy_profile', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}


	public function campaigns()
	{

		$applicant_login_status = $this->session->userdata('applicant_login_status');
		if(!$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('conservation-registration');
		}

		$applicant_id = $this->session->userdata('applicant_id');
		$where = 'post.blog_category_id = blog_category.blog_category_id AND post.applicant_id = '.$applicant_id;
		$table = 'post,blog_category';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'campaigns';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_causes($table, $where, $config["per_page"], $page, $order='post.created', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Campaigns';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('campaigns', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}


	public function my_trips()
	{

		$applicant_login_status = $this->session->userdata('applicant_login_status');
		if(!$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('conservation-registration');
		}

		$applicant_id = $this->session->userdata('applicant_id');
		$where = 'trip.applicant_id = '.$applicant_id.' AND trip.conservancy_id = applications.applicant_id';
		$table = 'trip,applications';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'campaigns';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_causes($table, $where, $config["per_page"], $page, $order='trip.created', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Trips';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('trips', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function conservancy_trips()
	{

		$applicant_login_status = $this->session->userdata('applicant_login_status');
		if(!$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('conservation-registration');
		}

		$applicant_id = $this->session->userdata('applicant_id');
		$where = 'trip.conservancy_id = '.$applicant_id.' AND trip.applicant_id = applications.applicant_id';
		$table = 'trip,applications';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'campaigns';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_causes($table, $where, $config["per_page"], $page, $order='trip.created', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Trips';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('conservancy_trips', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	public function campaign_contributions()
	{
		$applicant_login_status = $this->session->userdata('applicant_login_status');
		if(!$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('conservation-registration');
		}

		$applicant_id = $this->session->userdata('applicant_id');

		$where = 'post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 AND customer.customer_id = customer_donations.customer_id AND post.blog_category_id = blog_category.blog_category_id AND post.applicant_id ='.$applicant_id;
		$table = 'post,blog_category,customer_donations,customer';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'campaign-contributions';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_transactions($table, $where, $config["per_page"], $page, $order='customer_donations.customer_donation_id', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;
		
		
		$data['title'] = 'Contributions';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('contributions', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	

	}


	public function member_profile() 
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$v_data['customer_password_error'] = '';
		$v_data['customer_email_error'] = '';

		$applicant_login_status = $this->session->userdata('applicant_login_status');


		if(!$applicant_login_status)
		{
			$this->session->set_userdata('error_message', 'Please log in to continue');
			redirect('membership-registration');
		}
		
		//form validation rules
		$this->form_validation->set_rules('customer_password', 'Password', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'required|xss_clean|exists[customer.customer_email]');
		$this->form_validation->set_message('exists', 'Email not found. Have you registered?');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if user has valid login credentials
			if($this->customers_model->validate_customer())
			{
				$customer_id = $this->session->userdata('customer_id');
				$customer_password_readable = $this->session->userdata('customer_password_readable');
				
				if(empty($customer_password_readable))
				{
					$this->session->set_userdata('success_message', 'Welcome back');
					$customer_status = $this->session->userdata('customer_status');
					redirect('customer/profile');
				}
				
				else
				{
					$this->session->set_userdata('success_message', 'Welcome back. Your password was reset. Kindly update it to continue');
					redirect('customer/change-password');
				}
			}
			
			else
			{
				$this->session->set_userdata('login_error', 'The email or password provided is incorrect. Please try again');
				$v_data['customer_email'] = set_value('customer_email');
				$v_data['customer_password'] = set_value('customer_password');
			}
		}
		
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['customer_password_error'] = form_error('customer_password');
				$v_data['customer_email_error'] = form_error('customer_email');
				
				//repopulate fields
				$v_data['customer_password'] = set_value('customer_password');
				$v_data['customer_email'] = set_value('customer_email');
			}
			
			//populate form data on initial load of page
			else
			{
				$v_data['customer_password'] = "";
				$v_data['customer_email'] = "";
			}
		}
		$customer_id = $this->session->userdata('customer_id');
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		// $query2 = $this->customers_model->get_customer_documents($customer_id);
		// $v_data['document_types'] = $this->personnel_model->all_document_types();



		$v_data['customer_document_location'] = $this->customer_document_location;
		// $v_data['documents'] = $query2;
		
		$data['content'] = $this->load->view('member_profile', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}

	public function add_member_trip()
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('conservancy_id', 'Conservancy', 'required|xss_clean');
		$this->form_validation->set_rules('trip_persons', 'No of persons', 'required|xss_clean');
		$this->form_validation->set_rules('trip_start_date', 'Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('trip_end_date', 'End date', 'required|xss_clean');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person', 'required|xss_clean');
		$this->form_validation->set_rules('contact_person_phone', 'Contact Person', 'required|xss_clean');
		$this->form_validation->set_rules('contact_person_email', 'Contact Person', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->customers_model->add_member_trip())
			{
				$this->session->set_userdata('success_message', 'You have registered successfully for a trip');	

				redirect('member/profile');				
			}
			else
			{
				$this->session->set_userdata('success_message', 'Sorry Please try again');	
			}
			
		}
		
		else
		{
			$validation_errors = validation_errors();
		}

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('add_trip', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}


	public function edit_member_trip($trip_id)
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$this->form_validation->set_rules('conservancy_id', 'Conservancy', 'required|xss_clean');
		$this->form_validation->set_rules('trip_persons', 'No of persons', 'required|xss_clean');
		$this->form_validation->set_rules('trip_start_date', 'Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('trip_end_date', 'End date', 'required|xss_clean');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person', 'required|xss_clean');
		$this->form_validation->set_rules('contact_person_phone', 'Contact Person', 'required|xss_clean');
		$this->form_validation->set_rules('contact_person_email', 'Contact Person', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->customers_model->edit_member_trip($trip_id))
			{
				$this->session->set_userdata('success_message', 'You have successfully edited trip details');	

				redirect('member/profile');				
			}
			else
			{
				$this->session->set_userdata('success_message', 'Sorry Please try again');	
			}
			
		}
		
		else
		{
			$validation_errors = validation_errors();
		}

		$v_data['trip_details'] = $trip_details = $this->customers_model->get_trip_details($trip_id);
		$conservancy_name = '';
		if($trip_details->num_rows() > 0)
		{
			foreach ($trip_details->result() as $key => $value) {
				# code...
				$conservancy_name = $value->applicant_name;
			}
		}
        
		$data['title'] = $conservancy_name; //$this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('edit_trip', $v_data, true);
		
		$this->load->view('site/templates/general_page', $data);
	}
	
	public function approve_trip($trip_id)
	{
		$applicant_id = $this->session->userdata('applicant_id');
		$this->db->where('conservancy_id = '.$applicant_id.' AND trip_id ='.$trip_id);
		$data = array('approved'=>1);

		if($this->db->update('trip',$data))
		{
			$this->session->set_userdata('success_message', 'The trip was successfully approved');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something when wrong please try again');
		}

		redirect('conservancy/profile');
	}

	public function cancel_trip($trip_id)
	{
		$applicant_id = $this->session->userdata('applicant_id');
		$this->db->where('conservancy_id = '.$applicant_id.' AND trip_id ='.$trip_id);
		$data = array('approved'=>2);

		if($this->db->update('trip',$data))
		{
			$this->session->set_userdata('success_message', 'The trip was successfully approved');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something when wrong please try again');
		}
		redirect('conservancy/profile');
	}


	public function update_profile_picture()
	{

		$this->form_validation->set_rules('branch_image', 'Profile Picture', 'required|xss_clean');
		
		$customers_path = $this->branches_path;

		$attachment_name = '';
		if(is_uploaded_file($_FILES['attachment']['tmp_name']))
		{
			/*
				-----------------------------------------------------------------------------------------
				Upload image
				-----------------------------------------------------------------------------------------
			*/

			$response = $this->file_model->upload_any_file($customers_path, 'attachment');
			if($response['check'])
			{
				$attachment_name = $response['file_name'];

				if($this->customers_model->update_profile_picture($attachment_name,null))
				{
					$this->session->set_userdata('success_message', 'Profile picture successfully added');


				}else
				{
					$this->session->set_userdata('error_message', 'Sorry something went wrong please try again');
				}
			}
		
			else
			{
				$this->session->set_userdata('error_message', $response['error']);
			}
		}
		
		


		$redirect_url = $this->input->post('redirect-url');

		redirect($redirect_url);
	}


	public function update_member_picture()
	{

		
		$customers_path = $this->branches_path;

		$attachment_name = '';
		if(is_uploaded_file($_FILES['attachment']['tmp_name']))
		{

			$response = $this->file_model->upload_any_file($customers_path, 'attachment');
			if($response['check'])
			{
				$attachment_name = $response['file_name'];

				if($this->customers_model->update_member_picture($attachment_name,null))
				{
					$this->session->set_userdata('success_message', 'Profile picture successfully added');


				}else
				{
					$this->session->set_userdata('error_message', 'Sorry something went wrong please try again');
				}
			}
		
			else
			{
				$this->session->set_userdata('error_message', $response['error']);
			}
		}
		
		


		$redirect_url = $this->input->post('redirect-url');

		redirect($redirect_url);
	}


}