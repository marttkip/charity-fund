<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MX_Controller 
{
	var $csv_path;
	var $customers_path;
	var $customer_document_location;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('customers_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/file_model');
		$this->load->model('site/auth_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->customers_path = realpath(APPPATH . '../assets/customers');
		$this->customer_document_location = base_url().'assets/customers/';
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($page_name) 
	{

		$page = explode("/",uri_string());
		
		$name = strtolower($page[0]);
		$item = strtolower($page[1]);
		// var_dump($item); die();

		if($item == 'all-users')
		{
			$item = 'Donors';
		}
		else if($item == 'all-active-users')
		{
			$item = 'Donors';
		}
		else 
		{
			
			$item = $item;
		}

		$v_data['page_name'] = $this->site_model->decode_web_name($page_name) ;
		$decoded_web_name = $this->site_model->decode_web_name($item);
		// var_dump($decoded_web_name); die();

		$where = 'customer.customer_type_id = customer_type.customer_type_id AND customer_type.customer_type_name LIKE \'%'.$item.'%\'';
		$table = 'customer, customer_type';
		
		//pagination


		

		$v_data['data_name'] = $name;
		$segment = 3;
		$order = 'customer_surname';
		$order_method = 'ASC';
		$this->load->library('pagination');
		$config['base_url'] = $this->uri->uri_string();
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_customers($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Customers';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('customers/customers/all_customers', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Add a new customer
	*
	*/
	public function add_customer($item) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('customer_surname', 'Surname', 'xss_clean');
		$this->form_validation->set_rules('customer_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('customer_rate', 'Saloon Rate', 'xss_clean');
		$this->form_validation->set_rules('customer_van rate', 'Van Rate', 'xss_clean');
		$this->form_validation->set_rules('customer_password_readable', 'New Password', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$is_company = 0;
			$customer_id = $this->customers_model->add_customer($is_company,$item);
			if($customer_id)
			{
				$customer_email = $this->input->post('customer_email');
				$customer_first_name = $this->input->post('customer_first_name');
				$new_password = $this->input->post('customer_password_readable');


				//send registration email
			    // $email_reply = $this->auth_model->send_registration_email($customer_email, $customer_first_name);

			   $this->customers_model->send_customer_password($customer_first_name, $customer_id, $customer_email, $new_password);
				$this->session->set_userdata('success_message', 'Customer added successfully');
				redirect('customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Customer. Please try again');
			}
		}
		
		//open the add new Customers
		$data['title'] = 'Add Customer';
		$v_data['item'] = $item;
		$v_data['title'] = $data['title'];


	
		$data['content'] = $this->load->view('customers/customers/add_customer', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

   /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_customer($customer_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_surname', 'Surname', 'xss_clean');
		$this->form_validation->set_rules('customer_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('customer_rate', 'Saloon Rate', 'xss_clean');
		$this->form_validation->set_rules('customer_van rate', 'Van Rate', 'xss_clean');
		$this->form_validation->set_rules('customer_password_readable', 'New Password', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			$is_company = 0;
			if($this->customers_model->update_customer($customer_id, $is_company))
			{
				$this->session->set_userdata('success_message', 'Customer updated successfully');
				redirect('customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Customer. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Customer';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer($customer_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['customer'] = $query->result();
			$data['content'] = $this->load->view('customers/customers/edit_customer', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Customer does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		if($this->customers_model->deactivate_customer($customer_id))
		{
			$this->session->set_userdata('success_message', 'Customer disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be disabled. Please try again');
		}
		redirect('customers');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_customer($customer_id)
	{
		if($this->customers_model->activate_customer($customer_id))
		{
			$this->session->set_userdata('success_message', 'Customer activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be activated. Please try again');
		}
		redirect('customers');
	}



	/*
	*
	*	Add a new company
	*
	*/
	public function add_company($page_name) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_first_name', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('customer_type_id', 'Company Type', 'xss_clean');
		$this->form_validation->set_rules('customer_surname', 'Company Registration', 'xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('customer_rate', 'Saloon Rate', 'xss_clean');
		$this->form_validation->set_rules('customer_van rate', 'Van Rate', 'xss_clean');

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$is_company = 1;
			
			if($this->customers_model->add_customer($is_company,$page_name))
			{
				$this->session->set_userdata('success_message', 'Company added successfully');
				redirect('members/'.$page_name);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Campany. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Add '.$page_name;
		$v_data['item'] = $page_name;
		$v_data['title'] = $data['title'];
		$v_data['customer_types'] = $this->customers_model->get_customer_types();
	
		$data['content'] = $this->load->view('customers/customers/add_company', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Edit an existing Company
	*	@param int $vehicle_id
	*
	*/
	public function edit_company($customer_id,$page_name) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_first_name', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('customer_type_id', 'Company Type', 'xss_clean');
		$this->form_validation->set_rules('customer_surname', 'Company Registration', 'xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('customer_rate', 'Saloon Rate', 'xss_clean');
		$this->form_validation->set_rules('customer_van rate', 'Van Rate', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			$is_company = 1;
			if($this->customers_model->update_customer($customer_id, $is_company,$page_name))
			{
				$this->session->set_userdata('success_message', 'Company updated successfully');
				redirect('members/'.$page_name);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Company. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Company';
		$v_data['title'] = $data['title'];
		$v_data['item'] = $page_name;
		$v_data['customer_types'] = $this->customers_model->get_customer_types();
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer($customer_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['customer'] = $query->result();
			$data['content'] = $this->load->view('customers/customers/edit_company', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Company does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}



	/*
	*
	*	Add a new company_contacts
	*
	*/
	public function add_company_contact($company_id) 

	{
			
		//form validation rules
		$this->form_validation->set_rules('customer_contacts_sur_name', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_email', 'Email', 'required|xss_clean');
		

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->customers_model->add_company_contact($company_id))
			{
				$this->session->set_userdata('success_message', 'Company Contact added successfully');
				redirect('customers/add_person/'.$company_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Company Contact. Please try again');
			}
		}
		

        $order = 'customer_contacts_first_name';
		$order_method = 'ASC';
		$where = 'customer_id = '.$company_id;
		$table = 'customer_contacts';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'customers/add_person/'.$company_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query_sent = $this->customers_model->get_all_customer_contacts($table, $where, $config["per_page"], $page, $order, $order_method);
		//var_dump($query_sent); die();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		
		
		//open the add new Customers
		
		 $data['title'] = 'Add Company Contact Persons';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query_sent;
		$v_data['page'] = $page;
	
		$data['content'] = $this->load->view('customers/customers/add_company_contact', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_company_contact($customer_contact_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('customer_contacts_sur_name', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_contacts_email', 'Email', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->customers_model->update_company_contact($customer_contact_id))
			{
				$this->session->set_userdata('success_message', 'Company Contact updated successfully');
				redirect('customers');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Company Contact. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Company Contact';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer_contact($customer_contact_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['customer_contacts'] = $query->result();
			$data['content'] = $this->load->view('customers/customers/edit_company_contact', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Customer Contact does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		if($this->customers_model->deactivate_customer_contact($customer_contact_id))
		{
			$this->session->set_userdata('success_message', 'Customer Contact disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer could not be disabled. Please try again');
		}
		redirect('customers');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		if($this->customers_model->activate_customer_contact($customer_contact_id))
		{
			$this->session->set_userdata('success_message', 'Customer Contact activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Customer Contact could not be activated. Please try again');
		}
		redirect('customers');
	}

	/*
	*
	*	Add a new customer
	*
	*/
	public function add_vehicle() 
	{
		//form validation rules
		$this->form_validation->set_rules('ride_type_id', 'Type', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_plate', 'Licence Plate', 'is_unique[vehicle.vehicle_plate]|required|xss_clean');
		$this->form_validation->set_rules('vehicle_status', 'Vehicle Status', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_owner', 'Vehicle Owner', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_driver', 'Vehicle Driver', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_description', 'Vehicle Description', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->customers_model->add_vehicle())
			{
				$this->session->set_userdata('success_message', 'Vehicle added successfully');
				redirect('bike/customer');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add vehicle. Please try again');
			}
		}
		
		//open the add new vehicle
		
		$data['title'] = 'Add vehicle';
		$v_data['title'] = $data['title'];
		$v_data['ride_types'] = $this->customers_model->get_ride_types();
		$v_data['owners'] = $this->customers_model->get_personnel_type(4);
		$v_data['drivers'] = $this->customers_model->get_personnel_type(3);
		$data['content'] = $this->load->view('customer/add_vehicle', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_vehicle($vehicle_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('ride_type_id', 'Type', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_plate', 'Licence Plate', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_status', 'Vehicle Status', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_owner', 'Vehicle Owner', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_driver', 'Vehicle Driver', 'required|xss_clean');
		$this->form_validation->set_rules('vehicle_description', 'Vehicle Description', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->customers_model->update_vehicle($vehicle_id))
			{
				$this->session->set_userdata('success_message', 'Vehicle updated successfully');
				redirect('bike/customer');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update vehicle. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit vehicle';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_vehicle($vehicle_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['vehicle'] = $query->result();
			$v_data['ride_types'] = $this->customers_model->get_ride_types();
			$v_data['owners'] = $this->customers_model->get_personnel_type(4);
			$v_data['drivers'] = $this->customers_model->get_personnel_type(3);
			
			$data['content'] = $this->load->view('customer/edit_vehicle', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Vehicle does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function delete_vehicle($vehicle_id)
	{
		if($this->customers_model->delete_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be deleted. Please try again');
		}
		redirect('bike/customer');
	}
    
	/*
	*
	*	Delete an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function delete_customer($customer_id)
	{
		if($this->customers_model->delete_customer($customer_id))
		{
			$this->session->set_userdata('success_message', 'customer has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'customer could not be deleted. Please try again');
		}
		redirect('customers');
	}
    
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_vehicle($vehicle_id)
	{
		if($this->customers_model->activate_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be activated. Please try again');
		}
		redirect('bike/customer');
	}
    
	/*
	*
	*	Deactivate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_vehicle($vehicle_id)
	{
		if($this->customers_model->deactivate_vehicle($vehicle_id))
		{
			$this->session->set_userdata('success_message', 'Vehicle disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Vehicle could not be disabled. Please try again');
		}
		redirect('bike/customer');
	}
	
	function import_customers_template()
	{
		//export products template in excel 
		$this->customers_model->import_customers_template();
	}
	//do the customers import
	function do_customers_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->customers_model->import_csv_companies($this->csv_path);
				
				if($response == FALSE)
				{
					$this->session->set_userdata('error_message', 'Something went wrong. Please try again.');
				}
				
				else
				{
					if($response['check'])
					{
						$this->session->set_userdata('success_message', $response['response']);
					}
					
					else
					{
						$this->session->set_userdata('error_message', $response['response']);
					}
				}
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Please select a file to import.');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Please select a file to import.');
		}
		
		redirect('customers');
	}
	
	function import_companies_template()
	{
		//export products template in excel 
		$this->customers_model->import_companies_template();
	}
	
	//do the companies import
	function do_companies_import($company_status)
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->customers_model->import_csv_customers($this->csv_path, $company_status);
				
				if($response == FALSE)
				{
					$this->session->set_userdata('error_message', 'Something went wrong. Please try again.');
				}
				
				else
				{
					if($response['check'])
					{
						$this->session->set_userdata('success_message', $response['response']);
					}
					
					else
					{
						$this->session->set_userdata('error_message', $response['response']);
					}
				}
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Please select a file to import.');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Please select a file to import.');
		}
		
		redirect('customers');
	}

   /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function customer_documents($customer_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('document_type_id', 'Document Type', 'required|xss_clean');
		$this->form_validation->set_rules('customer_document_name', 'Document Name', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$file_name = '';
			if(is_uploaded_file($_FILES['customer_document_file']['tmp_name']))
			{
				$this->load->library('image_lib');
				
				$customers_path = $this->customers_path;
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_document($customers_path, 'customer_document_file');
				if($response['check'])
				{
					$file_name = $response['file_name'];
				}
			}
			//update vehicle
			$is_company = 0;
			if($this->customers_model->add_customer_document($customer_id, $file_name))
			{
				$this->session->set_userdata('success_message', 'Document added successfully');
				redirect('customers/customer-documents/'.$customer_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add document. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Customer Documents';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->customers_model->get_customer($customer_id);
		$query2 = $this->customers_model->get_customer_documents($customer_id);
		$v_data['document_types'] = $this->personnel_model->all_document_types();
		$v_data['customer'] = $query->result();
		$v_data['customer_document_location'] = $this->customer_document_location;
		$v_data['documents'] = $query2;
		$data['content'] = $this->load->view('customers/customers/customer_documents', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function delete_document_scan($document_upload_id, $customer_id)
	{
		if($this->customers_model->delete_document_scan($document_upload_id))
		{
			$this->session->set_userdata('success_message', 'Document has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Document could not deleted');
		}
		redirect('customers/customer-documents/'.$customer_id);
	}
	
	public function update_customers_password()
	{
		//get all paid customers for the current year
		$current_year = date('Y');
		$where = 'customer.customer_status = 1';
		
		$this->db->where($where);
		$query = $this->db->get('customer');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$customer_id = $res->customer_id;
				$new_password = substr(md5($customer_email), 0, 6);
				$customer_email = $res->customer_email;
				$customer_first_name = $res->customer_first_name;
				$customer_number = $res->customer_number;
				
				$data_update['customer_password'] = md5($new_password);
				$data_update['customer_password_readable'] = $new_password;
				
				$this->db->where('customer_id', $customer_id);
				if($this->db->update('customer', $data_update))
				{
					if(!empty($customer_email))
					{
						$response = $this->customers_model->send_customer_password($customer_first_name, $customer_id, $customer_email, $new_password);
					}
					$this->session->set_userdata('success_message', 'Member passwords updated and sent successfully');
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Could update customer passwords. Please try again');
				}
			}
		}
				
		else
		{
			$this->session->set_userdata('error_message', 'No paid customers found');
		}
		
		redirect('customers');
	}
	
	public function send_password($customer_id)
	{
		$this->db->where('customer_id', $customer_id);
		$query = $this->db->get('customer');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);
				$customer_id = $res->customer_id;
				$customer_email = $res->customer_email;
				$customer_first_name = $res->customer_first_name;
				$customer_number = $res->customer_number;
				$new_password = $res->customer_password_readable;
				
				$data_update['customer_password'] = md5($new_password);
				$data_update['customer_password_readable'] = $new_password;
				
				$this->db->where('customer_id', $customer_id);
				if($this->db->update('customer', $data_update))
				{
					if(!empty($customer_email))
					{
						$response = $this->customers_model->send_customer_password($customer_first_name, $customer_id, $customer_email, $new_password);
					}
					$this->session->set_userdata('success_message', 'Customer password sent successfully');
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Could not send customer passwords. Please try again');
				}
			}
		}
				
		else
		{
			$this->session->set_userdata('error_message', 'No paid customers found');
		}
		
		redirect('customers');
	}

	public function customer_donations()
	{
		$where = 'post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 AND customer_donations.customer_donation_status = 1';
		$table = 'post, customer_donations';	

		//pagination
		$segment = 2;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'donations';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_contributions($table, $where, $config["per_page"], $page, $order='customer_donations.customer_donation_id', $order_method='ASC');
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['contacts'] = $contacts;		
		
		$data['title'] = 'Donations';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('customers/customers/all_donations', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function customer_applications()
	{
		
	}
}
?>