<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">TRIPS</h2>
              
            </div>
          </div>
        </div>
      </div>      
    </section>

    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
              <div class="col-md-12">
                <h3>My Trips 
                	<?php
              		$customer_login_status = $this->session->userdata('customer_login_status');
              		if($customer_login_status == TRUE)
              		{
              			?>
              			<a href="<?php echo site_url().'customer/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to Dashboard</a>
              			<?php
              		}
              		else
              		{
              			?>
              			<a href="<?php echo site_url().'conservancy/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to Dashboard</a>
              			<?php
              		}
              	?> 
                </h3>
				<table class="table table-striped table-bordered tbl-shopping-cart">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Conservancy</th>
                      <th>Trip Start date</th>
                      <th>Trip End date</th>
                      <th>Trip Participants</th>
                      <th>Contact Person</th>
                      <th>Status</th>
                      <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$count = $page;
                  	$total_amount = 0;
                  	if($query->num_rows()>0)
          					{

          						foreach($query->result() as $row)
          						{
          							$trip_id = $row->trip_id;
          							$applicant_name = $row->applicant_name;
          							$trip_persons = $row->trip_persons;
          							$trip_start_date = $row->trip_start_date;
          							$trip_end_date = $row->trip_end_date;
          							$approved = $row->approved;
          							$contact_person_name = $row->contact_person_name;
          							$description = $row->description;
          							$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 100));
          							$created = $row->created;
          							$day = date('j',strtotime($created));
          							$month = date('M Y',strtotime($created));
          							$trip_start_date = date('jS M Y H:i a',strtotime($row->trip_start_date));
                        $trip_end_date = date('jS M Y H:i a',strtotime($row->trip_end_date));
          							
          							$categories = '';

                        if($approved == 0)
                        {
                          $approved_status = 'Processing request';
                        }
                        else if($approved == 1)
                        {
                          $approved_status = 'Approved Trip'; 
                        }
                        else
                        {
                          $approved_status = 'Trip Cancelled ';
                        }
          							$count++;
          							?>
          		                    <tr>
          		                      <td ><?php echo $count?></td>
          		                      <td><?php echo $applicant_name;?></td>
                                    <td><?php echo $trip_start_date;?></td>
                                    <td><?php echo $trip_end_date;?></td>
                                    <td><?php echo $trip_persons;?></td>
                                    <td><?php echo $contact_person_name;?></td>
                                    <td><?php echo $approved_status;?></td>
                                    <td>
                                    	 <a href="<?php echo site_url().'approve-trip/'.$trip_id.''?>" class="btn btn-success btn-flat btn-sm pull-right" data-loading-text="Please wait..." onclick="return confirm('Are you sure you want to approve this trip ?')" style="margin-right:5px;"><i class="fa fa-correct"></i> Approve Trip</a>
	                                    
	                                </td>
	                                <td>
	                                   
	                                    <a href="<?php echo site_url().'cancel-trip/'.$trip_id.''?>" class="btn btn-danger btn-flat btn-sm pull-right" data-loading-text="Please wait..." onclick="return confirm('Are you sure you want to cancel this trip ?')" style="margin-right:5px;"><i class="fa fa-trash"></i> Cancel Trip</a>
	                                </td>
          		                    </tr>
          		                <?php

          		            }
          		      }
		                ?>
                   
                   
                  </tbody>
                </table>
            </div>
           </div>
          </div>
         </div>
     </section>