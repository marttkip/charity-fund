<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-10">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36"><?php echo $title;?></h2>
              
            </div>
          </div>
        </div>
      </div>      
    </section>

    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
              <div class="col-md-12">
                <h3><?php echo $title;?> <a href="<?php echo site_url().'conservancy/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to Dashboard</a> <a href="<?php echo site_url().'add-campaign'?>" class="btn btn-success btn-flat btn-sm pull-right" data-loading-text="Please wait..." style="margin-right:5px;"><i class="fa fa-plus"></i> Add Campaign</a></h3>

                  	<?php
                  	if($query->num_rows()>0)
					{
						foreach($query->result() as $row)
						{
							$post_id = $row->post_id;
							// $blog_category_name = $row->blog_category_name;
							$blog_category_id = $row->blog_category_id;
							$post_image = $row->post_image;
							$post_content = $row->post_content;
							$post_title = $row->post_title;
							$post_comments = $row->post_comments;
							$post_status = $row->post_status;
							$post_views = $row->post_views;
							$post_target = $row->post_target;
							$image = base_url().'assets/images/posts/'.$row->post_image;
							$web_name = $this->site_model->create_web_name($post_title);
							$created_by = $row->created_by;
							$modified_by = $row->modified_by;
							$total_comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
							$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
							$description = $row->post_content;
							$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 100));
							$created = $row->created;
							$day = date('j',strtotime($created));
							$month = date('M Y',strtotime($created));
							$created_on = date('jS M Y H:i a',strtotime($row->created));
							
							$categories = '';
							$count = 0;
							$post_target = $row->post_target;
							$raised = $this->site_model->get_project_contributions($post_id);
							$contributors = $this->site_model->get_project_contributors($post_id);

							if($post_target == 0)
							{
							$post_target = 1;
							$value_post = 0;
							}
							else
							{
							$value_post = $post_target;
							}


						    if($raised == 0)
						    {
						      $percent = 0;
						    }
						    else
						    {
						      
						     $percent = ($raised / $value_post) * 100; 
						    }
							?>
							<div class="upcoming-events box-hover-effect effect1 media maxwidth400 bg-light mb-20">
					              <div class="row equal-height">
					                <div class="col-sm-4 pr-0 pr-sm-15" style="min-height: 16.31em;">
					                  <div class="thumb p-15">
					                    <img class="img-fullwidth" src="<?php echo $image?>" alt="...">
					                  </div>
					                </div>
					                <div class="col-sm-4 border-right pl-0 pl-sm-15" style="min-height: 16.31em;">
					                  <div class="event-details p-15 mt-20">
					                    <h4 class="media-heading text-uppercase font-weight-500"><?php echo $post_title;?></h4>
					                    <p><?php echo $mini_desc?></p>
					                    <a href="<?php echo site_url().'view-project/'.$web_name.'/'.$post_id?>" class="text-theme-colored">Details <i class="fa fa-angle-double-right"></i></a>
					                  </div>
					                </div>
					                <div class="col-sm-4" style="min-height: 16.31em;">
					                  <div class="event-count causes p-15 mt-15">
					                    
					                    <?php

					                      if($post_status == 1)
					                      {
					                      	?>
					                      	<div class="progress-item mt-20 mb-40">
						                      <div class="progress mb-30">
						                        <div class="progress-bar appeared" data-percent="<?php echo $percent?>" style="width: 85%;"><span class="percent"><?php echo $percent?></span></div>
						                      </div>
						                    </div>
						                    <ul class="list-inline clearfix">
						                      <li class="pull-left pr-0">Raised: <?php echo $raised?></li>
						                      <li class="text-theme-colored pull-right pr-0">Goal: <?php echo $value_post?></li>
						                    </ul>
						                    <?php
						                     }
						                     else
						                     {
						                     	?>
						                     	<div class="alert alert-warning"> Waiting for the administrator to approve</div>
						                     	<?php
						                     }
						                    ?>
					                    <div class="mt-10">
					                      <ul class="pull-left list-inline mt-15">
					                        <li class="pr-0"><i class="fa fa-heart-o text-theme-colored"></i> <?php echo $contributors?> Donor(s)</li>
					                      </ul>
					                      <?php

					                      if($post_status == 1)
					                      {
					                      	?>
					                      		<a href="<?php echo site_url().'view-project/'.$web_name.'/'.$post_id?>" class="btn btn-dark btn-flat btn-sm pull-right mb-10">Donations</a>
					                      	<?php
					                      }

					                      ?>

					                      <a href="<?php echo site_url().'edit-campaign/'.$web_name.'/'.$post_id?>" class="btn btn-success btn-flat btn-sm pull-right mb-10"><i class="fa fa-pencil"></i> Edit</a>
					                    </div>
					                  </div>
					                </div>
					              </div>
					            </div>
							<?php
							
						}
					}

                  	?>
                    <!-- </div> -->
              </div>
          </div>
        </div>
      </div>
    </section>