<?php
$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}
?>

<section class="page-section light-bg no-pad">
	<div id="blog-content-item" class="container">
		<div class="contactus">
			<div class="row">
            	<div class="col-md-12">
                	<?php
						$login_error = $this->session->userdata('login_error');
						$this->session->unset_userdata('login_error');
						
						if(!empty($login_error))
						{
							echo '<div class="alert alert-danger">'.$login_error.'</div>';
						}
						
						$validation_error = validation_errors();
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($error))
						{
							echo '<div class="alert alert-danger">'.$error.'</div>';
							$this->session->unset_userdata('error_message');
						}
						
						if(!empty($validation_error))
						{
							echo '<div class="alert alert-danger">'.$validation_error.'</div>';
						}
						
						if(!empty($success))
						{
							echo '<div class="alert alert-success">'.$success.'</div>';
							$this->session->unset_userdata('success_message');
						}

					?>
                </div>
				<div class="col-sm-12 col-md-4 col-md-offset-4">
					<h2><span>Change</span> Password</h2>
					<div class="contact-list contact-form">
						<div class="form">
							<form action="<?php echo site_url().'customer/change-password'?>" method="POST">
								<input required="" placeholder="Current Password" value="" name="current_password" class="txt" type="password">
								<input required="" placeholder="New Password" value="" name="new_password" class="txt" type="password">
								<input required="" placeholder="Confirm Password" value="" name="confirm_new_password" class="txt" type="password">
								<input value="submit" name="submit" class="txt2" type="submit">
							</form>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>