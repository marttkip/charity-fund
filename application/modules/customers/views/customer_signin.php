<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-10">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h3 class="text-theme-colored font-36">MEMBER LOGIN</h3>
             
            </div>
          </div>
        </div>
      </div>
 </section>

    <!-- Section: event calendar -->
<section>
	<div class="container">
		    	<?php
					$login_error = $this->session->userdata('login_error');
					$this->session->unset_userdata('login_error');
					
					if(!empty($login_error))
					{
						echo '<div class="alert alert-danger">'.$login_error.'</div>';
					}
					
					$validation_error = validation_errors();
					$success = $this->session->userdata('success_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger">'.$error.'</div>';
						$this->session->unset_userdata('error_message');
					}
					
					if(!empty($validation_error))
					{
						echo '<div class="alert alert-danger">'.$validation_error.'</div>';
					}
					
					if(!empty($success))
					{
						echo '<div class="alert alert-success">'.$success.'</div>';
						$this->session->unset_userdata('success_message');
					}
					$countries = '';
					$country_rs = $this->customers_model->get_app_countries();
					foreach ($country_rs->result() as $key => $value) {
						# code...
						$id = $value->id;
						$country_name = $value->country_name;
						$countries .= '<option value="'.$id.'">'.$country_name.'</option>';
					}
				?>
		        <div class="row">
		          <div class="col-md-4 mb-40">
		            <h4 class="text-gray mt-0 pt-5"> Login</h4>
		            <hr>
		            <form action="<?php echo site_url().'login'?>" method="POST">
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_username_email">Member Email</label>
		                  <input id="form_username_email" name="customer_email" class="form-control required" type="email">
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_password">Password</label>
		                  <input id="form_password" name="customer_password" class="form-control required" type="password">
		                </div>
		              </div>
		              <div class="checkbox pull-left mt-15">
		                <a class="text-theme-colored font-weight-600 font-12" href="<?php echo site_url().'reset-member-password'?>"> Forgot Your Password?</a>
		              </div>
		              <div class="form-group pull-right mt-10">
		                <button type="submit" class="btn btn-block btn-dark" data-loading-text="Please wait...">Login</button>
		              </div>
		            </form>
		          </div>
		          <div class="col-md-7 col-md-offset-1">
		             <form action="<?php echo site_url().'register-user'?>" method="POST">
		                <h4 class="text-gray mt-0 pt-5">Don't have an Account? Register Now.</h4>
		              <hr>
		              <div class="row">
		               <div class="form-group col-md-12">
		                  <label for="form_name">Name</label>
		                  <input id="form_name" name="customer_name" class="form-control" type="text">
		                </div>
		                
		              </div>
		              <div class="row">
		                
		                <div class="form-group col-md-6">
		                  <label>Phone</label>
		                  <input id="form_email" name="customer_phone" class="form-control" type="text" >
		                </div>
		                <div class="form-group col-md-6">
		                  <label>Email Address</label>
		                  <input id="form_email" name="customer_email" class="form-control" type="email">
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-6">
		                  <label for="form_choose_username">Gender</label>
		                  <select id="form_sex" name="gender_id" class="form-control required">
		                      <option value="1">Male</option>
		                      <option value="2">Female</option>
		                    </select>
		                </div>
		                <div class="form-group col-md-6">
		                  <label for="form_choose_username">Country</label>
		                  	<select id="form_sex" name="country_id" class="form-control required">
		                      <option value="1">SELECT A COUNTRY</option>
		                      <?php echo $countries;?>
		                    </select>
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-6">
		                  <label for="form_choose_password">Choose Password</label>
		                  <input id="form_choose_password" name="password" class="form-control" type="password">
		                </div>
		                <div class="form-group col-md-6">
		                  <label>Re-enter Password</label>
		                  <input id="form_re_enter_password" name="confirm_password" class="form-control" type="password">
		                </div>
		              </div>
		             
		              <div class="form-group">
		                <button class="btn btn-dark btn-lg btn-block mt-15" type="submit">Register Now</button>
		              </div>
		            </form>
		          </div>
		        </div>
		    </div>
			
</section>