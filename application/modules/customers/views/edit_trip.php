<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-10 col-md-offset-2 text-center">
              <h3 class="text-theme-colored font-36"><?php echo $title?></h3>
             
            </div>
          </div>
        </div>
      </div>
 </section>

 <?php
if($trip_details->num_rows() > 0)
{
	foreach ($trip_details->result() as $key => $value) {
		# code...
		$applicant_name = $value->applicant_name;
		$conservancy_id = $value->conservancy_id;
		$trip_id = $value->trip_id;
		$contact_person_name = $value->contact_person_name;
		$contact_person_phone = $value->contact_person_phone;
		$contact_person_email = $value->contact_person_email;
		$description = $value->description;
		$trip_start_date = $value->trip_start_date;
		$trip_end_date = $value->trip_end_date;
		$trip_persons = $value->trip_persons;
		$description = $value->description;

	}
}
else
{
	$description = set_value('description');
	$applicant_id = set_value('applicant_id');
	$contact_person_name = set_value('contact_person_name');
	$contact_person_phone = set_value('contact_person_phone');
	$contact_person_email = set_value('contact_person_email');
	$trip_start_date = set_value('trip_start_date');
	$trip_end_date = set_value('trip_end_date');
	$trip_id = '';
	$trip_persons = set_value('trip_persons');
}

 ?>

    <!-- Section: event calendar -->
<section>
	<div class="container">
		    	<?php
					$login_error = $this->session->userdata('login_error');
					$this->session->unset_userdata('login_error');
					
					if(!empty($login_error))
					{
						echo '<div class="alert alert-danger">'.$login_error.'</div>';
					}
					
					$validation_error = validation_errors();
					$success = $this->session->userdata('success_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger">'.$error.'</div>';
						$this->session->unset_userdata('error_message');
					}
					
					if(!empty($validation_error))
					{
						echo '<div class="alert alert-danger">'.$validation_error.'</div>';
					}
					
					if(!empty($success))
					{
						echo '<div class="alert alert-success">'.$success.'</div>';
						$this->session->unset_userdata('success_message');
					}
					$conservancies = '';
					$country_rs = $this->customers_model->get_conservancies();
					foreach ($country_rs->result() as $key => $value) {
						# code...
						$applicant_id_db = $value->applicant_id;
						$applicant_name = $value->applicant_name;
						// var_dump($applicant_id); die();
						if($conservancy_id == $applicant_id_db)
						{

							$conservancies .= '<option value="'.$applicant_id_db.'" selected>'.$applicant_name.'</option>';
						}
						else
						{

						$conservancies .= '<option value="'.$applicant_id_db.'">'.$applicant_name.'</option>';
						}
					}
				?>
		        <div class="row">
		        	<a href="<?php echo site_url().'member/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to Dashboard</a>
		          <div class="col-md-8 col-md-offset-2">
		             <form action="<?php echo site_url().'edit-member-trip/'.$trip_id?>" method="POST">
		              <div class="row">
		                
		                <div class="form-group col-md-6">
				              <div class="row">
				               <div class="form-group col-md-12">
				                  <label for="form_name">Conservancy</label>
				                  <select name="conservancy_id" class="form-control">
				                  	<option>--- SELECT A CONSERVACY ---- </option>
				                  	 <?php echo $conservancies?>
				                  </select>
				                </div>
				                
				              </div>
				            </div>
				            <div class="form-group col-md-6">
				            	<div class="row">
					               <div class="form-group col-md-12">
					                  <label for="form_name">Participants</label>
					                  <input id="form_name" name="trip_persons" class="form-control" type="text" value="<?php echo $trip_persons;?>">
					                </div>					                
					            </div>
				            </div>
				        </div>

				        <div class="row">		                
			                <div class="form-group col-md-6">
			                  <label>Start Date</label>
			                  	<div class="form-group">
					                <div class='input-group date'  >
					                    <input type='text' name="trip_start_date" class="form-control" id='datetimepicker1'  value="<?php echo $trip_start_date;?>" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
			                </div>
			                <div class="form-group col-md-6">
			                  <label>End Date</label>
			                  	<div class="form-group">
					                <div class='input-group date'  >
					                    <input type='text' name="trip_end_date" class="form-control" id='datetimepicker2' value="<?php echo $trip_end_date;?>" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
			                </div>
			              </div>


		             
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_choose_username">Contact Person</label>
		                	 <input id="form_email" name="contact_person_name" class="form-control" type="text" value="<?php echo $contact_person_name;?>" >
		                </div>
		                
		              </div>
		              
		              <div class="row">
		                
		                <div class="form-group col-md-6">
		                  <label>Phone</label>
		                  <input id="form_email" name="contact_person_phone" class="form-control" type="text"  value="<?php echo $contact_person_phone;?>">
		                </div>
		                <div class="form-group col-md-6">
		                  <label>Email Address</label>
		                  <input id="form_email" name="contact_person_email" class="form-control" type="email" value="<?php echo $contact_person_email;?>">
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_choose_password">More information</label>
		                  <textarea name="description"  class="form-control"><?php echo $description;?></textarea>
		                </div>
		              </div>
		             
		              <div class="form-group">
		                <button class="btn btn-dark btn-lg btn-block mt-15" type="submit">Edit Trip Details</button>
		              </div>
		            </form>
		          </div>
		        </div>
		    </div>
			
</section>

<script type="text/javascript">
    $(function () {
        // $('#datetimepicker1').datetimepicker();

        $('#datetimepicker2').datetimepicker({
                    format: "YYYY-MM-DD",
                });
         $('#datetimepicker1').datetimepicker({
                    format: "YYYY-MM-DD",
                });
    });
</script>