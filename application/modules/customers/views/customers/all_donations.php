 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
          <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		// $result = '<a href="'.site_url().'administration/reports/export_cash_report" class="btn btn-sm btn-success pull-right">Export</a>';
		$result = '';
		if(!empty($search))
		{
			echo '<a href="'.site_url().'administration/reports/close_cash_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Date</th>
						  <th>Member</th>
						  <th>Member Type</th>
						  <th>Transaction Method</th>
						  <th>Transaction ID</th>
						  <th>Transaction Amount</th>
						  <th>Cause</th>
						</tr>
					  </thead>
					  <tbody>
			';
			foreach ($query->result() as $row)
			{
				$count++;
				$total_invoiced = 0;
				$payment_created = date('jS M Y',strtotime($row->created));
				$post_id = $row->post_id;
				$customer_id = $row->customer_id;
				$member_id = $row->member_id;
				$post_title = $row->post_title;
				$donated_amount = $row->donated_amount;
				$transaction_tracking_id = $row->transaction_tracking_id;

				$name = '';
				$customer_type_name = 'Donor';
				if($customer_id > 0)
				{
					$this->db->where('customer.customer_id = '.$customer_id.' AND customer.customer_type_id = customer_type.customer_type_id');
					$query = $this->db->get('customer,customer_type');
					if($query->num_rows() > 0)
					{
						foreach ($query->result() as $key => $value) {
							# code...
							$customer_first_name = $value->customer_first_name;
							$customer_surname = $value->customer_surname;
						
						}

						$name = $customer_first_name.' '.$customer_surname;
					}
				}

				if($member_id > 0)
				{
					$this->db->where('applicant_id',$member_id);
					$query = $this->db->get('applications');
					if($query->num_rows() > 0)
					{
						foreach ($query->result() as $key => $value) {
							# code...
							$applicant_name = $value->applicant_name;
							$applicant_category = $value->applicant_category;

							if($applicant_category == 1)
							{
								$customer_type_name = 'Conservancy';
							}
							else if($applicant_category == 2)
							{
								$customer_type_name = 'School';
							}
							else if($applicant_category == 3)
							{
								$customer_type_name = 'Clubs';
							}
							else if($applicant_category == 4)
							{
								$customer_type_name = 'Corporates';
							}
						}

						$name = $applicant_name;
					}
				}
				
				$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$payment_created.'</td>
								<td>'.$name.'</td>
								<td>'.$customer_type_name.'</td>
								<td>Pesapal</td>
								<td>'.$transaction_tracking_id.'</td>
								<td>'.number_format($donated_amount, 2).'</td>
								<td>'.$post_title.'</td>
							</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no payments";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>