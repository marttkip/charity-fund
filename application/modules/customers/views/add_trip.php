<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-8 col-md-offset-2 text-center">
              <h3 class="text-theme-colored font-36">TRIP CREATION</h3>
             
            </div>
          </div>
        </div>
      </div>
 </section>

    <!-- Section: event calendar -->
<section>
	<div class="container">
		    	<?php
					$login_error = $this->session->userdata('login_error');
					$this->session->unset_userdata('login_error');
					
					if(!empty($login_error))
					{
						echo '<div class="alert alert-danger">'.$login_error.'</div>';
					}
					
					$validation_error = validation_errors();
					$success = $this->session->userdata('success_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger">'.$error.'</div>';
						$this->session->unset_userdata('error_message');
					}
					
					if(!empty($validation_error))
					{
						echo '<div class="alert alert-danger">'.$validation_error.'</div>';
					}
					
					if(!empty($success))
					{
						echo '<div class="alert alert-success">'.$success.'</div>';
						$this->session->unset_userdata('success_message');
					}
					$conservancies = '';
					$country_rs = $this->customers_model->get_conservancies();
					foreach ($country_rs->result() as $key => $value) {
						# code...
						$applicant_id = $value->applicant_id;
						$applicant_name = $value->applicant_name;
						$conservancies .= '<option value="'.$applicant_id.'">'.$applicant_name.'</option>';
					}
				?>
		        <div class="row">
		        	<a href="<?php echo site_url().'member/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to Dashboard</a>
		          <div class="col-md-8 col-md-offset-2">
		             <form action="<?php echo site_url().'add-member-trip'?>" method="POST">
		              <div class="row">
		                
		                <div class="form-group col-md-6">
				              <div class="row">
				               <div class="form-group col-md-12">
				                  <label for="form_name">Conservancy</label>
				                  <select name="conservancy_id" class="form-control">
				                  	<option>--- SELECT A CONSERVACY ---- </option>
				                  	 <?php echo $conservancies?>
				                  </select>
				                </div>
				                
				              </div>
				            </div>
				            <div class="form-group col-md-6">
				            	<div class="row">
					               <div class="form-group col-md-12">
					                  <label for="form_name">Participants</label>
					                  <input id="form_name" name="trip_persons" class="form-control" type="text">
					                </div>					                
					            </div>
				            </div>
				        </div>

				        <div class="row">		                
			                <div class="form-group col-md-6">
			                  <label>Start Date</label>
			                  	<div class="form-group">
					                <div class='input-group date'  >
					                    <input type='text' name="trip_start_date" class="form-control" id='datetimepicker1' />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
			                </div>
			                <div class="form-group col-md-6">
			                  <label>End Date</label>
			                  	<div class="form-group">
					                <div class='input-group date'  >
					                    <input type='text' name="trip_end_date" class="form-control" id='datetimepicker2' />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
			                </div>
			              </div>


		             
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_choose_username">Contact Person</label>
		                	 <input id="form_email" name="contact_person_name" class="form-control" type="text" >
		                </div>
		                
		              </div>
		              
		              <div class="row">
		                
		                <div class="form-group col-md-6">
		                  <label>Phone</label>
		                  <input id="form_email" name="contact_person_phone" class="form-control" type="text" >
		                </div>
		                <div class="form-group col-md-6">
		                  <label>Email Address</label>
		                  <input id="form_email" name="contact_person_email" class="form-control" type="email">
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_choose_password">More information</label>
		                  <textarea name="description"  class="form-control"></textarea>
		                </div>
		              </div>
		              <div class="g-recaptcha" data-sitekey="6LePDjoUAAAAABGuAN19LvmYE31y4NJYaLV0F9Z2"></div>
		             
		              <div class="form-group">
		                <button class="btn btn-dark btn-lg btn-block mt-15" type="submit">Register Now</button>
		              </div>
		            </form>
		          </div>
		        </div>
		    </div>
			
</section>

<script type="text/javascript">
    $(function () {
        // $('#datetimepicker1').datetimepicker();

        $('#datetimepicker2').datetimepicker({
                    format: "YYYY-MM-DD",
                });
         $('#datetimepicker1').datetimepicker({
                    format: "YYYY-MM-DD",
                });
    });
</script>