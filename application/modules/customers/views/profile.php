<?php
$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}

$customer_id = $this->session->userdata('customer_id');
$query = $this->customers_model->get_member_details($customer_id);

$profile_branch = 'http://placehold.it/360x360';
if($query->num_rows() > 0)
{
  foreach ($query->result() as $key => $value) {
    # code...
    $customer_id = $value->customer_id;
    $customer_email = $value->customer_email;
    $customer_phone = $value->customer_phone;
    $customer_first_name = $value->customer_first_name;
    $customer_surname = $value->customer_surname;

    $profile_image = $value->profile_image;

    if(!empty($profile_image))
    {
      $profile_branch = base_url().'assets/customers/'.$value->profile_image;
    }
    else
    {
      $profile_branch = 'http://placehold.it/360x360';
    }
  }
}
?>

<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_about?>">
      <div class="container pt-0 pb-10">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">DASHBOARD</h2>
             
            </div>
          </div>
        </div>
      </div>      
    </section>
     <section>
       <div class="container">
          <div class="section-content">
            <div class="row">
              <div class="col-md-12">
                

                <?php

                  $validation_error = validation_errors();
                  $success = $this->session->userdata('success_message');
                  
                  if(!empty($error))
                  {
                    echo '<div class="alert alert-danger">'.$error.'</div>';
                    $this->session->unset_userdata('error_message');
                  }
                  
                  if(!empty($validation_error))
                  {
                    echo '<div class="alert alert-danger">'.$validation_error.'</div>';
                  }
                  
                  if(!empty($success))
                  {
                    echo '<div class="alert alert-success">'.$success.'</div>';
                    $this->session->unset_userdata('success_message');
                  }
                ?>      
              </div>
            </div>
             <div class="row">
                <div class="col-md-2">
                  <form action="<?php echo site_url().'update-member-image';?>" method="post" role="form" enctype="multipart/form-data">
                   <div class="thumb">
                      <!-- <img src="http://placehold.it/360x360" alt=""> -->
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" >
                                <img src="<?php echo $profile_branch;?>" style="width:360px;" >
                            </div>
                              <input type="hidden" name="redirect-url" value="<?php echo $this->uri->uri_string()?>">
                                <span class="btn btn-file btn_pink"><span class="fileinput-new">Select Image</span><span class="fileinput-exists"></span><input type="file" name="attachment"></span>
                            <!-- </div> -->
                        </div>
                   </div>
                   <button data-loading-text="Please wait..." class="btn btn-flat btn-dark btn-theme-colored mt-5" type="submit">Update Profile Image</button>
                  </form>
                </div>
                <div class="col-md-9">
                   <ul class=" nav nav-tabs " role="tablist">
                      <li class="nav-item">
                         <a class="nav-link active" href="#fast" role="tab" data-toggle="tab">
                            <h4 class="line-bottom text-uppercase mt-0">Profile</h4>
                         </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#drinks" role="tab" data-toggle="tab">
                            <h4 class="line-bottom text-uppercase mt-0">Contributions</h4>
                         </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#rice" role="tab" data-toggle="tab">
                            <h4 class="line-bottom text-uppercase mt-0">Causes</h4>
                         </a>
                      </li>
                   </ul>
                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="fast">
                          <div class="row">
                           <div class="col-md-6">
                            <h4 class="line-bottom">About <?php echo strtoupper($customer_first_name.' '.$customer_surname)?></h4>

                             <form action="<?php echo site_url().'update-profile/'.$customer_id;?>" method="post" role="form">
                                    
                                <div class="form-group">
                                    <input type="text" name="customer_phone" id="phone" class="form-control input-lg" placeholder="Phone Number" tabindex="1" value="<?php echo $customer_phone?>">
                                </div>
                                <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                                <div class="form-group">
                                    <input type="text" name="customer_email" id="phone" class="form-control input-lg" placeholder="Phone Email" tabindex="2" value="<?php echo $customer_email?>">
                                </div>
                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6"><input type="submit" value="Update Detail" class="btn btn-primary btn-block btn-sm" tabindex="7"></div>
                                </div>
                            </form>
                            <div class="volunteer-address">
                              <ul>
                               
                                <li>
                                  <div class="bg-light media border-bottom p-15">
                                    <div class="media-left">
                                      <i class="fa fa-money text-theme-colored font-24 mt-5"></i>
                                    </div>
                                    <div class="media-body">
                                      <h5 class="mt-0 mb-0">MY CONTRIBUTION:</h5>
                                       <?php
                                       $customer_id = $this->session->userdata('customer_id');
                                       $contribution = $this->site_model->calculate_credit_total($customer_id);
                                       ?>
                                       <p><strong>KES. <?php echo number_format($contribution,2) ;?></strong> </p>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                              
                             
                           </div>
                            <div class="col-md-6">
                                 <h4 class="line-bottom">Change Password</h4>
                                <form action="<?php echo site_url().'change-member-password';?>" method="post" role="form">
                                    
                                    <div class="form-group">
                                        <input type="password" name="current_password" id="email" class="form-control input-lg" placeholder="Current Password" tabindex="4" value="">
                                    </div>
                                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                                    <div class="form-group">
                                        <input type="password" name="new_password" id="email" class="form-control input-lg" placeholder="New Password" tabindex="4" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirm_password" id="email" class="form-control input-lg" placeholder="Confirm Password" tabindex="4" value="">
                                    </div>
                                    
                                    <hr class="colorgraph">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6"><input type="submit" value="Reset Password" class="btn btn-primary btn-block btn-sm" tabindex="7"></div>
                                    </div>
                                </form>
                           </div>
                          </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="drinks">
                         <?php
                          $v_data['customer_id'] = $this->session->userdata('customer_id');
                          echo $this->load->view('member/transactions_view', $v_data)
                        ?>
                      </div>
                      
                      <div role="tabpanel" class="tab-pane fade" id="rice">
                          <?php
                            $v_data['customer_id'] = $this->session->userdata('customer_id');
                            echo $this->load->view('member/causes_view', $v_data)
                          ?>
                      </div>
                   </div>
                </div>
                <div class="col-md-1">
                  <a href="<?php echo site_url().'signout-customer'?>"  class="btn btn-sm btn-danger" onclick="return confirm('\ Do you want to sign out ? \')">  LOGOUT</a>  
                </div>
                
             </div>
          </div>
       </div>
    </section>
