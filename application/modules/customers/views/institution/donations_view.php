<?php
$applicant_login_status = $this->session->userdata('applicant_login_status');
$customer_login_status = $this->session->userdata('customer_login_status');

// var_dump($applicant_login_status); die();
if(!$customer_login_status AND !$applicant_login_status)
{
	$this->session->set_userdata('error_message', 'Please log in to continue');
	redirect('login');
}


$customer_id = $this->session->userdata('customer_id');
$applicant_id = $this->session->userdata('applicant_id');
if(!empty($customer_id))
{
	$add = ' AND customer_donations.customer_id = '.$applicant_id;
	$location = 'my-donations';
}
else if(!empty($applicant_id))
{
    $add = ' AND customer_donations.member_id = '.$applicant_id;
    $location = 'member-contributions';
}
$where = 'post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 '.$add;
$table = 'post, customer_donations';	

//pagination
$segment = 2;
$this->load->library('pagination');
$config['base_url'] = site_url().$location;
$config['total_rows'] = $this->users_model->count_items($table, $where);
$config['uri_segment'] = $segment;
$config['per_page'] = 20;
$config['num_links'] = 5;

$config['full_tag_open'] = '<ul class="pagination pull-right">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';

$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

$config['next_tag_open'] = '<li>';
$config['next_link'] = 'Next';
$config['next_tag_close'] = '</span>';

$config['prev_tag_open'] = '<li>';
$config['prev_link'] = 'Prev';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$this->pagination->initialize($config);

$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
$links = $this->pagination->create_links();
$query = $this->customers_model->get_all_contributions($table, $where, $config["per_page"], $page, $order='customer_donations.customer_donation_id', $order_method='ASC');






?>

<table class="table table-striped table-bordered tbl-shopping-cart">
	<thead>
		<tr>
		  <th>#</th>
		  <th>Project</th>
		  <th>Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php
		// var_dump()

		$counted = 0;

	  	$total_amount = 0;
	  	if($query->num_rows()>0)
		{

			foreach($query->result() as $row)
			{
				$post_id = $row->post_id;
				// $blog_category_name = $row->blog_category_name;
				$blog_category_id = $row->blog_category_id;
				$post_image = $row->post_image;
				$post_content = $row->post_content;
				$post_title = $row->post_title;
				$post_comments = $row->post_comments;
				$post_status = $row->post_status;
				$post_views = $row->post_views;
				$image = base_url().'assets/images/posts/'.$row->post_image;
				$web_name = $this->site_model->create_web_name($post_title);
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$total_comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 100));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M Y',strtotime($created));
				$created_on = date('jS M Y H:i a',strtotime($row->created));
				
				$categories = '';
				$post_target = $row->post_target;
				$donated_amount = $row->donated_amount;
				
				$total_amount = $total_amount + $donated_amount;
				
				$counted++;
				echo "	<tr>
	                      <td >".$counted."</td>
	                      <td>".$post_title."</td>
	                      <td><span class='amount'>Kes. ".number_format($donated_amount,2)."</span></td>
	                    </tr>";
			}
		}
		?>
		<tr>
          <td>Total Contribution</td>
          <td>&nbsp;</td>
          <td><span class="amount">Kes. <?php echo number_format($total_amount,2)?></td>
        </tr>

	</tbody>
</table>