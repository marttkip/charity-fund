<table class="table table-striped table-bordered tbl-shopping-cart">
	<thead>
		<tr>
		  <th>#</th>
		  <th>Project</th>
		  <th>Date</th>
		  <th>Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php
		// var_dump()

		// $count_visit = $visit_list->num_rows();
		$num_pages = $total_rows/$per_page;

		if($num_pages < 1)
		{
			$num_pages = 0;
		}
		$num_pages = round($num_pages);

		if($page==0)
		{
			$counted = 0;
		}
		else if($page > 0)
		{
			$counted = $per_page*$page;
		}

	  	$total_amount = 0;
	  	if($query->num_rows()>0)
		{

			foreach($query->result() as $row)
			{
				$post_id = $row->post_id;
				// $blog_category_name = $row->blog_category_name;
				$blog_category_id = $row->blog_category_id;
				$post_image = $row->post_image;
				$post_content = $row->post_content;
				$post_title = $row->post_title;
				$post_comments = $row->post_comments;
				$post_status = $row->post_status;
				$post_views = $row->post_views;
				$image = base_url().'assets/images/posts/'.$row->post_image;
				$web_name = $this->site_model->create_web_name($post_title);
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$total_comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 100));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M Y',strtotime($created));
				$created_on = date('jS M Y H:i a',strtotime($row->created));
				
				$categories = '';
				$count++;
				$post_target = $row->post_target;
				$donated_amount = $row->donated_amount;
				
				$total_amount = $total_amount + $donated_amount;
				
				$counted++;
				echo "	<tr>
	                      <td >".$counted."</td>
	                      <td>".$post_title."</td>
	                      <td><span class='amount'>Kes. ".number_format($donated_amount,2)."</span></td>
	                    </tr>";
			}
		}
		?>
		<tr>
          <td>Total Contribution</td>
          <td>&nbsp;</td>
          <td><span class="amount">Kes. <?php echo number_format($total_amount,2)?></td>
        </tr>

	</tbody>
</table>
<div class="row">
	<div class="col-md-12" style="padding-right: 25px;">
		<div class="pull-right">
			<?php
				$link ='<ul style="list-style:none;">';
				// echo $page;
				if($num_pages > $page)
				{
					// echo "now ".$num_pages." ".$page;
					$last_page = $num_pages -1;

					if($page > 0 AND $page < $last_page)
					{
						// echo $page;
						$page++;
						// echo "now".$page;
						$previous = $page -2;
						$link .='<li onclick="get_next_page('.$previous.','.$applicant_id.')" class="pull-left" style="margin-right:20px;" > <i class="fa fa-angle-left"></i> Back</li>  <li onclick="get_next_page('.$page.','.$applicant_id.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}else if($page == $last_page)
					{
						$page++;

						$previous = $page -2;
						// echo "equal".$num_pages." ".$page;
						$link .='<li onclick="get_next_page('.$previous.','.$applicant_id.')" class="pull-left"> <i class="fa fa-angle-left"></i> Back</li>';
					}
					else
					{
						$page++;
						$link .='<li onclick="get_next_page('.$page.','.$applicant_id.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}
					// var_dump($link); die();
				}
				$link .='</ul>';
				echo $link;
				
			?>
		</div>
	</div>
</div>
