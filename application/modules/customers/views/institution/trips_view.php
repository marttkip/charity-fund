<?php
$applicant_login_status = $this->session->userdata('applicant_login_status');
if(!$applicant_login_status)
{
	$this->session->set_userdata('error_message', 'Please log in to continue');
	redirect('conservation-registration');
}

$applicant_id = $this->session->userdata('applicant_id');
$where = 'trip.applicant_id = '.$applicant_id.' AND trip.conservancy_id = applications.applicant_id';
$table = 'trip,applications';	

//pagination
$segment = 2;
$this->load->library('pagination');
$config['base_url'] = site_url().'campaigns';
$config['total_rows'] = $this->users_model->count_items($table, $where);
$config['uri_segment'] = $segment;
$config['per_page'] = 20;
$config['num_links'] = 5;

$config['full_tag_open'] = '<ul class="pagination pull-right">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';

$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

$config['next_tag_open'] = '<li>';
$config['next_link'] = 'Next';
$config['next_tag_close'] = '</span>';

$config['prev_tag_open'] = '<li>';
$config['prev_link'] = 'Prev';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$this->pagination->initialize($config);

$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
$links = $this->pagination->create_links();
$query = $this->customers_model->get_all_causes($table, $where, $config["per_page"], $page, $order='trip.created', $order_method='ASC');

?>
<div class="row">
	<div class="col-md-12">
 		<a href="<?php echo site_url().'add-member-trip'?>" class="btn btn-success btn-flat btn-sm pull-right" data-loading-text="Please wait..." style="margin-right:5px;"><i class="fa fa-plus"></i> Add Trip</a>
 	</div>
</div>
<br>
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered tbl-shopping-cart">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Conservancy</th>
		      <th>Trip Start date</th>
		      <th>Trip End date</th>
		      <th>Trip Participants</th>
		      <th>Contact Person</th>
		      <th>Status</th>
		      <th>Action</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php
		  	$count = 0;
		  	$total_amount = 0;
		  	if($query->num_rows()>0)
			{
				foreach($query->result() as $row)
				{
					$trip_id = $row->trip_id;
					$applicant_name = $row->applicant_name;
					$trip_persons = $row->trip_persons;
					$trip_start_date = $row->trip_start_date;
					$trip_end_date = $row->trip_end_date;
					$approved = $row->approved;
					$contact_person_name = $row->contact_person_name;
					$description = $row->description;
					$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 100));
					$created = $row->created;
					$day = date('j',strtotime($created));
					$month = date('M Y',strtotime($created));
					$trip_start_date = date('jS M Y H:i a',strtotime($row->trip_start_date));
					$trip_end_date = date('jS M Y H:i a',strtotime($row->trip_end_date));
					$categories = '';

					if($approved == 0)
					{
					  $approved_status = 'Processing request';
					}
					else if($approved == 1)
					{
					  $approved_status = 'Approved Trip'; 
					}
					else
					{
					  $approved_status = 'Trip Cancelled ';
					}
					$count++;
					?>
		            <tr>
						<td ><?php echo $count?></td>
						<td><?php echo $applicant_name;?></td>
		                <td><?php echo $trip_start_date;?></td>
		                <td><?php echo $trip_end_date;?></td>
		                <td><?php echo $trip_persons;?></td>
		                <td><?php echo $contact_person_name;?></td>
		                <td><?php echo $approved_status;?></td>
		                <td><a href="<?php echo site_url().'edit-member-trip/'.$trip_id.''?>" class="btn btn-success btn-flat btn-sm pull-right" data-loading-text="Please wait..." style="margin-right:5px;"><i class="fa fa-pencil"></i> Edit trip</a></td>
		            </tr>
		        <?php

		    	}
		        }
		    ?>
		   
		   
		  </tbody>
		</table>
	</div>
</div>