<?php
$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}
?>
<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_about?>">
      <div class="container pt-0 pb-10">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-8 col-md-offset-2 text-center">
              <h3 class="text-theme-colored font-36">INSTITUTION REGISTRATION</h3>
             
            </div>
          </div>
        </div>
      </div>
 </section>

    <!-- Section: event calendar -->
<section>
	<div class="container">
		    	<?php
					$login_error = $this->session->userdata('login_error');
					$this->session->unset_userdata('login_error');
					
					if(!empty($login_error))
					{
						echo '<div class="alert alert-danger">'.$login_error.'</div>';
					}
					
					$validation_error = validation_errors();
					$success = $this->session->userdata('success_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger">'.$error.'</div>';
						$this->session->unset_userdata('error_message');
					}
					
					if(!empty($validation_error))
					{
						echo '<div class="alert alert-danger">'.$validation_error.'</div>';
					}
					
					if(!empty($success))
					{
						echo '<div class="alert alert-success">'.$success.'</div>';
						$this->session->unset_userdata('success_message');
					}
					$countries = '';
					$country_rs = $this->customers_model->get_app_countries();
					foreach ($country_rs->result() as $key => $value) {
						# code...
						$id = $value->id;
						$country_name = $value->country_name;
						$countries .= '<option value="'.$id.'">'.$country_name.'</option>';
					}
				?>
		        <div class="row">
		          <div class="col-md-4 mb-40">
		            <h4 class="text-gray mt-0 pt-5"> Login</h4>
		            <hr>		              
					<div class="row">
						<form action="<?php echo site_url().'login-conservancy'?>" method="POST">
			              <div class="row">
			                <div class="form-group col-md-12">
			                  <label for="form_username_email">Username</label>
			                  <input id="form_username_email" name="customer_email" class="form-control required" type="text">
			                </div>
			                <input type="hidden" name="applicant_category" value="2">
			              </div>
			              <div class="row">
			                <div class="form-group col-md-12">
			                  <label for="form_password">Password</label>
			                  <input id="form_password" name="customer_password" class="form-control required" type="password">
			                </div>
			              </div>
			              <div class="checkbox pull-left mt-15">
			                <a class="text-theme-colored font-weight-600 font-12" href="<?php echo site_url().'reset-password'?>"> Forgot Your Password?</a>
			              </div>
			              <div class="form-group pull-right mt-10">
			                <button type="submit" class="btn btn-block btn-dark" data-loading-text="Please wait...">Login</button>
			              </div>
			            </form>
					</div>
		            
		          </div>
		          <div class="col-md-7 col-md-offset-1">
		             <form action="<?php echo site_url().'membership-registration'?>" method="POST">
		                <h4 class="text-gray mt-0 pt-5">Are you a School, Club or Corporate Institution. Register Now.</h4>
		              <hr>
		              <div class="row">

		               <div class="form-group col-md-12">

		               <label for="form_name">Account Type  * </label>

		                  <input type="radio" id="contactChoice1"  name="account_type" value="2">
						    <label for="contactChoice1">School</label>

						    <input type="radio" id="contactChoice2" name="account_type" value="3">
						    <label for="contactChoice2">Club</label>

						    <input type="radio" id="contactChoice3" name="account_type" value="4">
						    <label for="contactChoice3">Corporates</label>
		                </div>
		                
		              </div>
		              <div class="row">
		               <div class="form-group col-md-6">
		                  <label for="form_name">Name *</label>
		                  <input id="form_name" name="conservancy_name" class="form-control" type="text">
		                </div>
		                <div class="form-group col-md-6">
		                  <label for="form_name">Website</label>
		                  <input id="form_name" name="website" class="form-control" type="text">
		                </div>
		                
		              </div>
		              <div class="row">
		                
		                <div class="form-group col-md-6">
		                  <label>Phone *</label>
		                  <input id="form_email" name="conservancy_phone" class="form-control" type="text" >
		                </div>
		                <div class="form-group col-md-6">
		                  <label>Email Address *</label>
		                  <input id="form_email" name="conservancy_email" class="form-control" type="email">
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-6">
		                  <label for="form_choose_username">Contact Person *</label>
		                	 <input id="form_email" name="conservancy_contact_person" class="form-control" type="text" >
		                </div>
		                <div class="form-group col-md-6">
		                  <label for="form_name">Designation *</label>
		                  <input id="form_name" name="designation" class="form-control" type="text">
		                </div>
		                
		                
		              </div>
		              <div class="row">
		                
		                <div class="form-group col-md-6">
		                  <label>Contact Person Phone *</label>
		                  <input id="form_email" name="contact_person_phone" class="form-control" type="text" >
		                </div>
		                <div class="form-group col-md-6">
		                  <label>Contact Person Email Address *</label>
		                  <input id="form_email" name="contact_person_email" class="form-control" type="email">
		                </div>
		              </div>
		              <div class="row">
		                <div class="form-group col-md-12">
		                  <label for="form_choose_password">Description *</label>
		                  <textarea name="conservancy_descrption"  class="form-control"></textarea>
		                </div>
		              </div>
		             
		              <div class="form-group">
		                <button class="btn btn-dark btn-lg btn-block mt-15" type="submit">Register Now</button>
		              </div>
		            </form>
		          </div>
		        </div>
		    </div>
			
</section>