<?php
$where = 'post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 AND customer_donations.customer_donation_status = 1 AND customer_donations.customer_id = '.$customer_id;
$table = 'post, customer_donations';	

//pagination
$segment = 2;
$this->load->library('pagination');
$config['base_url'] = site_url().'donations';
$config['total_rows'] = $this->users_model->count_items($table, $where);
$config['uri_segment'] = $segment;
$config['per_page'] = 20;
$config['num_links'] = 5;

$config['full_tag_open'] = '<ul class="pagination pull-right">';
$config['full_tag_close'] = '</ul>';

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';

$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

$config['next_tag_open'] = '<li>';
$config['next_link'] = 'Next';
$config['next_tag_close'] = '</span>';

$config['prev_tag_open'] = '<li>';
$config['prev_link'] = 'Prev';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$this->pagination->initialize($config);

$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
$v_data["links"] = $this->pagination->create_links();
$query = $this->customers_model->get_all_contributions($table, $where, $config["per_page"], $page, $order='customer_donations.customer_donation_id', $order_method='ASC');
?>
<table class="table table-striped table-bordered tbl-shopping-cart">
  <thead>
	<tr>
	  <th>#</th>
	  <th>Date</th>
	  <th>Member</th>
	  <th>Member Type</th>
	  <th>Transaction Method</th>
	  <th>Transaction ID</th>
	  <th>Transaction Amount</th>
	  <th>Cause</th>
	</tr>
  </thead>
  <tbody>
<?php
$result = '';
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	
	foreach ($query->result() as $row)
	{
		$count++;
		$total_invoiced = 0;
		$payment_created = date('jS M Y',strtotime($row->created));
		$post_id = $row->post_id;
		$customer_id = $row->customer_id;
		$member_id = $row->member_id;
		$post_title = $row->post_title;
		$donated_amount = $row->donated_amount;
		$transaction_tracking_id = $row->transaction_tracking_id;

		$name = '';
		$customer_type_name = 'Donor';
		if($customer_id > 0)
		{
			$this->db->where('customer.customer_id = '.$customer_id.' AND customer.customer_type_id = customer_type.customer_type_id');
			$query = $this->db->get('customer,customer_type');
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$customer_first_name = $value->customer_first_name;
					$customer_surname = $value->customer_surname;
				
				}

				$name = $customer_first_name.' '.$customer_surname;
			}
		}

		
		
		$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$payment_created.'</td>
						<td>'.$name.'</td>
						<td>'.$customer_type_name.'</td>
						<td>Pesapal</td>
						<td>'.$transaction_tracking_id.'</td>
						<td>'.number_format($donated_amount, 2).'</td>
						<td>'.$post_title.'</td>
					</tr> 
			';
	}
	

}

echo $result;
?>
	</tbody>
</table>

