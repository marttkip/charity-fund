<?php
$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}
?>
<section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_about?>">
      <div class="container pt-0 pb-10">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h3 class="text-theme-colored font-36">FORGOT PASSWORD</h3>
             
            </div>
          </div>
        </div>
      </div>
 </section>

    <!-- Section: event calendar -->
<section>
	<div class="container">
		<?php
		    $validation_errors = validation_errors();
		    if(!empty($validation_errors))
		    {
		        echo '<div class="alert alert-danger">'.$validation_errors.'</div>';
		    }
		    
		    $login_error = $this->session->userdata('login_error');
		    if(!empty($login_error))
		    {
		        $this->session->unset_userdata('login_error');
		        echo '<div class="alert alert-danger">'.$login_error.'</div>';
		    }
			
		    $success_message = $this->session->userdata('success_message');
		    if(!empty($success_message))
		    {
		        $this->session->unset_userdata('success_message');
		        echo '<div class="alert alert-success">'.$success_message.'</div>';
		    }
		    
		    $error_message = $this->session->userdata('error_message');
		    if(!empty($error_message))
		    {
		        $this->session->unset_userdata('error_message');
		        echo '<div class="alert alert-danger">'.$error_message.'</div>';
		    }
		?>

		<form action="<?php echo site_url().$this->uri->uri_string();?>" method="post" role="form">
		    <h2>Forgot Password</h2>
		    <hr class="colorgraph">
		    
		    <div class="form-group">
		        <input type="email" name="applicant_email" id="email" class="form-control input-lg" placeholder="Conservancy Email" tabindex="4" value="">
		    </div>
		    
		    <hr class="colorgraph">
		    <div class="row">
		        <div class="col-xs-12 col-md-6"><a href="<?php echo site_url().'conservation-registration';?>" class="btn btn-default btn-block btn-lg">Login</a></div>
		        <div class="col-xs-12 col-md-6"><input type="submit" value="Reset Password" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
		    </div>
		</form>
	</div>
</section>