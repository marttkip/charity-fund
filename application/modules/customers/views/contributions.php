<!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-0 pb-0">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">CONTRIBUTIONS</h2>
              
            </div>
          </div>
        </div>
      </div>      
    </section>

    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
              <div class="col-md-12">
                <h3>Contributions <a href="<?php echo site_url().'conservancy/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to Dashboard</a></h3>
				<table class="table table-striped table-bordered tbl-shopping-cart">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Project</th>
                      <th>Donor</th>
                      <th>Contribution</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                  	$count = $page;
                  	$total_amount = 0;
                  	if($query->num_rows()>0)
					{

						foreach($query->result() as $row)
						{
							$post_id = $row->post_id;
							// $blog_category_name = $row->blog_category_name;
							$blog_category_id = $row->blog_category_id;
							$post_image = $row->post_image;
							$post_content = $row->post_content;
							$post_title = $row->post_title;
							$post_comments = $row->post_comments;
							$customer_first_name = $row->customer_first_name;
							$customer_surname = $row->customer_surname;
							$post_status = $row->post_status;
							$post_views = $row->post_views;
							$image = base_url().'assets/images/posts/'.$row->post_image;
							$web_name = $this->site_model->create_web_name($post_title);
							$created_by = $row->created_by;
							$modified_by = $row->modified_by;
							$total_comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
							$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
							$description = $row->post_content;
							$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 100));
							$created = $row->created;
							$day = date('j',strtotime($created));
							$month = date('M Y',strtotime($created));
							$created_on = date('jS M Y H:i a',strtotime($row->created));
							
							$categories = '';
							$count++;
							$post_target = $row->post_target;
							$donated_amount = $row->donated_amount;
							
							$total_amount = $total_amount + $donated_amount;
							?>
		                    <tr>
		                      <td ><?php echo $count?></td>
		                      <td><?php echo $customer_first_name.' '.$customer_surname;?></td>
		                      <td><?php echo $post_title;?></td>
		                      <td><span class="amount">Kes. <?php echo number_format($donated_amount,2)?></span></td>
		                    </tr>
		                <?php

		            }
		        }
		                ?>
                   
                    <tr>
                      <td>Total Contribution</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><span class="amount">Kes. <?php echo number_format($total_amount,2)?></td>
                    </tr>
                  </tbody>
                </table>
            </div>
           </div>
          </div>
         </div>
     </section>