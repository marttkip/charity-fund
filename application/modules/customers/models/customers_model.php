<?php

class Customers_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/
	public function all_customers()
	{
		$this->db->where('customer_status = 1');
		$query = $this->db->get('customer');
		
		return $query;
	}

	public function get_applicant_details($applicant_id)
	{
		$this->db->where('applicant_id = '.$applicant_id);
		$query = $this->db->get('applications');
		
		return $query;
	}
	public function get_member_details($customer_id)
	{
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('customer');
		
		return $query;
	}
	
	/*
	*	Retrieve all ride types
	*
	*/
	public function get_ride_types()
	{
		$this->db->order_by('ride_type_name');
		$query = $this->db->get('ride_type');
		
		return $query;
	}
	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_customers($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_contributions($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('project_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	

	public function get_all_transactions($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_causes($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}	
	public function get_all_customer_contacts($table, $where, $per_page, $page, $order = 'customer_contacts_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	

	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function add_company_contact($company_id)
	{
		$data = array(
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				'customer_id'=>$company_id
				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_conservancy_user($conservancy_id)
	{


		
		$this->db->where('applicant_id = '.$conservancy_id.' AND username IS NULL AND userpassword IS NULL');
		$query = $this->db->get('applications');

		// var_dump($query->num_rows()); die();
		if($query->num_rows() > 0)
		{
			

			$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);
			
			$data_update['userpassword'] = md5($new_password);
			$data_update['username'] = $this->input->post('username'.$conservancy_id);
			$data_update['contact_person'] = $this->input->post('name'.$conservancy_id);
			$data_update['applicant_status'] = 1;
			$this->db->where('applicant_id', $conservancy_id);
			if($this->db->update('applications', $data_update))
			{
				$row = $query->row();
				$contact_person = $this->input->post('name'.$conservancy_id);
				$username = $this->input->post('username'.$conservancy_id);
				$applicant_email = $row->applicant_email;
				//Send new password to email
				$contacts = $this->site_model->get_contacts();
				$email = $contacts['email'];
				$company_name = $contacts['company_name'];
				$data['contacts'] = $contacts;
				$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
				$data['text'] = '<strong>Hello '.$contact_person.'</strong> 
						<p>Your Account has been successfully activated. Your credentials to your account are as follows:</p> 
						<p><strong>Username </strong> :'.$username.'</p>
						<p><strong>Password </strong> :'.$new_password.'</p>
						<p>You can log into your account and sign in with this password here: 
						<a href="'.site_url().'conservation-registration" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Login</a>
						<p>If you have not requested a password change please contact us here immediately.</p>
						<a href="'.site_url().'contact" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Contact Us</a>'
						;
				
				$message['text'] = $this->load->view('site/compose_mail', $data, TRUE);
				$message['subject'] =  "Account Activation?";
				
				$sender['email'] = $email;
				$sender['name'] = $company_name;
				$receiver['email'] =$applicant_email;
				$receiver['name'] = $contact_person;
			
				$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
				return  TRUE;
			}else
			{
				return FALSE;
			}
			

		}
		else
		{
			$this->db->where('applicant_id = '.$conservancy_id);
			$query = $this->db->get('applications');
			$data_update['username'] = $this->input->post('username'.$conservancy_id);
			$data_update['applicant_status'] = 1;
			$data_update['contact_person'] = $this->input->post('name'.$conservancy_id);
			$this->db->where('applicant_id', $conservancy_id);

			if($this->db->update('applications', $data_update))
			{
				$row = $query->row();
				$contact_person = $this->input->post('contact_person'.$conservancy_id);
				$applicant_email = $row->applicant_email;
				$username = $this->input->post('username'.$conservancy_id);
				//Send new password to email
				$contacts = $this->site_model->get_contacts();
				$email = $contacts['email'];
				$company_name = $contacts['company_name'];
				$data['contacts'] = $contacts;
				$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
				$data['text'] = '<strong>Hello '.$contact_person.'</strong> 
						<p>Your Account has edited.:</p> 
						<p><strong>Username </strong> :'.$username.'</p>
						<p>You can log into your account and sign in with this password here: 
						<a href="'.site_url().'conservation-registration" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Login</a>
						<p>If you have not requested a password change please contact us here immediately.</p>
						<a href="'.site_url().'contact" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Contact Us</a>'
						;
				
				$message['text'] = $this->load->view('site/compose_mail', $data, TRUE);
				$message['subject'] =  "Account edit?";
				
				$sender['email'] = $email;
				$sender['name'] = $company_name;
				$receiver['email'] = $applicant_email;
				$receiver['name'] = $contact_person;
			
				$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		

	}
	function get_payment_methods()
	{
		$table = "payment_method";
		$where = "payment_method_id > 0";
		$items = "*";
		$order = "payment_method";
		
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	

	function create_cat_number()
	{
		//select product code
		$preffix = "JOP-RN_";
		$this->db->from('utu_payments');
		$this->db->where("reciept_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(reciept_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function create_invoice_number()
	{
		//select product code
		$preffix = "NGE-INV-";
		$this->db->from('invoice');
		$this->db->where("invoive_number LIKE '".$preffix."%' AND invoice_status = 1");
		$this->db->select('MAX(invoive_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function get_invoices($customer_id)
	{
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('invoice');
		
		return $query;
	}
	public function get_cust_rides($invoice_id,$customer_id)
	{
		$this->db->where('customer_id = '.$customer_id.' AND invoice_id = '.$invoice_id);
		$query = $this->db->get('ride');
		
		return $query;
	}

	public function cat_payment($customer_id)
	{
		$amount = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}
		$receipt_number = $this->customers_model->create_cat_number();

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount'=>$amount,
			'customer_id'=>$customer_id,
			'transaction_code'=>$transaction_code,
			'reciept_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_status'=>1,
			'status'=>1
			
		);
		if ($this->db->insert('utu_payments',$data)){


				
				return TRUE;
			}
			else{
				return FALSE;
			}
	}

	public function add_invoice($customer_id)
	{
		$invoice_number = $this->customers_model->create_invoice_number();
		$data = array(
				'invoice_date'=>$this->input->post('invoice_date'),
				'customer_id'=>$customer_id,
				'invoive_number'=>$invoice_number,
				'invoice_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
			
		//retrieve all users
		$this->db->from('ride');
		$this->db->select('*');
		$this->db->where('((invoice_id = 0) || (invoice_id IS NULL)) AND (ride_date >= "'.$start_date.'" OR ride_date <= "'.$end_date.'")');
		$query = $this->db->get();
			
		if ($query->num_rows() > 0)
		{
			if($this->db->insert('invoice', $data))
			{
				$last_id = $this->db->insert_id();
				$data2 = array(
					'invoice_id' => $last_id
				);
				
				foreach ($query->result() as $row)
				{
					$ride_id = $row->ride_id;
					$invoice_id = $row->invoice_id;
					
					$this->db->where('ride_id', $ride_id);
					if($this->db->update('ride', $data2))
					{
						
					}
				
				}

				$invoice_amount='0';
				$this->db->select('SUM(ride.cost) AS total_cost');
				$this->db->where('ride.ride_status_id = 4 AND ride.invoice_id = '.$last_id);
				$query2 = $this->db->get('ride');
				$total2 = $query2->row();
				$invoice_amount = $total2->total_cost;

				//var_dump($invoice_amount);die();
				$data3 = array(
					'amount' => $invoice_amount
				);
				
				$this->db->where('invoice_id', $last_id);
				if($this->db->update('invoice', $data3))
				{
					return TRUE;
				}

				
			}
		}
		else
		{
			return FALSE;
		}
		
		
		
	}
	
	/*
	*	Add a new customer
	*	@param string $image_name
	*
	*/
	public function add_customer($is_company,$item)
	{
		if($is_company == 0){
			$one = 1;
		}
		else{
			$one = $this->input->post('customer_type_id');
		}
		$this->db->where('customer_type.customer_type_name LIKE \'%'.$item.'%\'');
		$query = $this->db->get('customer_type');
		$customer_type_id = 1;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$customer_type_id = $value->customer_type_id;
			}
		}
		$one = $customer_type_id;
		$data = array
		(
			'customer_surname'=>$this->input->post('customer_surname'),
			'customer_first_name'=>$this->input->post('customer_first_name'),
			'customer_phone'=>$this->input->post('customer_phone'),
			'customer_email'=>$this->input->post('customer_email'),
			'customer_post_code'=>$this->input->post('customer_post_code'),
			'customer_address'=>$this->input->post('customer_address'),
			'customer_number'=>$this->create_customer_email(),
			'customer_created'=>date('Y-m-d H:i:s'),
			'customer_type_id'=>$one
		);
		
		$customer_password_readable = $this->input->post('customer_password_readable');
		if(!empty($customer_password_readable))
		{
			$data['customer_password_readable'] = $customer_password_readable;
			$data['customer_password'] = md5($customer_password_readable);
		}
			
		if($this->db->insert('customer', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_customer($customer_id, $is_company,$page_name)
	{
		if($is_company == 0){
			$one = 1;
		}
		else
		{
			$one = $this->input->post('customer_type_id');
		}
		$this->db->where('customer_type.customer_type_name LIKE \'%'.$page_name.'%\'');
		$query = $this->db->get('customer_type');
		$customer_type_id = 1;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$customer_type_id = $value->customer_type_id;
			}
		}
		$one = $customer_type_id;
		
		$data = array(
			'customer_surname'=>$this->input->post('customer_surname'),
			'customer_first_name'=>$this->input->post('customer_first_name'),
			'customer_phone'=>$this->input->post('customer_phone'),
			'customer_email'=>$this->input->post('customer_email'),
			'customer_post_code'=>$this->input->post('customer_post_code'),
			'customer_address'=>$this->input->post('customer_address'),
			'customer_type_id'=>$one
		);
		
		$customer_password_readable = $this->input->post('customer_password_readable');
		if(!empty($customer_password_readable))
		{
			$data['customer_password_readable'] = $customer_password_readable;
			$data['customer_password'] = md5($customer_password_readable);
		}
		
		$this->db->where('customer_id', $customer_id);
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer($customer_id)
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customers()
	{
		//retrieve all users
		$this->db->from('customer');
		$this->db->select('*');
		$this->db->order_by('customer_first_name', 'ASC');
		$this->db->order_by('customer_surname', 'ASC');
		$this->db->order_by('customer_phone', 'ASC');
		$query = $this->db->get();
		
		return $query;
	}

/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_company_contact($customer_contact_id) 
	{

		$data = array(
			
				'customer_contacts_first_name'=>$this->input->post('customer_contacts_first_name'),
				'customer_contacts_sur_name'=>$this->input->post('customer_contacts_sur_name'),
				'customer_contacts_phone'=>$this->input->post('customer_contacts_phone'),
				'customer_contacts_email'=>$this->input->post('customer_contacts_email'),
				
				
			);
			
		$this->db->where('customer_contacts_id', $customer_contact_id);
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_customer_contact($customer_contact_id)
	{
		//retrieve all users
		$this->db->from('customer_contacts');
		$this->db->select('*');
		$this->db->where('customer_contacts_id = '.$customer_contact_id);
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 1
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}



	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer_contact($customer_contact_id)
	{
		$data = array(
				'customer_contacts_status' => 0
			);
		$this->db->where('customer_contacts_id', $customer_contact_id);
		
		if($this->db->update('customer_contacts', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Delete an existing customer
	*	@param int $customer_id
	*
	*/
	public function delete_customer($customer_id)
	{
		if($this->db->delete('customer', array('customer_id' => $customer_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated customer
	*	@param int $customer_id
	*
	*/
	public function activate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 1
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_customer($customer_id)
	{
		$data = array(
				'customer_status' => 0
			);
		$this->db->where('customer_id', $customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_personnel_type($personnel_type_id)
	{
		$this->db->where(array('personnel_status' => 1, 'personnel_type_id' => $personnel_type_id));
		$query = $this->db->get('personnel');
		
		return $query;
	}
	
	/*
	*	Retrieve personnel
	*
	*/
	public function get_customer_types()
	{
		$this->db->where('customer_type_id > 1');
		$query = $this->db->get('customer_type');
		
		return $query;
	}
	
	public function get_app_countries()
	{
		$this->db->where('id > 0');
		$query = $this->db->get('apps_countries');
		
		return $query;
	}

	public function get_conservancies()
	{
		$this->db->where('applicant_category = 1');
		$query = $this->db->get('applications');
		
		return $query;
	}
	//import template
	function import_customers_template()
	{
		$this->load->library('Excel');
		
		$title = 'Customers Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Customer number';
		$report[$row_count][1] = 'First name';
		$report[$row_count][2] = 'Last name';
		$report[$row_count][3] = 'Email Address';
		$report[$row_count][4] = 'Phone Number';
		$report[$row_count][5] = 'Address';
		$report[$row_count][6] = 'City';
		$report[$row_count][7] = 'Post Code';
		$report[$row_count][8] = 'Rate';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	
	//import Company template
	function import_companies_template()
	{
		$query = $this->get_customer_types();
		$types = '(';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$customer_type_id = $res->customer_type_id;
				$customer_type_name = $res->customer_type_name;
				
				$types .= $customer_type_name." - ".$customer_type_id.", ";
			}
		}
		$types .= ')';
		
		$this->load->library('Excel');
		
		$title = 'Companies Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Customer number';
		$report[$row_count][1] = 'Company name';
		$report[$row_count][2] = 'Registration Number';
		$report[$row_count][3] = 'Email Address';
		$report[$row_count][4] = 'Phone Number';
		$report[$row_count][5] = 'Address';
		$report[$row_count][6] = 'City';
		$report[$row_count][7] = 'Post Code';
		$report[$row_count][8] = 'Rate';
		$report[$row_count][9] = 'Customer Type '.$types;
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	//import customers
	public function import_csv_customers($upload_path, $company_status)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_customers_data($array, $company_status);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	//sort customers imported data
	public function sort_customers_data($array, $company_status)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && (($total_columns == 9) || ($total_columns == 10)))
		{
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Customer Number</th>
						  <th>First Name</th>
						  <th>Last Name</th>
						  <th>Email</th>
						  <th>Phone</th>
						  <th>Address</th>
						  <th>City</th>
						  <th>Post Code</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				//$items['personnel_onames'] = $items['personnel_nssf_number'] = $items['personnel_fname'] = $items['created']= $items['modified_by'] = $items['modified_by'] = $items['created_by'] = $items['personnel_nhif_number']=$items['personnel_kra_pin']=$items['gender_id']=$items['personnel_national_id_number']=$items['bank_branch_id']=$items['bank_account_number']=$items['personnel_email']=$items['staff_id']=$items['cost_center']=$items['engagement_date']=$items['date_of_exit']='';
				$items = array();
				$items['company_status'] = $company_status;
				$customer_email = $items['customer_email'] = $array[$r][0];
				$customer_phone = '';
				if(empty($customer_email))
				{
					$customer_email = $items['customer_email'] = $this->customers_model->create_customer_email();
				}
				if(!empty($array[$r][1]))
				{
					$items['customer_first_name'] = mysql_real_escape_string(ucwords(strtolower($array[$r][1])));
				}
				
				if(!empty($array[$r][2]))
				{
					$items['customer_surname'] = mysql_real_escape_string(ucwords(strtolower($array[$r][2])));
				}
				
				if(!empty($array[$r][3]))
				{
					$items['customer_email']  = mysql_real_escape_string(ucwords(strtolower($array[$r][3])));
				}
				
				if(!empty($array[$r][4]))
				{
					$customer_phone = $items['customer_phone']=$array[$r][4];
				}
				
				if(!empty($array[$r][5]))
				{
					$items['customer_address']=$array[$r][5];
				}
				
				if(!empty($array[$r][6]))
				{
					$items['customer_city']=$array[$r][6];
				}
				
				if(!empty($array[$r][7]))
				{
					$items['customer_post_code']=$array[$r][7];
				}
				
				if(!empty($array[$r][8]))
				{
					$items['customer_rate'] = $array[$r][8];
				}
				
				if(isset($array[$r][9]))
				{
					$customer_type_id = $array[$r][9];
					//echo $customer_type_id; die();
				
					if($customer_type_id > 0)
					{
						$items['customer_type_id'] = $customer_type_id;
					}
				}
				
				else
				{
					$items['customer_type_id'] = 1;
				}
				$items['customer_created'] = date('Y-m-d H:i:s');
				/*$items['modified_by'] = $this->session->userdata('personnel_id');
				$items['created_by'] = $this->session->userdata('personnel_id');*/
				
				$comment = '';
				if(!empty($customer_email))
				{
					// check if the number already exists
					if(($this->check_current_customer_exisits($customer_email, $customer_phone)) == TRUE)
					{
						//number exists then update existing data
						$data = array(
							'customer_email' => $customer_email
						);
						$this->db->where($data);
						$this->db->update('customer', $items);
						$comment .= '<br/>Duplicate customer number entered, personnel data updated successfully';
						$class = 'warning';
					}
					else
					{
						// number does not exisit
						//save product in the db
						//var_dump($items);die();
						if($this->db->insert('customer', $items))
						{
							$comment .= '<br/>Customer successfully added to the database';
							$class = 'success';
						}
						
						else
						{
							$comment .= '<br/>Internal error. Could not add personnel to the database. Please contact the site administrator';
							$class = 'danger';
						}
					}
				}
				
				else
				{
					$comment .= '<br/>Not saved ensure you have a customer number entered';
					$class = 'danger';
				}
				
				
				$response .= '
					
						<tr class="'.$class.'">
							<td colspan="7">'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	
	public function create_customer_email()
	{
		//select product code
		$preffix = "NGE-";
		$this->db->from('customer');
		$this->db->where("customer_email LIKE '%".$preffix."%'");
		$this->db->select('MAX(customer_email) AS number');

		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number) * 1;
			$real_number++;
			$number = $preffix.sprintf('%04d', $real_number);
		}
		else
		{
			$number = $preffix.sprintf('%04d', 1);
		}
		return $number;
	}
	
	public function check_current_customer_exisits($customer_email, $customer_phone = NULL)
	{
		$this->db->where(array ('customer_email'=> $customer_email));
		
		$query = $this->db->get('customer');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
		
	}
	function get_payments($customer_id)
	{
		$this->db->where('status = 1 AND customer_id = '.$customer_id);
		$query = $this->db->get('utu_payments');
		
		return $query;
	}
	function get_trip_details($trip_id)
	{
		$this->db->where('applications.applicant_id = trip.applicant_id AND trip_id = '.$trip_id);
		$query = $this->db->get('trip,applications');
		
		return $query;
	}
	public function get_customer_documents($customer_id)
	{
		$this->db->from('customer_document, document_type');
		$this->db->select('*');
		$this->db->where('customer_document.document_type_id = document_type.document_type_id AND customer_id = '.$customer_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function delete_document_scan($customer_document_id)
	{
		//delete parent
		if($this->db->delete('customer_document', array('customer_document_id' => $customer_document_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function add_customer_document($customer_id, $document)
	{
		$data = array(
			'document_type_id'=> $this->input->post('document_type_id'),
			'customer_document_name'=> $this->input->post('customer_document_name'),
			'customer_document_file'=> $document,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'customer_id'=>$customer_id
		);
		
		if($this->db->insert('customer_document', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function send_customer_password($customer_first_name, $customer_id, $customer_email, $new_password)
	{
		$this->load->model('site/email_model');
		
		$contacts = $this->site_model->get_contacts();
		$message['contacts'] = $contacts;
		if(count($contacts) > 0)
		{
			$email = $contacts['email'];
			$facebook = $contacts['facebook'];
			$linkedin = $contacts['linkedin'];
			$logo = $contacts['logo'];
			$company_name = $contacts['company_name'];
			$phone = $contacts['phone'];
			$address = $contacts['address'];
			$post_code = $contacts['post_code'];
			$city = $contacts['city'];
			$building = $contacts['building'];
			$floor = $contacts['floor'];
			$location = $contacts['location'];
			
			$working_weekday = $contacts['working_weekday'];
			$working_weekend = $contacts['working_weekend'];
		}
		//Notify admin
		$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$message['subject'] =  'NGE Client Login';
		$message['text'] = '<p>Hello '.$customer_first_name.'</p> 
				<p>You can now access your customer account at '.site_url().'home with your email address and your password '.$new_password.'</p>';
		$message['text'] = $this->load->view('site/compose_mail', $message, TRUE);
		
		$sender['email'] = $email;
		$sender['name'] = $company_name;
		$receiver['email'] = $customer_email;
		$receiver['name'] = $customer_first_name;
	
		$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
		return $response;
	}
	
	/*
	*	Validate a customer's login request
	*
	*/
	public function validate_customer()
	{
		//select the customer by email from the database
		$this->db->select('*');

		$customer_email = $this->input->post('customer_email');
		$customer_password = $this->input->post('customer_password');

		$where = 'customer_email = "'.$customer_email.'" AND customer_status = 1 AND customer_password = "'.md5($customer_password).'" ';
		$this->db->where($where);
		$query = $this->db->get('customer');
		
		//if customer exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			//create customer's login session
			$newdata = array(
                   'customer_login_status' => TRUE,
                   'first_name'     => $result[0]->customer_first_name,
                   'first_name'     => $result[0]->customer_first_name,
                   'email'     => $result[0]->customer_email,
                   'customer_id'  => $result[0]->customer_id,
                   'phone'  => $result[0]->customer_phone,
                   'customer_status'  => $result[0]->customer_status,
                   'customer_password_readable'  => $result[0]->customer_password_readable
               );
			$this->session->set_userdata($newdata);
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}


	public function validate_conservancy_user()
	{
		//select the customer by email from the database
		$this->db->select('*');

		$customer_email = $this->input->post('customer_email');
		$customer_password = $this->input->post('customer_password');
		$applicant_category = $this->input->post('applicant_category');

		$where = 'username = "'.$customer_email.'" AND applicant_status = 1 AND userpassword = "'.md5($customer_password).'" AND applicant_category = '.$applicant_category;
		$this->db->where($where);
		$query = $this->db->get('applications');
		
		
		//if customer exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			//create customer's login session
			$newdata = array(
                   'applicant_login_status' => TRUE,
                   'applicant_name'     => $result[0]->applicant_name,
                   'applicant_email'     => $result[0]->applicant_email,
                   'applicant_phone'     => $result[0]->applicant_phone,
                   'applicant_id'  => $result[0]->applicant_id,
                   'applicant_status'  => $result[0]->applicant_status,
                   'applicant_type'  => $result[0]->applicant_category
               );
			$this->session->set_userdata($newdata);
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function change_password()
	{
		$data = array(
				'customer_password' => md5($this->input->post('new_password')),
				'customer_password_readable' => ''
			);
		$this->db->where('customer_password = "'.md5($this->input->post('current_password')).'" AND customer_id ='.$this->session->userdata('customer_id'));
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function change_member_profile($customer_id)
	{
		$data = array(
				'customer_phone' => $this->input->post('customer_phone'),
				'customer_email' => $this->input->post('customer_email')
			);
		// var_dump($data); die();
		$this->db->where('customer_id',$customer_id);
		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function change_institution_password()
	{
		$data = array(
				'userpassword' => md5($this->input->post('new_password'))
			);
		$this->db->where('userpassword = "'.md5($this->input->post('current_password')).'" AND applicant_id ='.$this->session->userdata('applicant_id'));
		
		if($this->db->update('applications', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function register_customer()
	{
		$data = array(
						'customer_first_name'=> $this->input->post('customer_name'),
						'customer_email'=> $this->input->post('customer_email'),
						'customer_type_id'=> 1,
						'customer_password'=> md5($this->input->post('password')),
						'customer_phone'=> $this->input->post('customer_phone'),
						'customer_created'=> date('Y-m-d')
					);

		if($this->db->insert('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function register_conservancy()
	{
		//customer details
		$applicant_name = $this->input->post('conservancy_name');
		$conservancy_email = $this->input->post('conservancy_email');
		$conservancy_phone = $this->input->post('conservancy_phone');
		$applicant_message = $this->input->post('conservancy_description');
		$conservation_contact_person = $this->input->post('conservation_contact_person');
		
		
		$data = array(
			'created' => date('Y-m-d H:i:s'),
			'applicant_name' => $this->input->post('conservancy_name'),
			'applicant_phone' => $this->input->post('conservancy_phone'),
			'applicant_email' => $this->input->post('conservancy_email'),
			'applicant_message' => $this->input->post('conservancy_description'),
			'contact_person' => $this->input->post('conservation_contact_person'),
			'applicant_category'=>1,
			'applicant_status'=>0

		);
		// var_dump($data); die();
		
		//check if customer exists
		$this->db->where('applicant_email ="'.$conservancy_email.'" AND applicant_category = 1');
		$query = $this->db->get('applications');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$applicant_id = $row->applicant_id;
			//Update last login
			return FALSE;
		}
		
		else
		{
			$this->db->insert('applications',$data);
			return TRUE;
		}
	}
	public function add_member_trip()
	{
		//customer details
		$conservancy_id = $this->input->post('conservancy_id');
		$trip_persons = $this->input->post('trip_persons');
		$trip_start_date = $this->input->post('trip_start_date');
		$trip_end_date = $this->input->post('trip_end_date');
		$contact_person_name = $this->input->post('contact_person_name');
		$contact_person_email = $this->input->post('contact_person_email');
		$contact_person_phone = $this->input->post('contact_person_phone');
		$description = $this->input->post('description');		
		
		$data = array(
			'created' => date('Y-m-d H:i:s'),
			'conservancy_id' => $this->input->post('conservancy_id'),
			'trip_persons' => $this->input->post('trip_persons'),
			'trip_start_date' => $this->input->post('trip_start_date'),
			'trip_end_date' => $this->input->post('trip_end_date'),
			'contact_person_name' => $this->input->post('contact_person_name'),
			'contact_person_email' => $this->input->post('contact_person_email'),
			'contact_person_phone' => $this->input->post('contact_person_phone'),
			'description' => $this->input->post('description'),
			'trip_status'=>0,
			'applicant_id'=>$this->session->userdata('applicant_id')

		);		
		if($this->db->insert('trip',$data))
		{
			return TRUE;

		}
		else
		{

			return FALSE;
		}
	}

	public function edit_member_trip($trip_id)
	{
				
		
		$data = array(
			'conservancy_id' => $this->input->post('conservancy_id'),
			'trip_persons' => $this->input->post('trip_persons'),
			'trip_start_date' => $this->input->post('trip_start_date'),
			'trip_end_date' => $this->input->post('trip_end_date'),
			'contact_person_name' => $this->input->post('contact_person_name'),
			'contact_person_email' => $this->input->post('contact_person_email'),
			'contact_person_phone' => $this->input->post('contact_person_phone'),
			'description' => $this->input->post('description'),
			'applicant_id'=>$this->session->userdata('applicant_id')

		);		
		$this->db->where('trip_id',$trip_id);
		if($this->db->update('trip',$data))
		{
			return TRUE;

		}
		else
		{

			return FALSE;
		}
	}

	public function register_membership_person()
	{
		//customer details
		$applicant_name = $this->input->post('conservancy_name');
		$conservancy_email = $this->input->post('conservancy_email');
		$conservancy_phone = $this->input->post('conservancy_phone');
		$applicant_message = $this->input->post('conservancy_description');
		$conservation_contact_person = $this->input->post('conservancy_contact_person');
		
		// var_dump($_POST); die();
		$data = array(
			'created' => date('Y-m-d H:i:s'),
			'applicant_name' => $this->input->post('conservancy_name'),
			'applicant_phone' => $this->input->post('conservancy_phone'),
			'applicant_email' => $this->input->post('conservancy_email'),
			'applicant_message' => $this->input->post('conservancy_description'),
			'contact_person' => $this->input->post('conservancy_contact_person'),
			'contact_person_designation' => $this->input->post('designation'),
			'website' => $this->input->post('website'),
			'contact_person_phone' => $this->input->post('contact_person_phone'),
			'contact_person_email' => $this->input->post('contact_person_email'),
			'applicant_category'=>$this->input->post('account_type'),
			'applicant_status'=>0

		);
		// var_dump($data); die();
		
		//check if customer exists
		$this->db->where('applicant_email ="'.$conservancy_email.'" AND applicant_category = 1');
		$query = $this->db->get('applications');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$applicant_id = $row->applicant_id;
			//Update last login
			return FALSE;
		}
		
		else
		{
			$this->db->insert('applications',$data);
			return TRUE;
		}
	}
	public function reset_password3()
	{
		$applicant_email = $this->input->post('applicant_email');
		//get customer details
		$this->db->where('applicant_email = "'.$applicant_email.'" AND applicant_category = 1');
		$query = $this->db->get('applications');
		
		if($query->num_rows() > 0)
		{
			$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);
			
			$data_update['userpassword'] = md5($new_password);
			$this->db->where('applicant_email', $applicant_email);
			if($this->db->update('applications',$data_update))
			{
				$row = $query->row();
				$first_name = $row->applicant_name;
				//$last_name = $row->customer_surname;
				//Send new password to email
				$contacts = $this->site_model->get_contacts();
				$email = $contacts['email'];
				$company_name = $contacts['company_name'];
				$data['contacts'] = $contacts;
				$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
				$data['text'] = '<strong>Hello '.$first_name.'</strong> 
						<p>Did you request a password change? It has been updated to:</p> 
						<p><strong>'.$new_password.'</strong></p>
						<p>You can log into your account and sign in with this new password here: 
						<a href="'.site_url().'login" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Login</a>
						<p>If you have not requested a password change please contact us here immediately.</p>
						<a href="'.site_url().'contact" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Contact Us</a>'
						;
				
				$message['text'] = $this->load->view('site/compose_mail', $data, TRUE);
				$message['subject'] =  "You Requested A Password Change?";
				
				$sender['email'] = $email;
				$sender['name'] = $company_name;
				$receiver['email'] = $applicant_email;
				$receiver['name'] = $first_name;
			
				$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
				$return['status'] = TRUE;
			}
			
			else
			{
				$return['status'] = FALSE;
				$return['message'] = 'Internal Error. Please try again or contact us';
			}
		}
		
		else
		{
			$return['status'] = FALSE;
			$return['message'] = 'Email not found. Have you registered for membership?';
		}
		
		return $return;
	}


	public function reset_password2()
	{
		$applicant_email = $this->input->post('applicant_email');
		//get customer details
		$this->db->where('applicant_email = "'.$applicant_email.'" AND applicant_category = 2');
		$query = $this->db->get('applications');
		
		if($query->num_rows() > 0)
		{
			$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);
			
			$data_update['userpassword'] = md5($new_password);
			$this->db->where('applicant_email', $applicant_email);
			if($this->db->update('applications',$data_update))
			{
				$row = $query->row();
				$first_name = $row->applicant_name;
				//$last_name = $row->customer_surname;
				//Send new password to email
				$contacts = $this->site_model->get_contacts();
				$email = $contacts['email'];
				$company_name = $contacts['company_name'];
				$data['contacts'] = $contacts;
				$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
				$data['text'] = '<strong>Hello '.$first_name.'</strong> 
						<p>Did you request a password change? It has been updated to:</p> 
						<p><strong>'.$new_password.'</strong></p>
						<p>You can log into your account and sign in with this new password here: 
						<a href="'.site_url().'login" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Login</a>
						<p>If you have not requested a password change please contact us here immediately.</p>
						<a href="'.site_url().'contact" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Contact Us</a>'
						;
				
				$message['text'] = $this->load->view('site/compose_mail', $data, TRUE);
				$message['subject'] =  "You Requested A Password Change?";
				
				$sender['email'] = $email;
				$sender['name'] = $company_name;
				$receiver['email'] = $applicant_email;
				$receiver['name'] = $first_name;
			
				$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
				$return['status'] = TRUE;
			}
			
			else
			{
				$return['status'] = FALSE;
				$return['message'] = 'Internal Error. Please try again or contact us';
			}
		}
		
		else
		{
			$return['status'] = FALSE;
			$return['message'] = 'Email not found. Have you registered for membership?';
		}
		
		return $return;
	}


	public function reset_member_password()
	{
		$customer_email = $this->input->post('customer_email');
		//get customer details
		$this->db->where('customer_email = "'.$customer_email.'"');
		$query = $this->db->get('customer');
		
		if($query->num_rows() > 0)
		{
			$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);
			
			$data_update['customer_password'] = md5($new_password);
			$this->db->where('customer_email', $customer_email);
			if($this->db->update('customer',$data_update))
			{
				$row = $query->row();
				$first_name = $row->customer_first_name;
				$last_name = $row->customer_surname;
				//Send new password to email
				$contacts = $this->site_model->get_contacts();
				$email = $contacts['email'];
				$company_name = $contacts['company_name'];
				$data['contacts'] = $contacts;
				$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
				$data['text'] = '<strong>Hello '.$first_name.'</strong> 
						<p>Did you request a password change? It has been updated to:</p> 
						<p><strong>'.$new_password.'</strong></p>
						<p>You can log into your account and sign in with this new password here: 
						<a href="'.site_url().'login" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Login</a>
						<p>If you have not requested a password change please contact us here immediately.</p>
						<a href="'.site_url().'contact" target="_blank" style="-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; background-color: #FA9C2A; border-color: #FA9C2A; border-image: none; border-radius: 3px; border-style: solid; border-width: 4px 20px; color: #ffffff; cursor: pointer; display: inline-block; font-weight: bold; line-height: 2; text-decoration: none;">Contact Us</a>'
						;
				
				$message['text'] = $this->load->view('site/compose_mail', $data, TRUE);
				$message['subject'] =  "You Requested A Password Change?";
				
				$sender['email'] = $email;
				$sender['name'] = $company_name;
				$receiver['email'] = $customer_email;
				$receiver['name'] = $first_name;
			
				$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
				$return['status'] = TRUE;
			}
			
			else
			{
				$return['status'] = FALSE;
				$return['message'] = 'Internal Error. Please try again or contact us';
			}
		}
		
		else
		{
			$return['status'] = FALSE;
			$return['message'] = 'Email not found. Have you registered for membership?';
		}
		
		return $return;
	}


	public function update_profile_picture($profile_image,$profile_thumb)
	{

		$applicant_id = $this->session->userdata('applicant_id');

		$data = array(
				'profile_image' => $profile_image,
				'profile_thumb' => $profile_thumb
			);

		$this->db->where('applicant_id', $applicant_id);		
		if($this->db->update('applications', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}

	}

	public function update_member_picture($profile_image,$profile_thumb)
	{

		$customer_id = $this->session->userdata('customer_id');

		$data = array(
				'profile_image' => $profile_image
			);

		$this->db->where('customer_id', $customer_id);		
		if($this->db->update('customer', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}

	}
}
?>