<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="CharityFund - Charity & Crowdfunding HTML Template" />
<meta name="keywords" content="building,business,construction,cleaning,transport,workshop" />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title>CharityFund - Charity & Crowdfunding HTML Template</title>

<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-yellow.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="js/jquery-2.2.0.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

 <!-- Header -->
  <?php include 'header.php';?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">Watoto Wetu Worldwide</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">Watoto Wetu Worldwide</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: Services -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0">Watoto Wetu Worldwide</h3>
            <p>“Watoto Wetu” is Swahili for Our Children. CWA recognizes that our children are the future of not only our countries but also our planet. Our children will inherit nature and it’s biodiversity from us. It is with this realization that our platform designed a program to engage the young children in realizing the importance of preserving nature’s flora and fauna. All environmental initiatives that seek to be sustainable must involve the future generations. They should not only be about leaving a safe and sustainable earth for our children but also be about teaching them the importance of leaving it that way. </p>
            <p>The program utilizes interesting hands-on activities that engage the children while teaching them the importance of environmental preservation. These activities include field trips, role-playing, scientific activities e.g. use of microscopes to view living things in water, examining wild-life artifacts, nature films or cartoons, guided hikes, in-club or in-school activities, eco-art amongst others. The program also utilizes the power of technology and in particular virtual reality to bring the children closer to life in the oceans or savannah.</p>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: Call To Action -->
    <section>
      <div class="container p-0">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
             
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
               <p>The program utilizes interesting hands-on activities that engage the children while teaching them the importance of environmental preservation. These activities include field trips, role-playing, scientific activities e.g. use of microscopes to view living things in water, examining wild-life artifacts, nature films or cartoons, guided hikes, in-club or in-school activities, eco-art amongst others. The program also utilizes the power of technology and in particular virtual reality to bring the children closer to life in the oceans or savannah.</p>
            </div>
            <div class="col-md-3">
               <p>The program is carried out for students in:</p>
              
<div class="row mt-30 mb-30 ml-20">
  
            
             
              <ul class="mt-10">
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Pre-school and nursery school</li>
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp; Primary school</li>
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Junior and senior high school</li>
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp; Home school</li>
              </ul>
             
             
            </div>
            </div>
            <div class="col-md-4">
              <p>The program is divided into 3 major parts that are spread through-out the year so as to fit into school calendars or curricula. The 3 major parts are:</p>

              <ul class="mt-10">
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Air and water preservation</li>
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Wildlife preservation</li>
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Forest preservation</li>
              </ul>
            
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: features -->
    <section>
      <div class="container pb-0">
        <div class="row text-center">
          <div class="col-sm-4">
            <div class="icon-box iconbox-theme-colored bg-lighter">
              <a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#">
                <i class="fa fa-gavel"></i>
              </a>
              <h5 class="icon-box-title">Service One</h5>
              <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              <a class="btn btn-flat btn-dark btn-sm mt-15" href="#">Read more</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="icon-box iconbox-theme-colored bg-lighter">
              <a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#">
                <i class="fa fa-briefcase"></i>
              </a>
              <h5 class="icon-box-title">Service Two</h5>
              <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              <a class="btn btn-flat btn-dark btn-sm mt-15" href="#">Read more</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="icon-box iconbox-theme-colored bg-lighter">
              <a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#">
                <i class="fa fa-book"></i>
              </a>
              <h5 class="icon-box-title">Service Three</h5>
              <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              <a class="btn btn-flat btn-dark btn-sm mt-15" href="#">Read more</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-light" data-stellar-background-ratio="0.5" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-90 pb-90">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-smile text-black-light mt-5 font-48 pull-right" data-text-color="#ccc"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="754" data-animation-duration="2000">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Happy Clients</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-hammer text-black-light mt-5 font-48 pull-right" data-text-color="#ccc"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="125" data-animation-duration="2500">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Success Projects</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-magic-wand text-black-light mt-5 font-48 pull-right"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="225" data-animation-duration="3000">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Team Members</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact style-1 pb-15 pt-15 p-20 bg-lightest">
              <i class="pe-7s-portfolio text-black-light mt-5 font-48 pull-right" data-text-color="#ccc"></i>
              <h2 class="animate-number text-theme-colored mt-0 font-48" data-value="45" data-animation-duration="2500">0</h2>
              <div class="clearfix"></div>
              <h4 class="text-uppercase font-14">Brunches World Wide</h4>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <footer id="footer" class="footer pb-0" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pb-20">
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="images/logo-wide-white.png">
            <p class="font-12 mt-20 mb-10">CharityFund is a library of Crowdfunding and Charity templates with predefined elements which helps you to build your own site. Lorem ipsum dolor sit amet consectetur.</p>
            <a class="text-gray font-12" href="#"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="styled-icons icon-dark mt-20">
              <li><a href="#" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" data-bg-color="#05A7E3"><i class="fa fa-skype"></i></a></li>
              <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" data-bg-color="#C22E2A"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Pages</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list-border">
              <li><a href="#">+(012) 345 6789</a></li>
              <li><a href="#">hello@yourdomain.com</a></li>
              <li><a href="#" class="lineheight-20">121 King Street, Melbourne Victoria 3000, Australia</a></li>
            </ul>
            <p class="text-white mb-5 mt-15">Subscribe to our newsletter</p>
            <form id="footer-mailchimp-subscription-form" class="newsletter-form mt-10">
              <label class="display-block" for="mce-EMAIL"></label>
              <div class="input-group">
                <input type="email" value="" name="EMAIL" placeholder="Your Email"  class="form-control" data-height="37px" id="mce-EMAIL">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-colored btn-theme-colored m-0"><i class="fa fa-paper-plane-o text-white"></i></button>
                </span>
              </div>
            </form>
            <!-- Mailchimp Subscription Form Validation-->
            <script type="text/javascript">
              $('#footer-mailchimp-subscription-form').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#footer-mailchimp-subscription-form'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid bg-theme-colored p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="text-white font-11 m-0">Copyright &copy;2015 ThemeMascot. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

</body>
</html>