<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="CharityFund - Charity & Crowdfunding HTML Template" />
<meta name="keywords" content="building,business,construction,cleaning,transport,workshop" />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title>CharityFund - Charity & Crowdfunding HTML Template</title>

<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-yellow.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="js/jquery-2.2.0.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

  
  <!-- Header -->
  <?php include 'header.php';?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h3 class="text-theme-colored font-36">Causes</h3>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">Causes</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Upcoming Event -->
   <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-9">
            <div class="row">
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-30">
                  <div class="thumb">
                    <img class="img-fullwidth" alt="" src="http://placehold.it/265x195">
                  </div>
                  <div class="progress-item mt-0">
                    <div class="progress mb-0">
                      <div class="progress-bar" data-percent="84"></div>
                    </div>
                  </div>
                  <div class="causes-details clearfix border-bottom p-15 pt-10">
                    <h5><a href="#">Olbosolat Conservancy</a></h5>
                    <p>Taking care of Kenya's elephants one at a time.</p>
                    <ul class="list-inline clearfix mt-20">
                      <li class="pull-left pr-0">Raised: $1890</li>
                      <li class="text-theme-colored pull-right pr-0">Goal: $2500</li>
                    </ul>
                    <div class="mt-10">
                     <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="#">Donate</a>
                     <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> 89 Donors</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-30">
                  <div class="thumb">
                    <img class="img-fullwidth" alt="" src="http://placehold.it/265x195">
                  </div>
                  <div class="progress-item mt-0">
                    <div class="progress mb-0">
                      <div class="progress-bar" data-percent="85"></div>
                    </div>
                  </div>
                  <div class="causes-details clearfix border-bottom p-15 pt-10">
                    <h5><a href="#">Laikipia West Trust</a></h5>
                    <p>Environmental conservation is our passion. </p>
                    <ul class="list-inline clearfix mt-20">
                      <li class="pull-left pr-0">Raised: $1890</li>
                      <li class="text-theme-colored pull-right pr-0">Goal: $2500</li>
                    </ul>
                    <div class="mt-10">
                     <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="#">Donate</a>
                     <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> 89 Donors</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-30">
                  <div class="thumb">
                    <img class="img-fullwidth" alt="" src="http://placehold.it/265x195">
                  </div>
                  <div class="progress-item mt-0">
                    <div class="progress mb-0">
                      <div class="progress-bar" data-percent="86"></div>
                    </div>
                  </div>
                  <div class="causes-details clearfix border-bottom p-15 pt-10">
                    <h5><a href="#">Mara Cultural Initiative</a></h5>
                    <p>oin 1,000 foreign vistors in preserving the culture in the Mara</p>
                    <ul class="list-inline clearfix mt-20">
                      <li class="pull-left pr-0">Raised: $1890</li>
                      <li class="text-theme-colored pull-right pr-0">Goal: $2500000</li>
                    </ul>
                    <div class="mt-10">
                     <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="#">Donate</a>
                     <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> 89 Donors</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-30">
                  <div class="thumb">
                    <img class="img-fullwidth" alt="" src="http://placehold.it/265x195">
                  </div>
                  <div class="progress-item mt-0">
                    <div class="progress mb-0">
                      <div class="progress-bar" data-percent="87"></div>
                    </div>
                  </div>
                  <div class="causes-details clearfix border-bottom p-15 pt-10">
                    <h5><a href="#">Athi Kapiti Conservation Village</a></h5>
                    <p>Nature's beauty is found in Athi Kapiti Conservation</p>
                    <ul class="list-inline clearfix mt-20">
                      <li class="pull-left pr-0">Raised: $1890</li>
                      <li class="text-theme-colored pull-right pr-0">Goal: $2500</li>
                    </ul>
                    <div class="mt-10">
                     <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="#">Donate</a>
                     <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> 89 Donors</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-30">
                  <div class="thumb">
                    <img class="img-fullwidth" alt="" src="http://placehold.it/265x195">
                  </div>
                  <div class="progress-item mt-0">
                    <div class="progress mb-0">
                      <div class="progress-bar" data-percent="86"></div>
                    </div>
                  </div>
                  <div class="causes-details clearfix border-bottom p-15 pt-10">
                    <h5><a href="#">Lake Turkana Conservation Trust</a></h5>
                    <p>We are stationed around Lake Turkana taking care of the fishstock and water</p>
                    <ul class="list-inline clearfix mt-20">
                      <li class="pull-left pr-0">Raised: $1890</li>
                      <li class="text-theme-colored pull-right pr-0">Goal: $2500</li>
                    </ul>
                    <div class="mt-10">
                     <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="#">Donate</a>
                     <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> 89 Donors</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="causes bg-lighter box-hover-effect effect1 maxwidth500 mb-30">
                  <div class="thumb">
                    <img class="img-fullwidth" alt="" src="http://placehold.it/265x195">
                  </div>
                  <div class="progress-item mt-0">
                    <div class="progress mb-0">
                      <div class="progress-bar" data-percent="87"></div>
                    </div>
                  </div>
                  <div class="causes-details clearfix border-bottom p-15 pt-10">
                    <h5><a href="#">Ndakaini Half Marathon </a></h5>
                    <p>Sponsored by UAP for 15 years</p>
                    <ul class="list-inline clearfix mt-20">
                      <li class="pull-left pr-0">Raised: $1890</li>
                      <li class="text-theme-colored pull-right pr-0">Goal: $2500</li>
                    </ul>
                    <div class="mt-10">
                     <a class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10" href="#">Donate</a>
                     <div class="pull-right mt-15"><i class="fa fa-heart-o text-theme-colored"></i> 89 Donors</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <nav>
                  <ul class="pagination theme-colored pull-right xs-pull-center mb-xs-40">
                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Search box</h5>
                <div class="search-form">
                  <form>
                    <div class="input-group">
                      <input type="text" placeholder="Click to Search" class="form-control search-input">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Categories</h5>
                <div class="categories">
                  <ul class="list list-border angle-double-right">
                   <li><a href="#">Environment<span>(19)</span></a></li>
                    <li><a href="#">Water<span>(21)</span></a></li>
                    <li><a href="#">Wildlife<span>(15)</span></a></li>
                    <li><a href="#">Culture<span>(35)</span></a></li>
                    <li><a href="#">Events<span>(16)</span></a></li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Latest News</h5>
                <div class="latest-posts">
                   <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Regional Partnership</a></h5>
                      <p>CWA in November signed a partnership agreement with KWCA</p>
                    </div>
                  </article>
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Sponsorship for Watoto Wetu Worldwide</a></h5>
                      <p>Some of the Sponsors with CWA CEO Edward Macharia</p>
                    </div>
                  </article>
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">UNEP announces Ksh.100 million fund for Africa</a></h5>
                      <p>The United Nations Environmental Programme has launched a new fund in support of initiatives in Africa</p>
                    </div>
                  </article>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Photos from Flickr</h5>
                <div id="flickr-feed" class="clearfix">
                  <!-- Flickr Link -->
                  <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155@N08">
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <footer id="footer" class="footer pb-0" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pb-20">
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="images/logo-wide-white.png">
            <p class="font-12 mt-20 mb-10">CharityFund is a library of Crowdfunding and Charity templates with predefined elements which helps you to build your own site. Lorem ipsum dolor sit amet consectetur.</p>
            <a class="text-gray font-12" href="#"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="styled-icons icon-dark mt-20">
              <li><a href="#" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" data-bg-color="#05A7E3"><i class="fa fa-skype"></i></a></li>
              <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" data-bg-color="#C22E2A"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Pages</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list-border">
              <li><a href="#">+(012) 345 6789</a></li>
              <li><a href="#">hello@yourdomain.com</a></li>
              <li><a href="#" class="lineheight-20">121 King Street, Melbourne Victoria 3000, Australia</a></li>
            </ul>
            <p class="text-white mb-5 mt-15">Subscribe to our newsletter</p>
            <form id="footer-mailchimp-subscription-form" class="newsletter-form mt-10">
              <label class="display-block" for="mce-EMAIL"></label>
              <div class="input-group">
                <input type="email" value="" name="EMAIL" placeholder="Your Email"  class="form-control" data-height="37px" id="mce-EMAIL">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-colored btn-theme-colored m-0"><i class="fa fa-paper-plane-o text-white"></i></button>
                </span>
              </div>
            </form>
            <!-- Mailchimp Subscription Form Validation-->
            <script type="text/javascript">
              $('#footer-mailchimp-subscription-form').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#footer-mailchimp-subscription-form'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid bg-theme-colored p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="text-white font-11 m-0">Copyright &copy;2015 ThemeMascot. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

</body>
</html>