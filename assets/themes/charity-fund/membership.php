<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="CharityFund - Charity & Crowdfunding HTML Template" />
<meta name="keywords" content="building,business,construction,cleaning,transport,workshop" />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title>CharityFund - Charity & Crowdfunding HTML Template</title>

<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-yellow.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="js/jquery-2.2.0.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

 <!-- Header -->
  <?php include 'header.php';?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">Membership</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">Membership</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: About -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0">Membership</h3>
            <p>You can become a donor today by signing up by clicking the “sign up” tab on the Home Page. Connect Wild Africa values and recognizes the support and contribution of our donors. Your kind donation will go a long way in ensuring some beautiful things (our rivers, forests, wildlife, fish stocks etc.) last forever.</p>
          </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- divider: Emergency Services -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2"  data-bg-img="http://placehold.it/1920x1280">
      <div class="container">
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-12">
              <h3 class="mt-0">How you can help us</h3>
              <h2>Just call at <span class="text-theme-colored">(01) 234 5678</span> to make a donation</h2>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- divider: Emergency Services -->
   
    <!-- divider: Emergency Services -->
        <!-- divider: Emergency Services -->
       <!-- divider: Emergency Services -->
    <!-- divider: Emergency Services -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6">
              <h3> MEMBERSHIP BENEFITS </h3>
              <p>As a member you will be entitled to your own personal account with:</p>
                <div class="row mt-30 mb-30 ml-20">
           

              <ul class="mt-10">
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Your profile</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;A history of the contributions you have made</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;A history of the projects you have supported</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;The impact your support or contribution has made in a particular environmental initiative</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Communiqué with the conservancies and environmental groups carrying out the initiatives you are supporting</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Certificates, awards or even invite to events just to appreciate the great work you are making as a change maker!</li>
              </ul>
             
          

          </div>
            </div>
            <div class="col-md-6">
              <h3>DONOR BILL OF RIGHTS</h3>

                <p>As a platform we also respect the rights and privileges of all our donors. We therefore have a donor’s bill of rights that includes the following:</p>
                          <div class="row mt-30 mb-30 ml-20">
           

              <ul class="mt-10">
                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;All donors are entitled to receive a charitable donations tax receipt.</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Donors and prospective donors will never be subjected to coercion or undue pressure.</li>

                <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;All reasonable efforts will be taken to honour any request by a donor not to be contacted at home by telephone or other technology; also, all reasonable efforts will be taken to honour requests from donors who are contacted by telephone to receive printed material concerning the CWA.</li>

                 <li class="mb-10"><i class="fa fa-check-circle text-theme-colored"></i>&emsp;Any confidential information from or about donors that is obtained by or on behalf of the CWA shall not be disclosed without the express consent of the donor.</li>
              </ul>

          
          </div>
            </div>
          </div>
        </div>
      </div>      
    </section>
    

     <!-- Section: features -->
    <section>
      <div class="container pb-0">
        <div class="row text-center">
          <div class="col-sm-4">
            <div class="icon-box iconbox-theme-colored bg-lighter">
              <a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#">
                <i class="fa fa-gavel"></i>
              </a>
              <h5 class="icon-box-title">Service One</h5>
              <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              <a class="btn btn-flat btn-dark btn-sm mt-15" href="#">Read more</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="icon-box iconbox-theme-colored bg-lighter">
              <a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#">
                <i class="fa fa-briefcase"></i>
              </a>
              <h5 class="icon-box-title">Service Two</h5>
              <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              <a class="btn btn-flat btn-dark btn-sm mt-15" href="#">Read more</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="icon-box iconbox-theme-colored bg-lighter">
              <a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#">
                <i class="fa fa-book"></i>
              </a>
              <h5 class="icon-box-title">Service Three</h5>
              <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              <a class="btn btn-flat btn-dark btn-sm mt-15" href="#">Read more</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- divider: Became a Volunteers -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.5" data-bg-img="http://placehold.it/1920x1280">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-7">
              <h3 class="line-bottom">Became a Volunteer</h3>
              <p class="mt-30 mb-30">Volunteers make an invaluable contribution to environmental conservation efforts. Volunteers like you help us maintain more than 3,300 acres of natural areas, and offer hundreds of school programs, special events, hikes, and weekend programs throughout the year across the country.</p>
              <a class="btn btn-dark btn-theme-colored btn-lg btn-flat pull-left pl-30 pr-30" href="#">Join Us</a>
            </div>
            <div class="col-md-5">
              <div class="fluid-video-wrapper">
                <iframe src="//player.vimeo.com/video/22029657?title=0&amp;byline=0&amp;portrait=0" width="640" height="360"  title="Creative" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Divider: Partners & Donors -->
    <section class="">
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-12">
            <div class="clients-logo carousel">
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <footer id="footer" class="footer pb-0" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pb-20">
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="images/logo-wide-white.png">
            <p class="font-12 mt-20 mb-10">CharityFund is a library of Crowdfunding and Charity templates with predefined elements which helps you to build your own site. Lorem ipsum dolor sit amet consectetur.</p>
            <a class="text-gray font-12" href="#"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="styled-icons icon-dark mt-20">
              <li><a href="#" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" data-bg-color="#05A7E3"><i class="fa fa-skype"></i></a></li>
              <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" data-bg-color="#C22E2A"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Pages</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list-border">
              <li><a href="#">+(012) 345 6789</a></li>
              <li><a href="#">hello@yourdomain.com</a></li>
              <li><a href="#" class="lineheight-20">121 King Street, Melbourne Victoria 3000, Australia</a></li>
            </ul>
            <p class="text-white mb-5 mt-15">Subscribe to our newsletter</p>
            <form id="footer-mailchimp-subscription-form" class="newsletter-form mt-10">
              <label class="display-block" for="mce-EMAIL"></label>
              <div class="input-group">
                <input type="email" value="" name="EMAIL" placeholder="Your Email"  class="form-control" data-height="37px" id="mce-EMAIL">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-colored btn-theme-colored m-0"><i class="fa fa-paper-plane-o text-white"></i></button>
                </span>
              </div>
            </form>
            <!-- Mailchimp Subscription Form Validation-->
            <script type="text/javascript">
              $('#footer-mailchimp-subscription-form').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#footer-mailchimp-subscription-form'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid bg-theme-colored p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="text-white font-11 m-0">Copyright &copy;2015 ThemeMascot. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

</body>
</html>