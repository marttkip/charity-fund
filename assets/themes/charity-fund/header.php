<body class="">
<div id="wrapper">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>

 <header id="header" class="header">
    <div class="header-top bg-deep sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="widget no-border m-0">
              <ul class="styled-icons icon-sm sm-text-center">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-9">
            <div class="widget no-border m-0">
              <ul class="list-inline text-right sm-text-center mt-5">
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored"></i> <a class="text-gray" href="#">123-456-789</a> </li>
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-clock-o text-theme-colored"></i> Mon-Fri 8:00 to 2:00 </li>
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored"></i> <a class="text-gray" href="#">contact@yourdomain.com</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
        <div class="container">
          <nav id="menuzord-right" class="menuzord orange bg-lightest">
            <a class="menuzord-brand" href="javascript:void(0)">
              <img src="images/logo-wide.png" alt="">
            </a>
            <ul class="menuzord-menu">
              <li class="active"><a href="index.php">Home</a>
            
              </li>
              <li><a href="about.php">About Us</a>
               
              </li>
              <li><a href="#">Programmes</a>
                    <ul class="dropdown">
                      <li><a href="watoto-wetu-worldwide.php">Watoto Wetu World Wide</a></li>
                      <li><a href="mazingira-link.php">Mazingira Link</a></li>
                    </ul>
                  </li>
              <li><a href="membership.php">Membership</a></li>
              <li><a href="initiatives.php">Initiatives</a></li>
             
              <li><a href="#">Opportunities</a>
                <ul class="dropdown">
                  <li><a href="volunteer.php">Volunteer Opportunities</a></li>
                      <li><a href="internship.php">Internship Opportunities</a></li>
                      <li><a href="research.php">Research Opportunities</a></li>
                      <li><a href="opportunities.php">Job Opportunities</a></li>
                </ul>
              </li>
              <li><a href="events.php">Events</a></li>
            <li><a href="blog.php">Blog</a></li>       
            
             
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
  