<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="CharityFund - Charity & Crowdfunding HTML Template" />
<meta name="keywords" content="building,business,construction,cleaning,transport,workshop" />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title>CharityFund - Charity & Crowdfunding HTML Template</title>

<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- CSS | Theme Color -->
<link href="css/colors/theme-skin-yellow.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="js/jquery-2.2.0.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

  <!-- Header -->
  <?php include 'header.php';?>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-30 pb-30">
        <!-- Section Content -->
        <div class="section-content text-center">
          <div class="row"> 
            <div class="col-md-6 col-md-offset-3 text-center">
              <h2 class="text-theme-colored font-36">About</h2>
              <ol class="breadcrumb text-center mt-10 white">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">About</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: About -->
    <section> 
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-5">
            <h3 class="text-theme-colored text-uppercase mt-0">About Us</h3>
            <p>Connect Wild Africa (CWA) is a digital web platform that enables environmental enthusiasts and the general public to contribute to environmental initiatives that are making a difference in protecting and preserving nature’s bio-diversity for us and for the future generations. </p>

            <p> CWA leverages the power of the internet and the new wave of green crowd-funding to support the initiatives and conservancies that are doing the good work of environmental and wildlife preservation. CWA provides a community through which to exchange ideas, educate, tell stories, recruit, and fundraise for environmental initiatives. Through this engagement, conservancies, eco businesses, environmental educators and projects that use the green platform build connections with like-minded environmental change-makers and visionaries, and find resources that help to prepare them to perpetuate positive change.</p>

            <p> Our vision is to build a global online community that supports and contributes to wildlife and environmental preservation.</p> 

            <p> Our mission is to bring environmental enthusiasts and conservancies or initiatives together by providing a platform for engagement and an easy, secure and seamless payment pathway for all those who want to contribute to environmental preservation. </p>
            <p> Join us today and be a change-maker!
                                                  </p>  
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="image-carousel">
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
              <div class="item">
                <div class="thumb">
                  <img src="http://placehold.it/1920x1280" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- divider: Emergency Services -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2"  data-bg-img="http://placehold.it/1920x1280">
      <div class="container">
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-12">
              <h3 class="mt-0">How you can help us</h3>
              <h2>Just call at <span class="text-theme-colored">(01) 234 5678</span> to make a donation</h2>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- divider: Emergency Services -->
    <section>
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-7">
              <p></p>
            </div>
            <div class="col-md-5">
              <p></p>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- divider: Became a Volunteers -->
    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.5" data-bg-img="http://placehold.it/1920x1280">
      <div class="container">
        <div class="section-content">
          <div class="row">
             <div class="col-md-5">
              <div class="fluid-parallax">
                <img src="http://placehold.it/1920x1280" alt="">
              </div>
            </div>
            <div class="col-md-7">
              <h3 class="line-bottom">Word from the CEO</h3>
              <p class="mt-30 mb-30">At CWA, we are excited about bringing all stakeholders and citizens together in building a common front to address environmental concerns. We are focussed on mobilizing and implementing environmental initiatives that have a positive impact on the environmental preservation and protection agenda.</p>

              <p> Plants, beautiful animals, fish stocks, fresh air, clean water and hydrological cycles are nature's gift to us. It is therefore imperative that we must strive to preserve these wonderful things for our children and future generations.</p>

              <p> We must not forget that the element of sustainable development in terms of environment is a subject that may require the financial obligations and contributions of nations and their people so that it can be achieved.</p>

              <p> We invite you to use our platform to contribute to those initiatives you feel are having the most positive impact on the environment and culture. You are a changemaker!</p>
             
            </div>
           
          </div>
        </div>
      </div>      
    </section>


     <!-- Section: Volunteer -->

    <section class="divider parallax layer-overlay overlay-deep" data-stellar-background-ratio="0.2" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pb-30">
        <div class="section-content">
           <div class="col-md-12">
              <h3 class="text-theme-colored text-uppercase mt-0">Our Team</h3>
           </div>
          <div class="row multi-row-clearfix">
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">John Bisley</a></h4>
                  <h6 class="occupation">Member, Advisory Board</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Jane Bisley</a></h4>
                  <h6 class="occupation">Member, Advisory Board</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Floric Macharia</a></h4>
                  <h6 class="occupation">Member, Advisory Board</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Emmy Bisley</a></h4>
                  <h6 class="occupation"> Head of Business Development</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Stacey Bisley</a></h4>
                  <h6 class="occupation">Head of Marketing</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Dennis Maina</a></h4>
                  <h6 class="occupation">Chief Financial Officer</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Viviane Bisley</a></h4>
                  <h6 class="occupation">Head of Programmes</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#"> Felix Quentin</a></h4>
                  <h6 class="occupation">Head of Research,Strategy and Innovation</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
                        <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">John Bisley</a></h4>
                  <h6 class="occupation">Member, Advisory Board</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Jane Bisley</a></h4>
                  <h6 class="occupation">Member, Advisory Board</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Floric Macharia</a></h4>
                  <h6 class="occupation">Member, Advisory Board</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3 mb-sm-60 text-left sm-text-center">
              <div class="volunteer border bg-white-fa maxwidth400 mb-30 p-30">
                <div class="thumb"><img alt="" src="http://placehold.it/270x270" class="img-fullwidth"></div>
                <div class="info">
                  <h4 class="name"><a href="#">Emmy Bisley</a></h4>
                  <h6 class="occupation"> Head of Business Development</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit dictum nobis.</p>
                  <hr>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored mt-10 mb-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  

    <!-- Divider: Partners & Donors -->

    <section class="">
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-12">

              <h3 class="text-theme-colored text-uppercase mt-0">Our Partners</h3>
          
            <div class="clients-logo carousel">
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="http://placehold.it/150x120" alt=""></a> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <footer id="footer" class="footer pb-0" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pb-20">
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="images/logo-wide-white.png">
            <p class="font-12 mt-20 mb-10">Connect Wild Africa (CWA) is a digital web platform that enables environmental enthusiasts and the general public to contribute to environmental initiatives that are making a difference in protecting and preserving nature’s bio-diversity for us and for the future generations</p>
            <a class="text-gray font-12" href="#"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="styled-icons icon-dark mt-20">
              <li><a href="#" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" data-bg-color="#05A7E3"><i class="fa fa-skype"></i></a></li>
              <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" data-bg-color="#C22E2A"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Pages</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Donor Privacy Policy</a></li>
              <li><a href="#">Disclaimer</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Copyright Notice</a></li>
              <li><a href="#">Media Center</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list-border">
              <li><a href="#">0721139585/072</a></li>
              <li><a href="#">hello@yourdomain.com</a></li>
              <li><a href="#" class="lineheight-20"> Ongata Rongai, Nairobi Kenya</a></li>
            </ul>
            <p class="text-white mb-5 mt-15">Subscribe to our newsletter</p>
            <form id="footer-mailchimp-subscription-form" class="newsletter-form mt-10">
              <label class="display-block" for="mce-EMAIL"></label>
              <div class="input-group">
                <input type="email" value="" name="EMAIL" placeholder="Your Email"  class="form-control" data-height="37px" id="mce-EMAIL">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-colored btn-theme-colored m-0"><i class="fa fa-paper-plane-o text-white"></i></button>
                </span>
              </div>
            </form>
            <!-- Mailchimp Subscription Form Validation-->
            <script type="text/javascript">
              $('#footer-mailchimp-subscription-form').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#footer-mailchimp-subscription-form'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid bg-theme-colored p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="text-white font-11 m-0">Copyright &copy;2015 ThemeMascot. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

</body>
</html>